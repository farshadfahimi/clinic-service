@extends('layouts.app')

@section('body')

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $service->name }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('service', $service) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Inner Pages Main Section -->
<section class="ulockd-service-details">
    <div class="container-fluid">	
        <div class="col-md-4 col-lg-3 ulockd-pdng0">
            <div class="ulockd-all-service">
                <h4>@lang('message.pages.services')</h4>
                <div class="list-group">
                    @foreach ($services as $item)
                        <a href="{{ urldecode(route('services.show', ['name' => $item->name])) }}" 
                                class="list-group-item {{ $item->id == $service->id ? 'active' : null }}">
                                {{ $item->name }}
                        </a>   
                    @endforeach
                </div>
            </div>

            {{-- <div class="ulockd-inr-brochure">
                <h4>Useful Documentation</h4>
                <p>pain was born and I will give you a complete accounf the system, and expound the actuteachings of the grea</p>
                <ul class="list-unstyled">
                    <li><a class="btn btn-default btn-lg ulockd-btn-thm2" href="#"><i class="fa fa-file-excel-o fz20"></i> Document1.csv</a></li>
                    <li><a class="btn btn-default btn-lg ulockd-btn-thm2" href="#"><i class="fa fa-file-word-o fz20"></i> Document2.DOC</a></li>
                    <li><a class="btn btn-default btn-lg ulockd-btn-thm2" href="#"><i class="fa fa fa-file-pdf-o fz20"></i> Document3.PDF</a></li>
                </ul>
            </div> --}}
        </div>

        <div class="col-md-8 col-lg-9">
            {!! $service->body !!}
        </div>
    </div>
</section>

@endsection
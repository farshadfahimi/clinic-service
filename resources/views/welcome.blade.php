@extends('layouts.app')

@section('body')
    <!-- Home Design -->
    @if($slides->count())
        <div class="ulockd-home-slider">
            <div class="container-fluid">
                <div class="row">
                    <div class="ulockd-pmz">
                        <swiper class="main-slider2" 
                                :options="{
                                pagination: {el:'.swiper-pagination', dynamicBullets: true, clickable: true}, 
                                loop: true,
                                speed: 700,
                                autoplay: { delay: 3000, disableOnInteraction: true},
                                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev'}
                            }">
                            @foreach ($slides as $slide)
                            <swiper-slide>
                                @include('components.slider', ['slide' => $slide])
                            </swiper-slide>
                            @endforeach
                            <div class="swiper-pagination" slot="pagination"></div>

                            <div class="swiper-button-prev" slot="button-prev"></div>
                            <div class="swiper-button-next" slot="button-next"></div>
                        </swiper>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- Our First Divider -->
	<section class="ulockd-frst-divider style1" data-stellar-background-ratio="0.2">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-12 text-center">
					<div class="subscriber-form subscribe">
                        <h3 class="color-white">@lang('message.search_title')</h3>

                        <form action="{{ route('search') }}" method="GET">
                            <input type="text" name="q" />
                            <button type="submit">@lang('message.search')</button>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</section>

    <!-- Our About -->
    @if($departments->count())
    <section class="ulockd-about bgc-snowshade">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2><span class="text-thm2">@lang('message.welcome.about-us')</span></h2>
                        <div class="mt-separator"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($departments as $department)
                    @include('components.about', ['department' => $department])
                @endforeach            
            </div>
        </div>
    </section>
    @endif

    @if($servicesCategories->count())
    <!-- Our Service -->
	<section class="ulockd-service">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2 class="text-uppercase"><span class="text-thm2">@lang('message.welcome.services')</span></h2>
                        <div class="mt-separator"></div>
                    </div>
                </div>
            </div>

            <div class="row ulockd-mrgn1210">
                @foreach ($servicesCategories as $service)    
                    @include('components.service', ['service' => $service])
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!-- Our Team -->
    @if($teams->count())
	<section class="ulockd-team-two">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2 class="text-uppercase"><span class="text-thm2">@lang('message.welcome.team')</span></h2>
                        <div class="mt-separator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach ($teams as $team)
                    @include('components.teams.team', ['team', $team])
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!-- Our Blog -->
    @if($threads->count())
	<section class="ulockd-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2 class="text-uppercase"><span class="text-thm2">@lang('message.welcome.posts')</span></h2>
                        <div class="mt-separator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach ($threads as $thread)
                    @include('components.post_item', ['post'=> $thread])
                @endforeach
            </div>
        </div>
    </section>
    @endif

    {{-- Gallery --}}
    <section class="ulockd-service-three">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2 class="text-uppercase"><span class="text-thm2">@lang('message.welcome.gallery')</span></h2>
						<div class="mt-separator"></div>
					</div>
				</div>
            </div>
            
            <div class="row">
                <gallery-swiper :items="{{ json_encode($gallery) }}"/>
            </div>
		</div>
    </section>

    {{-- Users opinions --}}
    @if($opinions->count())
    <section class="ulockd-service-three">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2 class="text-uppercase"><span class="text-thm2">@lang('message.welcome.opinion')</span></h2>
						<div class="mt-separator"></div>
					</div>
				</div>
            </div>
            
            <div class="row">
                <swiper :options="{ slidesPerView: 3, loop: true, navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                  }}">
                    @foreach ($opinions as $item)
                    <swiper-slide>
                        <article class="ulockd-blog-post ulockd-mrgn630">
                            <div class="bp-details one">
                                <h5 class="post-title" dir="auto">{{ $item->name }}</h5>
                                
                                <p>
                                    {{ $item->body }}
                                </p>
                            </div>
                        </article>
                    </swiper-slide>
                    @endforeach

                    <div class="swiper-button-prev" slot="button-prev"></div>
                    <div class="swiper-button-next" slot="button-next"></div>
                </swiper>        
            </div>
		</div>
    </section>
    @endif

    
    
    {{-- News --}}
    <!-- Our Blog -->
    @if($news->count())
	<section class="ulockd-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2 class="text-uppercase"><span class="text-thm2">@lang('message.pages.news')</span></h2>
                        <div class="mt-separator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach ($news as $newsItem)
                    @include('components.news_item', ['news'=> $newsItem])
                @endforeach
            </div>
        </div>
    </section>
    @endif
@endsection
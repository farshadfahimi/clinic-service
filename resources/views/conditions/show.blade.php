@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home" style="background-image: url({{ $condition->poster }})">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $condition->name }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('condition', $condition) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Inner Pages Main Section -->
<section class="ulockd-service-details">
    <div class="container-fluid">
        <div class="col-md-3">
            @foreach($condition->variations as $item)
                <a class="btn btn-default ulockd-btn-thm2 ulockd-mrgn610" href="{{ route('conditions.show', ['condition' => $item->id]) }}">{{ $item->name }}</a>
            @endforeach
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12 ulockd-mrgn1210">
                    <div class="ulockd-pd-content">
                        {!! $condition->body !!}
                    </div>
                </div>

                @if($condition->reason_description)
                <div class="col-md-12 ulockd-mrgn1210">
                @if($condition->reason_title)
                    <h3>{{ $condition->reason_title }}</h3>
                @endif

                    <div class="ulockd-pd-content">
                        {!! $condition->reason_description !!}
                    </div>
                </div>
                @endif

                @if($condition->signs_description)
                <div class="col-md-12 ulockd-mrgn1210">
                @if($condition->signs_title)
                    <h3>{{ $condition->signs_title }}</h3>
                @endif

                    <div class="ulockd-pd-content">
                        {!! $condition->signs_description !!}
                    </div>
                </div>
                @endif

                @if($condition->approach_description)
                <div class="col-md-12 ulockd-mrgn1210">
                @if($condition->approach_title)
                    <h3>{{ $condition->approach_title }}</h3>
                @endif

                    <div class="ulockd-pd-content">
                        {!! $condition->approach_description !!}
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            @if(count($condition->questions))
                <div class="col-md-12 ulockd-mrgn1210">
                    @component('components.faq', ['faqs' => $condition->questions])   
                    @endcomponent
                </div>
            @endif
        </div>
    </div>
</section>
@endsection
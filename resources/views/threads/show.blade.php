@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home" style="background-image: url({{$thread->poster}})">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $thread->title }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('thread', $thread) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Inner Pages Main Section -->
<section class="ulockd-service-details">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 ulockd-mrgn1210">
                    <div class="ulockd-pd-content" dir="auto">
                        {!! $thread->body !!}
                    </div>
                </div>
            </div>

            {{-- Comments Section --}}
            <div class="row">
                <div class="col-md-12 ulockd-mrgn1210">
                    @foreach ($tags as $tag)
                        <a href="{{ route('threads-list', ['tag' => $tag]) }}" class="btn btn-primary">
                            {{ $tag }}
                        </a>
                    @endforeach
                </div>

                <div class="col-md-12" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    <div class="ulockd-bp-single">
                        {{-- <h2 class="ulockd-bps-title">@lang('message.comment')</h2> --}}
                        @foreach ($thread->replies as $reply)
                            <div class="ulockd-bps-first">
                                <div class="media">
                                    <div class="media-left pull-left">
                                        <a href="#">
                                            <img class="media-object thumbnail" src={{ gravatar($reply->email) }} alt={{ $reply->name }}>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            {{ $reply->name }} 
                                            <span class="ulockd-bps-date pull-left"> ارسال شده در {{ edate($thread->created_at, 'Y M, d') }}</span> 
                                        </h4>

                                        <p>{{ $reply->body }}</p>
                                    </div>
                                </div>
                            </div>

                            @if($reply->parent)
                                <div class="ulockd-bps-second">
                                    <div class="media">
                                        <div class="media-right pull-left">
                                            <img class="media-object thumbnail" src={{ gravatar($reply->parent->email) }} alt={{ $reply->parent->name }}>
                                        </div>

                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                {{ $reply->parent->name }} 
                                                <span class="ulockd-bps-date pull-left"> Posted on {{ edate($thread->created_at, 'd M, Y') }}</span> 
                                            </h4>

                                            <p>{{ $reply->parent->body }}</p>
                                        </div>
                                    </div>
                                </div>  
                            @endif
                        @endforeach

                        <form name="contact_form" class="ulockd-bps-contact-form" action={{ route('reply.create', ['thread' => $thread->id]) }} method="POST" novalidate="novalidate">
                            @csrf
                            @method('POST')

                            <div class="ulockd-bps-contact-form">
                                <h2>@lang('message.leave-comment')</h2>
                            </div>

                            <div class="messages"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="form_name" value="{{ old('name') }}" name="name" class="form-control ulockd-form-bps required" placeholder=@lang('message.input.name') required data-error="Name is required." type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="form_email" value="{{ old('email') }}" name="email" class="form-control ulockd-form-bps required email" placeholder=@lang('message.input.email') required data-error="Email is required." type="email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>  

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea id="form_message" name="body" class="form-control ulockd-bps-textarea required" rows="5" placeholder=@lang('message.input.message') required data-error="Message is required." value={{ old('body') }}>
                                        </textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
                                        <button type="submit" class="btn btn-default btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">@lang('message.send')</button>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
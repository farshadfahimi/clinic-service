@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home" style="background-image: url({{ $treatment->poster }})">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $treatment->name }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('treatment', $treatment) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Inner Pages Main Section -->
<section class="ulockd-service-details">
    <div class="container-fluid">
        <div class="col-md-3">
            @foreach($treatment->variations as $item)
                <a class="btn btn-default ulockd-btn-thm2 ulockd-mrgn610" href="{{ route('treatments.show', ['treatment' => $item->id]) }}">{{ $item->name }}</a>
            @endforeach
        </div>

        <div class="col-md-9">
            <div class="row">
                {{-- start treatment content --}}
                <div class="col-md-12">
                    {!! $treatment->body !!}
                </div>
                {{-- end treatment content --}}
            </div>

            <div class="row ulockd-mrgn1225" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                {{-- start treatment price section --}}
                @if($pricing = $treatment->pricing)
                <div class="col-md-6">
                    <h3>@lang('message.treatment.prices')</h3>
                    <div style="text-align: justify">
                        {!! nl2br($pricing->description) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    @if($pricing->prices)
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>@lang('message.treatment.price_first_col')</th>
                                <th>@lang('message.treatment.price_second_col')</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($pricing->prices as $item)
                            <tr>
                                <td>{{ $item['title'] }}</td>
                                <td>{{ $item['price'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                @endif
                {{-- end treatment price section --}}

                {{-- start advice section --}}
            <div class="col-md-12 ulockd-mrgn1250">
                <form name="contact_form" class="ulockd-bps-contact-form" action={{ route('treatments.advice-request', ['id' => $treatment->id]) }} method="POST">
                    @csrf
                    @method('POST')

                    <div class="ulockd-bps-contact-form" dir="{{LaravelLocalization::getCurrentLocaleDirection()}}">
                        <h2>@lang('message.treatment.advice')</h2>
                    </div>

                    <div class="messages"></div>
                    <div class="row" dir="{{LaravelLocalization::getCurrentLocaleDirection()}}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.first_name')</label>
                                <input id="form_name" value="{{ old('first_name') }}" name="first_name" class="form-control ulockd-form-bps" required type="text">
                                @error('first_name')
                            <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.last_name')</label>
                                <input id="form_name" value="{{ old('last_name') }}" name="last_name" class="form-control ulockd-form-bps" required type="text">
                                @error('last_name')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.phone')</label>
                                <input id="form_email" value="{{ old('phone') }}" name="phone" class="form-control ulockd-form-bps" required type="text">
                                @error('phone')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.email')</label>
                                <input id="form_email" value="{{ old('email') }}" name="email" class="form-control ulockd-form-bps" type="email">
                                @error('email')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.country')</label>
                                <input id="form_email" value="{{ old('country') }}" name="country" class="form-control ulockd-form-bps" type="text" />
                                @error('country')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">@lang('message.input.city')</label>
                                <input id="form_email" value="{{ old('city') }}" name="city" class="form-control ulockd-form-bps" type="text" />
                                @error('city')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">@lang('message.input.message')</label>
                                <textarea id="form_message" name="description" class="form-control ulockd-bps-textarea" rows="5" value="{{ old('description') }}" required></textarea>
                                @error('description')
                                <div class="help-block with-errors">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group" dir="{{LaravelLocalization::getCurrentLocaleDirection()}}">
                                <button type="submit" class="btn btn-default btn-lg ulockd-btn-thm2">@lang('message.send')</button>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
            {{-- end advice section --}}
            </div>
        </div>

        <div class="col-md-12">
            @if($comments = $treatment->comments)
            {{-- start comments section --}}
            <div class="col-md-12 ulockd-mrgn1250">
                <div id="treatments_comments">
                    <swiper :options="{pagination: {el:'.swiper-pagination', dynamicBullets: true}, navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev'}}">
                        @foreach ($comments as $comment)
                        <swiper-slide>
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="{{ $comment->before_image }}" alt="{{ $treatment->name }}">
                                </div>
    
                                <div class="col-md-6">
                                    <img src="{{ $comment->after_image }}" alt="{{ $treatment->name }}">
                                </div>
    
                                <div class="col-md-12 ulockd-mrgn1225 text-center">
                                    {{ $comment->description }}
                                </div>
                            </div>
                        </swiper-slide>
                        @endforeach
                    </swiper>
                </div>
            </div>
            {{-- end comments section --}}
            @endif

            {{-- start faq questions --}}
            @if(count($questions))
            <div class="col-md-12 ulockd-mrgn1210">
                @component('components.faq', ['faqs' => $questions])   
                @endcomponent
            </div>
            @endif
            {{-- end faq questions --}}
        </div>
    </div>
</section>
@endsection
@extends('layouts.app')
@section('body')
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <h1 class="text-uppercase">@lang('message.pages.team')</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                        {{ Breadcrumbs::render('employees') }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <section class="ulockd-team">
        <div class="container">
            <div class="row">
                @foreach ($employees as $employee)
                    @if($loop->index <= 2)
                        @include('components.teams.team', ['team' => $employee, 'size' => 'col-xs-12 col-sm-6 col-md-4'])
                    @else
                        @include('components.teams.small', ['team' => $employee])
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
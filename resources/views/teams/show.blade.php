@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $employee->name }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('employee', $employee) }}
                </div>
            </div>
        </div>
    </div>
</div>


<section class="ulockd-team-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="ulockd-team-member">
                    <div class="team-thumb">
                        <img class="img-responsive img-whp" src="{{ $employee->avatar }}" alt="{{ $employee->name }}">
                        <div class="team-overlay">
                            <ul class="list-inline team-icon style2">
                                @if($employee->contact)
                                    @isset($employee->contact->facebook)
                                        <li>
                                            <a href={{ $employee->contact->facebook }} target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                    @endisset

                                    @isset($employee->contact->twitter)
                                        <li>
                                            <a href={{ $employee->contact->twitter }} target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                    @endisset

                                    @isset($employee->contact->telegram)
                                        <li>
                                            <a href={{ $employee->contact->telegram }} target="_blank">
                                                <i class="fa fa-telegram"></i>
                                            </a>
                                        </li>
                                    @endisset

                                    @isset($employee->contact->instagram)
                                        <li>
                                            <a href={{ $employee->contact->instagram }} target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                    @endisset
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="team-details" dir="auto">
                        <h3 class="member-name">{{ $employee->name }}</h3>
                        <h5 class="member-post">- {{ $employee->expert }}</h5>
                    </div>
                </div>
            </div>

            @if($employee->values)
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="thumbnail">
                    <div class="caption">
                        <ul class="list-unstyled ulockd-pesonar-info">
                            @foreach ($employee->values as $item)
                                <li><strong>{{ $item->title }} :</strong>{{ $item->value }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection
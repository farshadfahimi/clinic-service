@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatments-list') }}
    </div>

    <div class="container">
        <div class="text-right mb-3">
            @if(request()->has('archive'))
            <a href="{{ url()->current() }}" class="btn btn-info">مشاهده پیامهای خوانده نشده</a>
            @else
            <a href="{{ url()->current()."?archive" }}" class="btn btn-info">مشاهده تاریخچه</a>
            @endif
        </div>
        

        @foreach ($requests as $item)
        <div class="card mb-3" id="request-{{$item->id}}">
            <div class="card-header text-right">
                {{ $item->treatment->name }}
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12 mb-3">
                        <ul class="list-group list-group-horizontal text-right pr-0">
                            <li class="list-group-item">نام: {{ $item->first_name }}</li>
                            <li class="list-group-item">نام خانوادگی: {{ $item->last_name }}</li>
                            <li class="list-group-item">شماره تماس: {{ $item->phone }}</li>
                            <li class="list-group-item">ایمیل: {{ $item->email }}</li>
                            <li class="list-group-item">کشور: {{ $item->country }}</li>
                            <li class="list-group-item">شهر: {{ $item->city }}</li>
                        </ul>
                    </div>

                    <div class="col-12 {{ $item->lang == 'EN' ? 'text-left' : 'text-right' }}">
                        {{ $item->description }}
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-6 text-right">
                        زمان درخواست:  {{ $item->created_at->diffForHumans() }}
                    </div>

                    <div class="col-6 text-left text-info">
                    @if($item->checked_at == null)
                        <checked-request id="{{ $item->id }}"></checked-request>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        {{ $requests->links() }}
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/component.js') }}"></script>
@endpush
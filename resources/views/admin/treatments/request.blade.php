@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatments-list') }}
    </div>

    <div class="container">
        @foreach ($requests as $item)
        <div class="card mb-3">
            <div class="card-body text-right">
                <ul class="list-group pr-0">
                    <li class="list-group-item">
                        نام و نام خانوادگی: {{ $item->first_name }} {{ $item->last_name }}
                    </li>

                    <li class="list-group-item">
                        <p>شماره تماس: {{ $item->phone }}</p>
                        <p>ایمیل: {{ $item->email }}</p>
                    </li>

                    <li class="list-group-item">
                        <p>کشور: {{ $item->country }}</p>
                        <p>شهر: {{ $item->city }}</p>
                    </li>

                    <li class="list-group-item">
                        توضیحات:
                        <p dir="auto">
                            {!! nl2br($item->description) !!}
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        @endforeach

        {{ $requests->links() }}
    </div>
@endsection
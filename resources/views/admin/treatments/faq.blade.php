@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatment-faq', $treatment) }}
    </div>

    <div class="container" id="faq_container" data-type="treatments" data-id="{{ $treatment->id }}"></div>
@endsection

@push('js')
    <script src={{ mix('assets/js/admin/faq.js') }}></script>
@endpush
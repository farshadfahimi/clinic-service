@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatment-comment-create', $treatment) }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                ویرایش : {{ $comment->name }}
            </div>

            <div class="card-body">
                <form action="{{ route('admin.treatments.comments.update', ['id' => $treatment->id, 'comment_id' => $comment->id]) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">عنوان</label>
                                <input type="text" class="form-control" name="name" value="{{ $comment->name }}">
                                <small class="small text-muted">این متن صرفا برای اوپراتور بوده و به کاربر نمایش داده نمیشود</small>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" name="before" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left cover" for="cover">قبل</label>
                                <small class="d-block">سایز فایل: 480*480</small>
                            </div>

                            <img src="{{ $comment->before_image }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="after" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left page_cover" for="page_cover">بعد</label>
                                <small class="d-block">سایز فایل: 480*480</small>
                            </div>

                            <img src="{{ $comment->after_image }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>
                        
                        <div class="col-md-12 mt-5">
                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="description" id="body">{{ $comment->description }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatment-comment-create', $treatment) }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                افزودن نظر جدید
            </div>

            <div class="card-body">
                <form action="{{ route('admin.treatments.comments.store', ['id' => $treatment->id]) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">عنوان</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                <small class="small text-muted">این متن صرفا برای اوپراتور بوده و به کاربر نمایش داده نمیشود</small>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" name="before" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)" required>
                                <label class="custom-file-label text-left cover" for="cover">قبل</label>
                                <small>سایز فایل: 480*480</small>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="after" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)" required>
                                <label class="custom-file-label text-left page_cover" for="page_cover">بعد</label>
                                <small>سایز فایل: 480*480</small>
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="description" id="body">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatment-comment-list', $treatment) }}
    </div>

    <div class="container">
        @if($comments->count())
        <div class="text-center mb-3">
            <a class="btn btn-sm btn-primary" href="{{ route('admin.treatments.comments.create', ['id' => $treatment->id]) }}">ثبت نظر جدید: {{ $treatment->name }}</a>
        </div>

        <table class="table table-stripe text-right">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>عنوان</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach($comments as $comment)
                <tr>
                    <td>{{ fa_nums(++$loop->index) }}</td>
                    <td>{{ $comment->name }}</td>

                    <td class="d-inline-block">
                        <a href="{{ route('admin.treatments.comments.edit', ['id' => $treatment->id, 'comment_id' => $comment->id]) }}" class="btn btn-sm btn-warning ml-2">
                            <i class="fa fa-edit"></i>
                        </a>    

                        <form action="{{ route('admin.treatments.comments.delete', ['id' => $treatment->id, 'comment_id' => $comment->id]) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <div class="text-center">
                <a class="btn btn-sm btn-primary" href="{{ route('admin.treatments.comments.create', ['id' => $treatment->id]) }}">میخوای اولین نظر رو ثبت کنی برای: {{ $treatment->name }}</a>
            </div>
        @endif

        {{ $comments->links() }}
    </div>
@endsection
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-condition', $treatment) }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                مشاوره {{ $treatment->name }}
            </div>

            <div class="card-body">
                <div id="treatments_pricing" data-id="{{ $treatment->id }}"></div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/treatments_pricing.js') }}"></script>
@endpush
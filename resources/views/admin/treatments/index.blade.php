@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatments-list') }}
    </div>

    <div class="container" id="treatments_container"></div>
@endsection

@push('js')
    <script src={{ mix('assets/js/admin/treatments.js') }}></script>
@endpush
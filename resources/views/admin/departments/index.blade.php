@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-departments')}}
    </div>

    <div class="container" id="departments_container"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/departments.js') }}"></script>
@endpush 
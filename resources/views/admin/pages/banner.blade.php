@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-banner')}}
    </div>

    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 border-left mb-3">
                        <form action="{{ route('admin.settings.store_banner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-right">بنر اصلی صفحه پست ها:</p>
                            <img src="{{ $setting->posts_banner }}" alt="بنر صفحه پست ها" style="height:50px;width:100%;" class="mb-2">

                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="posts_banner" name="banner">
                                <label class="custom-file-label" for="posts_banner">انتخاب فایل</label>
                                <small class="text-right d-block mt-1"> سایز تصاویر ۱۹۲۰ * ۹۹۵ میباشد و فرمت (jpg)</small>
                            </div>
                            {{-- declear the file type --}}
                            <input type="hidden" name="type" value="posts">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </form>
                    </div>

                    <div class="col-md-6">
                        <form action="{{ route('admin.settings.store_banner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-right">بنر اصلی صفحه بیماری ها:</p>
                            <img src="{{ $setting->conditions_banner }}" alt="بنر صفحه پست ها" style="height:50px;width:100%;" class="mb-2">

                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="conditions_banner" name="banner">
                                <label class="custom-file-label" for="conditions_banner">انتخاب فایل</label>
                                <small class="text-right d-block mt-1"> سایز تصاویر ۱۹۲۰ * ۹۹۵ میباشد و فرمت (jpg)</small>
                            </div>
                            {{-- declear the file type --}}
                            <input type="hidden" name="type" value="conditions">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </form>
                    </div>

                    <div class="col-md-6 border-left">
                        <form action="{{ route('admin.settings.store_banner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-right">بنر اصلی صفحه درمان ها:</p>
                            <img src="{{ $setting->treatments_banner }}" alt="بنر صفحه پست ها" style="height:50px;width:100%;" class="mb-2">
                            
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="treatments_banner" name="banner">
                                <label class="custom-file-label" for="treatments_banner">انتخاب فایل</label>
                                <small class="text-right d-block mt-1">سایز تصاویر ۱۹۲۰ * ۹۹۵ میباشد و فرمت (jpg)</small>
                            </div>
                            {{-- declear the file type --}}
                            <input type="hidden" name="type" value="treatments">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </form>
                    </div>

                    <div class="col-md-6 border-left">
                        <form action="{{ route('admin.settings.store_banner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-right">بنر اصلی صفحه تیم ما:</p>
                            <img src="{{ $setting->team_banner }}" alt="بنر صفحه تیم ما" style="height:50px;width:100%;" class="mb-2">
                            
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="treatments_banner" name="banner">
                                <label class="custom-file-label" for="treatments_banner">انتخاب فایل</label>
                                <small class="text-right d-block mt-1">سایز تصاویر ۱۹۲۰ * ۹۹۵ میباشد و فرمت (jpg)</small>
                            </div>
                            {{-- declear the file type --}}
                            <input type="hidden" name="type" value="team">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </form>
                    </div>

                    <div class="col-md-6 border-left">
                        <form action="{{ route('admin.settings.store_banner') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-right">بنر اصلی صفحه خدمات ما:</p>
                            <img src="{{ $setting->services_banner }}" alt="بنر صفحه خدمات ما" style="height:50px;width:100%;" class="mb-2">
                            
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="treatments_banner" name="banner">
                                <label class="custom-file-label" for="treatments_banner">انتخاب فایل</label>
                                <small class="text-right d-block mt-1">سایز تصاویر ۱۹۲۰ * ۹۹۵ میباشد و فرمت (jpg)</small>
                            </div>
                            {{-- declear the file type --}}
                            <input type="hidden" name="type" value="services">
                            <input type="submit" class="btn btn-sm btn-primary" value="ذخیره">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
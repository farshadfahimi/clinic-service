@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-menu-maker')}}
    </div>

    <div class="container">
        <menu-generator></menu-generator>
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/component.js') }}"></script>
@endpush
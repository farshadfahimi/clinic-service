@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-home-slider')}}
    </div>

    <div class="container">
        <div class="card">
            <div class="card-header">
                افزودن اسلاید جدید
            </div>

            <div class="card-body">
                <form action="{{ route('admin.settings.store-main-slider') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row text-right">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" value="{{ @old('title') }}">
                            @error('title')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="description">توضیح</label>
                                <input type="text" class="form-control" name="description" value="{{ @old('description') }}">
                                @error('description')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-form">
                                <label for="lang">انتخاب زبان</label>
                                <select name="lang" id="lang" class="custom-select">
                                    <option value="FA">فارسی</option>
                                    <option value="EN">انگلیسی</option>
                                    <option value="AR">عربی</option>
                                </select>
                                @error('lang')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <label for="">آپلود تصویر</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" id="image">
                                <label class="custom-file-label" for="image">آپلود تصویر</label>
                                <small>سایز تصویر ۷۰۰*۱۹۲۰</small>
                              </div>
                              @error('image')
                                <small class="text-danger">{{$message}}</small>
                              @enderror
                        </div>

                        <div class="col-md-6 mb-2">
                            <label for="">آپلود تصویر موبایل</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="mobile_image" id="mobile_image">
                                <label class="custom-file-label" for="mobile_image">آپلود تصویر</label>
                                <small>سایز تصویر ۵۰۰*۷۶۸</small>
                              </div>
                              @error('image')
                                <small class="text-danger">{{$message}}</small>
                              @enderror
                        </div>

                        <div class="col-md-12">
                            <input type="submit" value="ذخیره" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="update_slide" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ویرایش اسلایدر</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <form method="POST" action="{{ route('admin.settings.update-main-slider') }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" id="slide_id" name="id">
                        <div class="modal-body">
                            <div class="row text-right">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="slide_title">عنوان</label>
                                        <input type="text" id="slide_title" class="form-control" name="title"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="slide_description">توضیحات</label>
                                        <input type="text" class="form-control" id="slide_description" name="description" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_links">لینک اول</label>
                                        <input type="text" class="form-control" id="first_links" name="links[]"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="second_links">لینک دوم</label>
                                        <input type="text" class="form-control" id="second_links" name="links[]" />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="slide_lang">زبان</label>
                                        <select id="slide_lang" name="lang" class="custom-select">
                                            <option id="LANG_FA" value="FA">فارسی</option>
                                            <option id="LANG_EN" value="EN">انگلیسی</option>
                                            <option id="LANG_AR" value="AR">عربی</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="image">آپلود تصویر</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image"/>
                                        <label class="custom-file-label" for="image">آپلود تصویر</label>
                                        <small>سایز تصویر ۷۰۰*۱۹۲۰</small>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-2">
                                    <label for="">آپلود تصویر موبایل</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="mobile_image" id="mobile_image">
                                        <label class="custom-file-label" for="mobile_image">آپلود تصویر</label>
                                        <small>سایز تصویر ۵۰۰*۷۶۸</small>
                                      </div>
                                      @error('image')
                                        <small class="text-danger">{{$message}}</small>
                                      @enderror
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary ml-2" data-dismiss="modal">انصراف</button>
                            <input type="submit" class="btn btn-primary" value="ذخیره">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="sliders_container"></div>
    </div>
@endsection

@push('js')
<script src="{{ mix('assets/js/admin/main_slider.js') }}"></script>
@endpush
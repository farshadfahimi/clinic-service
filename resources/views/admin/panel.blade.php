@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="alert alert-info text-center">به پنل کاربری خود خوش آمدید</div>

        <div class="row">
            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد کاربران

                        <div class="badge badge-primary">
                            {{ $users }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد پست

                        <div class="badge badge-primary">
                            {{ $threads }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد بیماری

                        <div class="badge badge-primary">
                            {{ $conditions }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد راه درمان

                        <div class="badge badge-primary">
                            {{ $treatments }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد کامنت

                        <div class="badge badge-primary">
                            {{ $comments }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد نظرات

                        <div class="badge badge-primary">
                            {{ $replies }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد اخبار

                        <div class="badge badge-primary">
                            {{ $news }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3 text-right">
                <div class="card">
                    <div class="card-body">
                        تعداد درخواست‌ها

                        <div class="badge badge-primary">
                            {{ $requests }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
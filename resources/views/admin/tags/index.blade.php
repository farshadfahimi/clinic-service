@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-tags-index') }}
    </div>

    <div class="container">
        <div id="tags_container"></div>
    </div>
@endsection

@push('js')
<script src="{{ mix('assets/js/admin/tags.js') }}"></script>
@endpush
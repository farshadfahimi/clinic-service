@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('create-thread', $channel) }}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">آپلود تصویر</h5>

                <button type="button" class="btn" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="card-body text-right bg-light">
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.channels.threads.store', $channel->id) }}">
                @csrf
                @method('POST')
                
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        
                    </div>

                    <div class="col-md-6">
                        <div class="custom-file">
                            <input type="file" name="cover" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)">
                            <label class="custom-file-label text-left cover" for="cover">انتخاب کاور</label>
                            <small>سایز فایل: ۴۴۲*۶۰۵</small>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="page_cover" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)">
                            <label class="custom-file-label text-left page_cover" for="page_cover">کاور صفحه</label>
                            <small>سایز فایل: ۹۹۵*۱۹۲۰</small>
                        </div>
                    </div>

                    <div class="col-md-12 my-3">
                        <div class="form-group">
                            <label for="">برچسب‌ها</label>
                            <input type="text" class="form-control" name="tag" value="{{ old('tag') }}" />
                            <small>هر برچسب را میتوان با ' - ' از سایر جدا کرد</small>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="body">محتوا</label>

                        <textarea name="body" id="body" class="form-control body">{{ old('body') }}</textarea>
                    </div>  

                    <div class="col-md-12 mt-3">
                        <button class="btn btn-success" type="submit" id="submit">
                            ذخیره
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush


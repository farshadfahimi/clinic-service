@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('channel', $channel) }}
    </div>

    <div class="container">
        <a href="{{ route('admin.channels.threads.create', ['id' => $channel->id]) }}" class="btn btn-sm btn-primary d-block">ایجاد پست</a>
    </div>

    <div id="threads_container" class="container" data-id="{{ request()->id }}"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/threads.js') }}"></script>
@endpush


@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-thread-replies', $channel, $thread) }}
    </div>

    <div id="replies_container" class="container" data-channel="{{ $channel->id }}" data-thread="{{ $thread->id }}"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/replies.js') }}"></script>
@endpush


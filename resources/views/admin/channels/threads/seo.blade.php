@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('admin-thread-seo', $channel, $thread) }}
    </div>
    
    <div class="container">
        <div class="card-body text-right bg-light">
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.channels.threads.seo', ['id' => $channel->id,'thread' => $thread->id]) }}">
                @csrf
                @method('PATCH')
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" value="{{ $thread->seo ? $thread->seo->title : old('title') }}">
                        </div>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">کلمات کلیدی</label>
                            <input type="text" class="form-control" name="keywords" value="{{ $thread->seo ? $thread->seo->keywords : old('keywords') }}">
                        </div>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">توضیحات</label>
                            <input type="text" class="form-control" name="description" value="{{ $thread->seo ? $thread->seo->description : old('description') }}">
                        </div>
                    </div>

                    <div class="col-md-12 mt-3">
                        <button class="btn btn-success" type="submit" id="submit">
                            ذخیره
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush


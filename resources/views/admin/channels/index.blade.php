@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('channels') }}
    </div>
    
    <div id="channels_container" class="container"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/channels.js') }}"></script>
@endpush
@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('channels') }}
    </div>
    
    <div class="container">
        <div class="accordion">
        @foreach ($replies as $item)
        <div class="card mb-3">
            <div class="card-header text-right">
                <a href="{{ route('threads-show', ['channel'  =>  $item->thread->channel_id, 'thread' => $item->thread->id]) }}" target="_blank">{{ $item->thread->title }}</a>

                <button type="button" class="btn btn-sm btn-outline-info float-left" data-toggle="collapse" href="#collapse-{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-reply"></i>
                </button>
            </div>

            <div class="collapse" id="collapse-{{ $item->id }}">
                <form action="{{ route('admin.replies.store', ['reply' => $item->id]) }}" method="POST" class="py-3">
                    @csrf

                    <div class="col-12 text-right">
                        <div class="form-group">
                            <label for="">پاسخ شما</label>
                            <textarea name="body" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-sm btn-outline-primary">ذخیره</button>
                    </div>
                </form>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <ul class="form-group text-right pr-0">
                            <li class="list-group-item">نام: {{ $item->name }}</li>
                            <li class="list-group-item">ایمیل: {{ $item->email }}</li>
                        </ul>
                    </div>

                    <div class="col-6 text-justify">
                        {!! $item->body !!}
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-6 text-right">
                        زمان ثبت نظر:  {{ $item->created_at->diffForHumans() }}
                    </div>

                    <div class="col-6">
                        <form action="{{ route('admin.replies.delete', ['reply' => $item->id]) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <i class="fa fa-trash-alt"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        </div>

        {{ $replies->links() }}
    </div>
@endsection
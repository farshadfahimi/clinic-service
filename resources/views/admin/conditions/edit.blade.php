@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-condition', $condition) }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                ویرایش {{ $condition->name }}
            </div>

            <div class="card-body">
                <form action="{{ route('admin.conditions.update', ['id' => $condition->id]) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">نام</label>
                                <input type="text" name="name" class="form-control" value="{{ $condition->name }}">
                                @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="parent_id">درمان اصلی</label>
                                <select name="parent_id" id="parent_id" class="custom-select">
                                    <option value="0">هیچکدام</option>
                                    @foreach ($conditions as $item)
                                        <option value="{{ $item->id }}" {{ $item->id == $condition->parent_id ? 'selected' : null }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @error('parent_id')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left cover" for="cover">انتخاب کاور</label>
                                <small>سایز فایل: ۴۴۲*۶۰۵</small>
                            </div>
    
                            <img src="{{ $condition->cover }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>
    
                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="page_cover" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left page_cover" for="page_cover">کاور صفحه</label>
                                <small>سایز فایل: ۹۹۵*۱۹۲۰</small>
                            </div>
    
                            <img src="{{ $condition->poster }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>

                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="body">{{ $condition->body }}</textarea>
                                @error('body')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label for="" class="h3">علت ایجاد</label>
                                <input type="text" class="form-control" name="reason_title" value="{{ $condition->reason_title }}">
                            </div>

                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="reason_description">{{ $condition->reason_description }}</textarea>
                                @error('reason_description')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label for="" class="h3">علائم</label>
                                <input type="text" class="form-control" name="signs_title" value="{{ $condition->signs_title }}">
                            </div>

                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="signs_description">{{ $condition->signs_description }}</textarea>
                                @error('signs_description')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label for="" class="h3">راهکار های درمان</label>
                                <input type="text" class="form-control" name="approach_title" value="{{ $condition->approach_title }}">
                            </div>

                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="approach_description">{{ $condition->approach_description }}</textarea>
                                @error('approach_description')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <input type="submit" id="submit" class="btn btn-primary" value="ذخیره">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush
@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('admin-condition-seo', $condition) }}
    </div>
    
    <div class="container">
        <div class="card-body text-right bg-light">
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.conditions.seo', $condition->id) }}">
                @csrf
                @method('PATCH')
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" value="{{ $condition->seo ? $condition->seo->title : old('title') }}">
                        </div>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">کلمات کلیدی</label>
                            <input type="text" class="form-control" name="keywords" value="{{ $condition->seo ? $condition->seo->keywords : old('keywords') }}">
                        </div>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">توضیحات</label>
                            <input type="text" class="form-control" name="description" value="{{ $condition->seo ? $condition->seo->description : old('description') }}">
                        </div>
                    </div>

                    <div class="col-md-12 mt-3">
                        <button class="btn btn-success" type="submit" id="submit">
                            ذخیره
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
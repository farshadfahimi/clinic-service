@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-condition-faq', $condition) }}
    </div>

    <div class="container" id="faq_container" data-type="conditions" data-id="{{ $condition->id }}"></div>
@endsection

@push('js')
    <script src={{ mix('assets/js/admin/faq.js') }}></script>
@endpush
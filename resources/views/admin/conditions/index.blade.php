@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-conditions-list') }}
    </div>

    <div class="container" id="conditions_container"></div>
@endsection


@push('js')
    <script src={{ mix('assets/js/admin/conditions.js') }}></script>
@endpush
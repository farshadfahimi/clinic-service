@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-news-edit') }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                ویرایش {{ $news->title }}
            </div>

            <div class="card-body">
                <form action="{{ route('admin.news.update', ['id' => $news->id]) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">عنوان</label>
                                <input type="text" name="title" class="form-control" value="{{ $news->title }}">
                                @error('title')
                                    <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">زبان</label>
                                <select name="lang" class="custom-select">
                                    <option value="FA" {{ $news->lang == 'FA' ? 'selected' : '' }}>فارسی</option>
                                    <option value="EN" {{ $news->lang == 'EN' ? 'selected' : '' }}>انگلیسی</option>
                                    <option value="AR" {{ $news->lang == 'AR' ? 'selected' : '' }}>عربی</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" name="cover" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left cover" for="cover">انتخاب کاور</label>
                                <small>سایز فایل: ۴۴۲*۶۰۵</small>
                            </div>
    
                            <img src="{{ $news->cover }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>
    
                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="page_cover" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)">
                                <label class="custom-file-label text-left page_cover" for="page_cover">کاور صفحه</label>
                                <small>سایز فایل: ۹۹۵*۱۹۲۰</small>
                            </div>
    
                            <img src="{{ $news->poster }}" alt="تصویر کوچک" class="img-thumbnail" style="height:250px">
                        </div>

                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label for="body">توضیحات</label>
                                <textarea class="form-control body" name="body" id="body">{{ $news->body }}</textarea>
                                @error('body')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <input type="submit" id="submit" class="btn btn-primary" value="ذخیره">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush
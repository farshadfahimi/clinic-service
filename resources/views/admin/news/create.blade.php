@extends('layouts.admin')

@section('content')
    <div class="container">
    {{ Breadcrumbs::render('admin-news-create') }}
    </div>

    <div class="container">
        <div class="card-body text-right bg-light">
            <form method="post" enctype="multipart/form-data" action="{{ route('admin.news.store') }}">
                @csrf
                @method('POST')
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">زبان</label>
                            <select name="lang" class="custom-select">
                                <option value="FA" {{ old('lang') == 'FA' ? 'selected' : '' }}>فارسی</option>
                                <option value="EN" {{ old('lang') == 'EN' ? 'selected' : '' }}>انگلیسی</option>
                                <option value="AR" {{ old('lang') == 'AR' ? 'selected' : '' }}>عربی</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="custom-file">
                            <input type="file" name="cover" class="custom-file-input" id="cover" value="کاور" onchange="prograssBar(this)">
                            <label class="custom-file-label text-left cover" for="cover">انتخاب کاور</label>
                            <small>سایز فایل: ۴۴۲*۶۰۵</small>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="page_cover" id="page_cover" value="عکس بالای صفحه" onchange="prograssBar(this)">
                            <label class="custom-file-label text-left page_cover" for="page_cover">کاور صفحه</label>
                            <small>سایز فایل: ۹۹۵*۱۹۲۰</small>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="body">محتوا</label>

                        <textarea name="body" id="body" class="form-control body">{{ old('body') }}</textarea>
                    </div>  

                    <div class="col-md-12 mt-3">
                        <button class="btn btn-success" type="submit" id="submit">
                            ذخیره
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
    <script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush


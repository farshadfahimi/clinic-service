@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-treatments-list') }}
    </div>

    <div class="container">
        <div class="card">
            <div class="card-header text-right">
                لیست اخبار

                <a href="{{ route('admin.news.create') }}" class="btn btn-sm btn-primary float-left">+ خبر جدید</a>
            </div>

            <div class="card-body">
                @if($news->count())
                <table class="table table-bordered">
                    <thead>
                        <tr class="text-right">
                            <th>#</th>
                            <th>عنوان</th>
                            <th>زبان</th>
                            <th></th>
                        </tr>
                    </thead>
        
                    <tbody>
                        @foreach ($news as $item)
                        <tr class="text-right">
                            <td>{{ fa_nums(++$loop->index) }}</td>
                            <td dir="auto">{{ $item->title }}</td>
                            <td>{{ $item->lang }}</td>
                            <td class="d-flex">
                                <a href="{{ route('admin.news.edit', ['id' => $item->id]) }}" class="btn btn-sm bg-transparent text-warning">
                                    <i class="fa fa-edit"></i>ویرایش 
                                </a>

                                <form action="{{ route('admin.news.delete', ['id' => $item->id]) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm bg-transparent text-danger" type="submit">
                                        <i class="fa fa-trash-alt"></i>
                                        حذف
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="card-footer">
                {{ $news->links() }}
            </div>
            @else
                <div class="alert alert-info text-center">اولین خبر را ثبت کنید</div>
            @endif
        </div>
    </div>
@endsection
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-users-list') }}
    </div>

    <div class="container">
        <div class="card">
            <div class="card-header text-right">
                لیست کاربران
            </div>
    
            <div class="card-body">
                <table class="table table-striped text-right">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>نام</th>
                            <th>ایمیل</th>
                            <th></th>
                        </tr>
                    </thead>
            
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a href="{{ route('admin.users.edit', ['user' => $user->id]) }}" class="btn btn-sm btn-outline-warning">ویرایش</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
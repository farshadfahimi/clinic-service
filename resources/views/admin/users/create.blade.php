@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-users-create') }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                افزودن کاربر جدید
            </div>

            <div class="card-body">
                <form action="{{ route('admin.users.store') }}" class="mt-4" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">نام</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @error('name')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">ایمیل</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">رمز عبور</label>
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                                @error('password')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">تغییر رمز عبور</label>
                                <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                @error('password_confirmation')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <button class="btn btn-sm btn-outline-primary">ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
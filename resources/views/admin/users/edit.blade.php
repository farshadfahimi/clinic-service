@extends('layouts.admin')
@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-users-edit', $user) }}
    </div>

    <div class="container">
        <div class="card text-right">
            <div class="card-header">
                اطلاعات کاربری {{ $user->name }}
            </div>

            <div class="card-body">
                <ul class="nav nav-pills pr-0" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">ویرایش اطلاعات کاربری</a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">مجوزها</a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">ویرایش رمز عبور</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form action="{{ route('admin.users.update', ['user' => $user->id]) }}" class="mt-4" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">نام</label>
                                        <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">ایمیل</label>
                                        <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button class="btn btn-sm btn-outline-primary">بروزرسانی</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <form action="{{ route('admin.users.roles', ['user' => $user->id]) }}" method="POST">
                            @csrf
                            <div class="row">
                                @foreach ($roles as $role)
                                <div class="col-md-4">
                                    <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="roles" value="{{ $role->name }}" id="{{ $role->name }}" {{ $userRoles->contains($role->name) ? 'checked' : '' }}>

                                        <label class="form-check-label mr-3" for="{{ $role->name }}">
                                            {{ $role->description }}
                                        </label>
                                    </div>
                                </div>
                                @endforeach

                                <div class="col-12 mt-5">
                                    <button class="btn btn-sm btn-outline-primary">بروزرسانی مجوزها</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <form action="{{ route('admin.users.security', ['user' => $user->id]) }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">رمز عبور</label>
                                        <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">تکرار رمز عبور</label>
                                        <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn btn-sm btn-outline-primary">ذخیره</button>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
@endsection
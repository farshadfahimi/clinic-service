@extends('layouts.admin')

@section('content')
    <div class="container">
            {{ Breadcrumbs::render('admin-employees') }}
    </div>

    <div class="container" id="employees_container"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/employees.js') }}"></script>
@endpush
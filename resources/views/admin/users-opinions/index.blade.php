@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-users-opinions') }}
    </div>

    <div class="container">
      
      <div class="card">
        <div class="card-header text-right">
          لیست نظرات
          <a class="btn btn-primary float-left" href="{{ route('admin.users-opinions.create') }}">افزودن نظر جدید</a>
        </div>
        <div class="card-body text-right">
          <div class="container">
            @if($opinions->count())
            <table class="table table-striped thead-dark">
              <thead>
                <tr>
                  <th>#</th>
                  <th>نام</th>
                  <th>زبان</th>
                  <th>توضیحات</th>
                  <th></th>
                </tr>
              </thead>
      
              <tbody>
                @foreach ($opinions as $item)
                <tr>
                  <td>{{ fa_nums(++$loop->index) }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->language }}</td>
                  <td>{{ str_limit($item->body, 250) }}</td>
                  <td class="d-flex">
                    <a class="btn btn-sm btn-warning" href="{{ route('admin.users-opinions.edit', ['id' => $item->id]) }}">
                      ویرایش
                      <i class="fa fa-edit"></i>
                    </a>

                    <form method="POST" action="{{ route('admin.users-opinions.destroy', ['id' => $item->id]) }}" class="mr-2">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit">
                        حذف
                        <i class="fa fa-trash-alt"></i>
                      </button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>

            <div class="mt-3">
              {{ $opinions->links() }}
            </div>
            @else
              <div class="alert alert-info">
                اطلاعاتی ثبت نشده است
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
@endsection
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-users-opinions-create') }}
    </div>

    <div class="container">
      <div class="card">
        <div class="card-body text-right">
          <div class="container">
            <form method="post" class="row" action="{{ route('admin.users-opinions.update', ['id' => $opinion->id]) }}">
              @csrf
              @method('PUT')

              <div class="col-md-6">
                <div>
                  <label for="">زبان</label>
                  <select name="lang" class="form-control" id="" required>
                    <option value="">انتخاب کنید</option>
                    <option value="FA" {{ $opinion->lang == 'FA' ? 'selected' : '' }} >فارسی</option>
                    <option value="EN" {{ $opinion->lang == 'EN' ? 'selected' : '' }}>انگلیسی</option>
                    <option value="AR" {{ $opinion->lang == 'AR' ? 'selected' : '' }}>عربی</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="">نام</label>
                  <input type="text" name="name" class="form-control" required value="{{ $opinion->name }}" />
                </div>
              </div>

              <div class="col-12">
                <label for="">توضیحات</label>
                <textarea rows="5" class="form-control" name="body">{{ $opinion->body }}</textarea>
              </div>

              <div class="col-12 mt-3">
                <button type="submit" class="btn btn-success">ذخیره</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
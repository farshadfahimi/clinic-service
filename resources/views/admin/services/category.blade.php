@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-categories-services')}}
    </div>

    <div class="container" id="services_categories_container"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/services_categories.js') }}"></script>
@endpush 
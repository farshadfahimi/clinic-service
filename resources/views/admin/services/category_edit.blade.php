@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-categories-services')}}
    </div>

    <div class="container">
        <div class="card-body bg-light">
            <form action="{{ route('admin.services-categories.update', ['id' => $category->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row text-right">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">عنوان</label>
                            <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>آیکون</label>
                        <div class="mb-3">
                            <div class="custom-file">
                                <input type="file" name="icon" class="custom-file-input" id="icon"/>
                                <label class="custom-file-label text-left" htmlFor="icon">انتخاب فایل</label>
                                <small class="text-right d-block mt-1"> سایز تصاویر ۶۴ * ۶۴ میباشد و فرمت (png)</small>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lang">زبان</label>
                            <select name="lang" id="" class="custom-select">
                                <option value="fa" {{ $category->lang == "FA" ? 'selected' : '' }}>فارسی</option>
                                <option value="ar" {{ $category->lang == "AR" ? 'selected' : '' }}>عربی</option>
                                <option value="en" {{ $category->lang == "EN" ? 'selected' : '' }}>انگلیسی</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="desc">توضیحات</label>
                        <input type="text" name="description" class="form-control" value="{{ $category->description }}">
                    </div>

                    <div class="col-md-12">
                        <label for="body">محتوا</label>
                        <textarea name="body" id="body" class="body">
                        {{ $category->body }}
                        </textarea>
                    </div>

                    <div class="col-md-12 mt-3">
                        <button type="submit" class="btn btn-primary">ذخیره</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
<script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush 
@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-settings')}}
    </div>

    <div class="container" id="settings_container"></div>
@endsection

@push('js')
    <script src="{{ mix('assets/js/admin/setting.js') }}"></script>
@endpush
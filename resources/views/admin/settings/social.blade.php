@extends('layouts.admin')

@section('content')
    <div class="container">
        {{Breadcrumbs::render('admin-social')}}
    </div>

    <div class="container">
        <div class="card">
            <div class="card-header text-right">
                شبکه اجتماعی فارسی زبان
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.settings.store-social') }}">
                    @csrf
                    @method('POST')

                    <div class="row text-right">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="instagram" value="{{ $socialFa && isset($socialFa->values->instagram) ? $socialFa->values->instagram : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-facebook-f"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="facebook" value="{{ $socialFa && isset($socialFa->values->facebook) ? $socialFa->values->facebook : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="twitter" value="{{ $socialFa && isset($socialFa->values->twitter) ? $socialFa->values->twitter : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="youtube" value="{{ $socialFa && isset($socialFa->values->youtube) ? $socialFa->values->youtube : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-whatsapp"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="whatsapp" value="{{ $socialFa && isset($socialFa->values->whatsapp) ? $socialFa->values->whatsapp  : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-pinterest-p"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="pinterest" value="{{ $socialFa && isset($socialFa->values->aparat) ? $socialFa->values->aparat : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-telegram-plane"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="telegram" value="{{ $socialFa && isset($socialFa->values->telegram) ? $socialFa->values->telegram : '' }}">
                            </div>
                        </div>

                        <input type="text" class="form-control" name="lang" value="FA" hidden>

                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header text-right">
                شبکه اجتماعی عربی زبان
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.settings.store-social') }}">
                    @csrf
                    @method('POST')

                    <div class="row text-right">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="instagram" value="{{ $socialAr && isset($socialAr->values->instagram) ? $socialAr->values->instagram : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-facebook-f"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="facebook" value="{{ $socialAr && isset($socialAr->values->facebook) ? $socialAr->values->facebook : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="twitter" value="{{ $socialAr && isset($socialAr->values->twitter) ? $socialAr->values->twitter : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="youtube" value="{{ $socialAr && isset($socialAr->values->youtube) ? $socialAr->values->youtube : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-whatsapp"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="whatsapp" value="{{ $socialAr && isset($socialAr->values->whatsapp) ? $socialAr->values->whatsapp  : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-pinterest-p"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="pinterest" value="{{ $socialAr && isset($socialAr->values->aparat) ? $socialAr->values->aparat : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-telegram-plane"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="telegram" value="{{ $socialAr && isset($socialAr->values->telegram) ? $socialAr->values->telegram : '' }}">
                            </div>
                        </div>

                        <input type="text" class="form-control" name="lang" value="AR" hidden>

                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header text-right">
                شبکه اجتماعی انگلیسی زبان
            </div>
            
            <div class="card-body">
                <form method="POST" action="{{ route('admin.settings.store-social') }}">
                    @csrf
                    @method('POST')

                    <div class="row text-right">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="instagram" value="{{ $socialEn && isset($socialEn->values->instagram) ? $socialEn->values->instagram : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-facebook-f"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="facebook" value="{{ $socialEn && isset($socialEn->values->facebook) ? $socialEn->values->facebook : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="twitter" value="{{ $socialEn && isset($socialEn->values->twitter) ? $socialEn->values->twitter : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="youtube" value="{{ $socialEn && isset($socialEn->values->youtube) ? $socialEn->values->youtube : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-whatsapp"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="whatsapp" value="{{ $socialEn && isset($socialEn->values->whatsapp) ? $socialEn->values->whatsapp  : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-pinterest-p"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="pinterest" value="{{ $socialEn && isset($socialEn->values->aparat) ? $socialEn->values->aparat : '' }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="fab fa-telegram-plane"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" dir="auto" name="telegram" value="{{ $socialEn && isset($socialEn->values->telegram) ? $socialEn->values->telegram : '' }}">
                            </div>
                        </div>

                        <input type="text" class="form-control" name="lang" value="EN" hidden>

                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
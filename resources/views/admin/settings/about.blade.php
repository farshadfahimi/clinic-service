@extends('layouts.admin')

@section('content')
    <div class="container">
        {{ Breadcrumbs::render('admin-about') }}
    </div>

    <div class="container">
        <form class="card p-2" action="{{ route('admin.settings.about-page') }}" method="POST">
            @csrf
            <div class="form-group text-right">
                <label for="">متن فارسی</label>
                <textarea name="fa" id="fa" class="form-control body">{{ $fa ? $fa->values : '' }}</textarea>
            </div>

            <div class="form-group text-right">
                <label for="">متن انگلیسی</label>
                <textarea name="en" id="en" class="form-control body">{{ $en ? $en->values : '' }}</textarea>
            </div>

            <div class="form-group text-right">
                <label for="">متن عربی</label>
                <textarea name="ar" id="ar" class="form-control body">{{ $ar ? $ar->values : '' }}</textarea>
            </div>

            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">ذخیره تغییرات</button>
            </div>
        </form>
    </div>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/xv3yszddrqi786uyfpl0vjjrgokm86a719783eo4weku3u6k/tinymce/5/tinymce.min.js"></script>
<script src="{{ mix('assets/js/admin/create_thread.js') }}"></script>
@endpush
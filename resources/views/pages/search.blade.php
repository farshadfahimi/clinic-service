@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">@lang('message.search')</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="{{ route('/') }}"> @lang('message.pages.home') </a></li>
                        <li><a href="#"> > </a></li>
                        <li> <a href="{{ route('search') }}"> @lang('message.search') </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Inner Pages Main Section -->
<section class="ulockd-contact-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <div class="ulockd-cp-title">
                    <h2 class="text-uppercase">@lang('message.search')</h2>
                </div>
            </div>
        </div>

        <div class="row">
            @if($data->count())
            @foreach ($data as $item)
            <div class="col-md-12 ulockd-mrgn620">
                @component('components.search_item', ['item' => $item])
                @endcomponent
            </div>
            @endforeach
            @else
            <div class="alert h2 text-center">
                {{ __('message.notfound') }}
            </div>
            @endif

            {{ $data->appends(['q' => request()->q])->links() }}
        </div>
    </div>
</section>
@endsection
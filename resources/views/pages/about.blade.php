@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">@lang('message.pages.about')</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="{{ route('/') }}"> @lang('message.pages.home') </a></li>
                        <li><a href="#"> > </a></li>
                        <li> <a href="{{ route('about') }}"> @lang('message.pages.about') </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Our About -->
<section class="ulockd-about2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-7">
                <div class="ulock-about">
                    <h1 class="title-bottom ulockd-mrgn630"> @lang('message.welcome.about') <span class="text-thm2"> @lang('message.app') </span></h1>
                </div>

                @if($about)
                    {!! $about->values !!}    
                @endif
            </div>

            <div class="col-sm-12 col-md-5">
                <div class="ulockd-about-box ulockd-mrgn1260">
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home" style="background-image: url({{ $setting->team_banner }})">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">@lang('message.welcome.team')</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('teamus') }}
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ulockd-team">
    <div class="container">
        @if ($teams->count())
        <div class="row">
            @foreach ($teams as $team)
                @include('components.teams.team', ['team' => $team, 'size' => 'col-md-4'])
            @endforeach
        </div>
        @endif
    </div>
</section>
@endsection
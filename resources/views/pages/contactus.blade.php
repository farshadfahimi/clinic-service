@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">@lang('message.pages.contact')</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="{{ route('/') }}"> @lang('message.pages.home') </a></li>
                        <li><a href="#"> > </a></li>
                        <li> <a href="{{ route('contactus') }}"> @lang('message.pages.contact') </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Inner Pages Main Section -->
<section class="ulockd-contact-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <div class="ulockd-cp-title">
                    <h2 class="text-uppercase">@lang('message.pages.contact')</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="contact-info">
                    <div class="contact-details one">
                        <ul class="contact-place one">
                            <li>
                                <span class="flaticon-black-back-closed-envelope-shape"> 
                                    <small>
                                        <i class="fa fa-envelope"></i>
                                        {{ $setting->values->email }}
                                    </small>
                                </span>
                            </li>
                            
                            <li>
                                <span class="flaticon-old-handphone"> 
                                    <i class="fa fa-phone"></i>
                                    <small> {{ $setting->values->phone }} </small>
                                </span>
                            </li>

                            <li>
                                <span class="flaticon-map-marker"> 
                                    <i class="fa fa-map-marker"></i>
                                    <small>{{ $setting->values->address }}</small>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="ulockd-contact-form">
                    <form id="contact_form" name="contact_form" class="contact-form" action="includes/contact.php" method="post" novalidate="novalidate">
                        <div class="messages"></div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="form_name" name="form_name" class="form-control ulockd-form-fg required" placeholder="@lang('message.input.name')" type="text">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="form_email" name="form_email" class="form-control ulockd-form-fg required email" placeholder="@lang('message.input.email')" type="email">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="form_phone" name="form_phone" class="form-control ulockd-form-fg required" placeholder="@lang('message.input.phone')" type="text">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input id="form_subject" name="form_subject" class="form-control ulockd-form-fg required" placeholder="Subject" type="text">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea id="form_message" name="form_message" class="form-control ulockd-form-tb required" rows="12" placeholder="@lang('message.input.message')"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group ulockd-contact-btn">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
                                    <button type="submit" class="btn btn-default ulockd-btn-thm2" data-loading-text="Getting Few Sec...">@lang('message.send')</button>
                                </div>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ulockd-padz">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-google-map">
                    <div class="" id="map-location" style="height: 312px;"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
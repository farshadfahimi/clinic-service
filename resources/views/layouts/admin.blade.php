<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@lang('message.app')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/logo/favicon.ico') }}"/>
    <link rel="stylesheet" href="{{ mix('assets/css/admin.css') }}" />
    @stack('css')
</head>

<body>
    <!-- Start Header -->
    @include('layouts/admin_header')
    <!-- End Header -->
    <div class="wrapper" id="app">
        <!-- Start Sidebar Holder -->
        @include('layouts/admin_sidebar')
        <!-- End Sidebar Holder -->

        <!-- Start Content -->
        <div class="container content-area">
            <div class="row">
                @yield('content')
            </div>
        </div>
        <!-- End Content -->
    </div>

    <script src="{{ mix('assets/js/admin/manifest.js') }}"></script>
    <script src="{{ mix('assets/js/admin/vendor.js') }}"></script>
    <script src="{{ mix('assets/js/admin.js') }}"></script>

    @include('sweetalert::alert')
    @stack('js')
</body>

</html> 
<nav id="sidebar" class="nav-sidebar">
    <ul class="list-unstyled components slimescroll-id" id="accordion">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div>
                    <img src="{{ gravatar(auth()->user()->email) }}" alt="user-img" class="img-circle">
                </div>

                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ auth()->user()->name }}</a>
                <ul class="dropdown-menu">
                    <li>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="btn btn-sm"><i class="fa fa-power-off"></i> خروج</a>
                        </form>
                    </li>
                </ul>
            </div>
        </div>

        @hasanyrole('content-manager|clinic-manager')
        <li class="text-right">
            <a href="{{ route('admin.departments.index') }}">
                <i class="fas fa-building ml-2"></i>
                شعبه‌ها
            </a>
        </li>
        @endhasanyrole

        @hasanyrole('content-manager|clinic-manager')
        <li class="text-right">
            <a href="{{ route('admin.users-opinions.index') }}">
                <i class="fas fa-note ml-2"></i>
                نظر کاربران
            </a>
        </li>
        @endhasanyrole

        @hasanyrole('clinic-manager')
        <li class="text-right">
            <a href="{{ route('admin.employees.index') }}">
                <i class="fas fa-user-md ml-2"></i>
                پزشکان
            </a>
        </li>
        @endhasanyrole

        @hasanyrole('content-manager|clinic-manager')
        <li class="text-right services">
            <a href="#services" class="accordion-toggle" data-toggle="collapse" aria-expanded="false">
                <i class="fas fa-book-reader ml-2"></i> خدمات
            </a>

            <ul class="collapse list-unstyled" id="services" data-parent="#accordion">
                <li><a href="{{ route('admin.services-categories.index') }}">دسته بندی خدمات</a></li>
                <li><a href="{{ route('admin.conditions.index') }}">بیماری ها</a></li>
                <li><a href="{{ route('admin.treatments.index') }}">راه های درمان</a></li>
                <li><a href="{{ route('admin.treatments.requests') }}">درخواست مشاوره</a></li>
            </ul>
        </li>
        @endhasanyrole

        @role('clinic-manager')
        <li class="text-right">
            <a href="#users" class="accordion-toggle" data-toggle="collapse" aria-expanded="false">
                <i class="fas fa-users-cog ml-2"></i> کاربران
            </a>

            <ul class="collapse list-unstyled" id="users" data-parent="#accordion">
                <li><a href="{{route('admin.users.index')}}">لیست کاربران</a></li>
                <li><a href="{{ route('admin.users.create') }}">افزودن کاربر</a></li>
            </ul>
        </li>
        @endrole

        @hasanyrole('content-manager|clinic-manager')
        <li class="text-right">
            <a href="{{ route('admin.media.index') }}">
                <i class="fas fa-images ml-2"></i>
                گالری
            </a>
        </li>
        @endhasanyrole

        @hasanyrole('content-manager|clinic-manager')
        <li class="text-right">
            <a href="#posts" class="accordion-toggle" data-toggle="collapse" aria-expanded="false">
                <i class="fas fa-blog ml-2"></i>بلاگ
            </a>

            <ul class="collapse list-unstyled" id="posts" data-parent="#accordion">
                <li>
                    <a href="{{ route('admin.channels.index') }}">دسته بندی پست ها</a>
                </li>

                <li>
                    <a href="{{ route('admin.replies') }}">نظرات کاربران</a>
                </li>

                <li>
                    <a href="{{ route('admin.news.index') }}">لیست اخبار</a>
                </li>
            </ul>
        </li>
        @endhasanyrole

        @role('content-manager')
        <li class="text-right">
            <a href="#pages" class="accordion-toggle" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-file"></i>صفحات
            </a>

            <ul class="collapse list-unstyled" id="pages" data-parent="#accordion">
                <li>
                    <a href="{{ route('admin.settings.home-main-slider') }}">اسلایدر صفحه اصلی</a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.welcome-page') }}">تنظیمات صفحه اصلی</a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.about-page') }}">صفحه درباره ما</a>
                </li>
            </ul>
        </li>
        
        <li class="text-right">
            <a href="#settings" class="accordion-toggle" data-toggle="collapse" aria-expanded="false">
                <i class="fas fa-cogs ml-2"></i>تنظیمات
            </a>

            <ul class="collapse list-unstyled" id="settings" data-parent="#accordion">
                <li>
                    <a href="{{ route('admin.settings.index') }}">تنظیمات سراسری</a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.social') }}">شبکه های اجتماعی</a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.menu') }}">منو ساز</a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.banner') }}">آپلود بنر</a>
                </li>
            </ul>
        </li>
        @endrole
    </ul>
</nav> 
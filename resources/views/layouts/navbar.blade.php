<!-- Header Styles -->
<header class="header-nav">
    <div class="main-header-nav navbar-scrolltofixed">
        <div class="container">
            <nav class="navbar navbar-default bootsnav menu-style1">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="text-white">
                            <a href={{ route('/') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.home')
                            </a>
                        </li>

                        @if($menuTreatment && $menuTreatment != null)
                        <li class="dropdown megamenu-fw">
                            <a href="{{ route('treatments.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.treatments')
                                <i class="fas fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        @foreach ($menuTreatment->values as $treatment)
                                        <div class="col-menu col-md-{{ floor(12 / count($menuTreatment->values)) }}">
                                            <a class="title">{{ $treatment->title }}</a>

                                            <div class="content">
                                                @if($treatment->childs)
                                                <ul class="menu-col">
                                                    @foreach($treatment->childs as $child)
                                                    <li>
                                                        <a href="/{{\LaravelLocalization::getCurrentLocale()}}{{ $child->link }}">{{$child->title}}</a>
                                                    </li>
                                                    @endforeach

                                                    <li>
                                                        <a class="text-primary" href="/{{\LaravelLocalization::getCurrentLocale()}}/treatments?treatment={{ $treatment->id }}">@lang('message.more')</a>
                                                    </li>
                                                </ul>
                                                @endif
                                            </div>
                                        </div><!-- end col-3 -->
                                        @endforeach
                                    </div><!-- end row -->
                                </li>
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href={{ route('treatments.index') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.treatments')
                            </a>
                        </li>
                        @endif

                        @if($menuCondition && $menuCondition->values != null)
                        <li class="dropdown megamenu-fw">
                            <a href="{{ route('conditions.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.conditions')
                                <i class="fas fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        @foreach ($menuCondition->values as $condition)
                                        <div class="col-menu col-md-{{ floor(12 / count($menuCondition->values)) }}">
                                            <a class="title">{{ $condition->title }}</a>
                                            <div class="content">
                                                @if($condition->childs)
                                                <ul class="menu-col">
                                                    @foreach($condition->childs as $child)
                                                    <li><a href="/{{\LaravelLocalization::getCurrentLocale()}}{{ $child->link }}">{{$child->title}}</a></li>
                                                    @endforeach

                                                    <li>
                                                        <a class="text-primary" href="/{{\LaravelLocalization::getCurrentLocale()}}/conditions?condition={{ $condition->id }}">@lang('message.more')</a>
                                                    </li>
                                                </ul>
                                                @endif
                                            </div>
                                        </div><!-- end col-3 -->
                                        @endforeach
                                    </div><!-- end row -->
                                </li>
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href={{ route('conditions.index') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.conditions')
                            </a>
                        </li>
                        @endif
                        
                        @if($menuChannel && $menuChannel->values != null)
                        <li class="dropdown megamenu-fw">
                            <a href="{{ route('threads-list') }}" class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.posts')
                                <i class="fas fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        @foreach ($menuChannel->values as $channel)
                                        <div class="col-menu col-md-{{ floor(12 / count($menuChannel->values)) }}">
                                            <a class="title">{{ $channel->title }}</a>
                                            <div class="content">
                                                @if($channel->childs)
                                                <ul class="menu-col">
                                                    @foreach($channel->childs as $child)
                                                    <li><a href="/{{\LaravelLocalization::getCurrentLocale()}}{{ $child->link }}">{{$child->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </div>
                                        </div><!-- end col-3 -->
                                        @endforeach
                                    </div><!-- end row -->
                                </li>
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href={{ route('threads-list') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.posts')
                            </a>
                        </li>
                        @endif

                        <li>
                            <a href={{ route('about') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.about')
                            </a>
                        </li>

                        <li>
                            <a href={{ route('contactus') }} class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.contact')
                            </a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
</header>
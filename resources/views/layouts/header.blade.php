
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="social-linked">
                    @isset($social)
                    <ul class="list-inline">
                        @if(isset($social->values->facebook) && $social->values->facebook != null)
                            <li><a href="{{ $social->values->facebook }}" target="_blank">
                                <i class="fab fa-facebook-f"></i></a>
                            </li>
                        @endif

                        @if(isset($social->values->aparat) && $social->values->aparat != null)
                            <li><a href="{{ $social->values->aparat }}"><i class="fab fa-pinterest"></i></a></li>
                        @endif

                        @if(isset($social->values->twitter) && $social->values->twitter != null)
                            <li><a href="{{ $social->values->twitter }}"><i class="fab fa-twitter"></i></a></li>
                        @endif

                        @if(isset($social->values->youtube) && $social->values->youtube != null)
                            <li><a href="{{ $social->values->youtube }}"><i class="fab fa-youtube"></i></a></li>
                        @endif

                        @if(isset($social->values->whatsapp) && $social->values->whatsapp != null)
                            @if(is_mobile())
                                <li><a href="https://api.whatsapp.com/send?phone={{ $social->values->whatsapp }}"><i class="fab fa-whatsapp"></i></a></li>
                            @else
                                <li><a href="https://web.whatsapp.com/send?phone={{ $social->values->whatsapp }}"><i class="fab fa-whatsapp"></i></a></li>
                            @endif
                        @endif

                        @if($social->values->telegram != null)
                            <li><a href="{{ $social->values->telegram }}"><i class="fab fa-telegram"></i></a></li>
                        @endif
                    </ul>
                    @endisset
                </div>
            </div>

            <div class="col-md-4"></div>

            <div class="col-md-4">
                <ul class="list-inline {{ \LaravelLocalization::getCurrentLocale() == 'en' ? 'text-right' : 'text-left'}} tac-smd">
                  <li>
                    <a href="/en" class="language-en"> English</a>
                  </li>

                  <li>
                    <a href="/fa" class="language-fa"> فارسی</a>
                  </li>

                  <li>
                    <a href="/ar" class="language-ar">  العربیة</a>
                  </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="container">
        <div class="row d-flex align-items-center">
            @if(!is_mobile() && isset($siteInfo))
            <div class="col-xxs-12 col-xs-12 col-sm-6 col-md-7">
                <div class="ulockd-welcm-hmddl tac-md">
                    <a href="{{ route('/') }}">
                        @if(\LaravelLocalization::getCurrentLocale() == 'en')
                            <img src="{{ asset('assets/img/logo-en.png') }}" class="logo" alt="{{ $siteInfo->values->title }}">
                        @else
                            <img src="{{ asset('assets/img/logo2.png') }}" class="logo" alt="{{ $siteInfo->values->title }}">
                        @endif
                    </a>
                </div>
            </div>
            @endif

            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                <div class="ulockd-ohour-info style2 text-center center-content-row align-items-around">
                    <div class="ulockd-icon text-thm2">
                        <i class="fab fa-instagram"></i>
                    </div>

                    <div class="ulockd-info">
                        @if(isset($social) && isset($social->values->instagram) && $social->values->instagram != null)
                        <a class="ulockd-addrss font-weight-bold" href="{{ $social->values->instagram }}" style="font-size:20px">{{ __('message.instagram') }}</a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                <div class="ulockd-ohour-info style2 text-center center-content-row align-items-around">
                    <div class="ulockd-icon text-thm2">
                        <i class="fas fa-phone"></i>
                    </div>

                    @isset($siteInfo)
                    <div class="ulockd-info">
                        <a class="ulockd-addrss font-weight-bold" href="tel:+98{{ ltrim(en_num($siteInfo->values->phone), 0) }}" style="font-size:20px">{{$siteInfo->values->phone}}</a>
                    </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header-nav">
    <div class="main-header-nav-two navbar-scrolltofixed">
        <div class="container">
            <nav class="navbar navbar-default bootsnav menu-style2">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    
                    <a class="navbar-brand ulockd-main-logo2" href="{{ route('/') }}">
                        <img src="{{ asset('assets/img/logo2.png') }}" class="logo" alt="{{ $siteInfo->values->title }}">
                    </a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right" data-in="jello">
                        <li>
                            <a href={{ route('/') }} class="dropdown-toggle active" data-toggle="dropdown">
                                @lang('message.pages.home')
                            </a>
                        </li>

                        @if($treatments->count())
                        <li class="dropdown">
                            <a href="{{ route('treatments.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.treatments')
                            </a>
                            
                            <ul class="dropdown-menu">
                                @foreach ($treatments as $treatment)
                                    <li @if($treatment->childs->count()) class="dropdown" @endif>
                                        <a href="{{ urldecode(route('treatments.show', ['name' => $treatment->slug])) }}" class="dropdown-toggle" data-toggle="dropdown">
                                            {{ $treatment->name }}
                                        </a>
                                        @if($treatment->childs->count())
                                        <ul class="dropdown-menu">
                                            @foreach ($treatment->childs as $child)
                                                <li>
                                                    <a href="{{ urldecode(route('treatments.show', ['name' => $child->slug])) }}">
                                                        {{ $child->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>    
                                @endforeach
                            </ul>
                        </li>
                        @endif

                        @if($conditions->count())
                        <li class="dropdown">
                            <a href="{{ route('conditions.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                                @lang('message.pages.conditions')
                            </a>
                            
                            <ul class="dropdown-menu">
                                @foreach ($conditions as $condition)
                                    <li @if($condition->childs->count()) class="dropdown" @endif>
                                        <a href="{{ urldecode(route('conditions.show', ['name' => $condition->slug])) }}" class="dropdown-toggle" data-toggle="dropdown">
                                            {{ $condition->name }}
                                        </a>
                                        @if($condition->childs->count())
                                        <ul class="dropdown-menu">
                                            @foreach ($condition->childs as $child)
                                                <li>
                                                    <a href="{{ urldecode(route('conditions.show', ['name' => $child->slug])) }}">
                                                        {{ $child->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>    
                                @endforeach
                            </ul>
                        </li>
                        @endif

                        <li>
                            <a href={{ route('threads-list') }} class="dropdown-toggle active" data-toggle="dropdown">@lang('message.pages.posts')</a>
                        </li>

                        <li>
                            <a href={{ route('about') }} class="dropdown-toggle active" data-toggle="dropdown">@lang('message.pages.about')</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
</div>
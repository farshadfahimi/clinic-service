<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}" lang="{{ str_replace('_', '-', app()->getLocale()) }}" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    {!! SEO::generate() !!}

    @if(config('app.env') == 'production')
    <script>dataLayer = []</script>
    @endif

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/logo/apple-icon.png')}}">
    <link rel="mask-icon" type="image/png" sizes="144x144"  href="{{asset('assets/img/logo/ms-icon-144x144.png')}}">
    <link rel="icon" type="image/png" sizes="144x144"  href="{{asset('assets/img/logo/android-icon.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"  href="{{asset('assets/img/logo/favicon-16x16.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"  href="{{asset('assets/img/logo/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"  href="{{asset('assets/img/logo/favicon-96x96.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/logo/favicon.ico') }}"/>
    <meta name="msapplication-TileColor" content="#79ccca">
    <meta name="theme-color" content="#79ccca">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
    @if(LaravelLocalization::getCurrentLocaleDirection() == "rtl")
    <link rel="stylesheet" href="{{ asset('assets/css/rtl.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}"/>
    @stack('css')

    @if(config('app.env') == 'production')
    <!--BEGIN RAYCHAT CODE-->
    <script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o+"?href="+window.location.href;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="bd496dd4-e647-4bf8-8816-b682a2f4a748";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>
    <!--END RAYCHAT CODE-->
    @endif

</head>
<body class="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-572VKSR"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    
    <div class="wrapper" id="app">
        @include('layouts.header')
    
        @include('layouts.navbar')
    
        @yield('body')
    
        @include('layouts.footer')

        @if(isset($social->values->whatsapp) && $social->values->whatsapp != null)
            @component('components.fixed_button', ['link' => $social->values->whatsapp])    
            @endcomponent
        @endif
    </div>
    

    <script src="{{ mix('assets/js/admin/manifest.js') }}"></script>
    <script src="{{ mix('assets/js/admin/vendor.js') }}"></script>
    @stack('page')
    <script src="{{ mix('assets/js/app.js') }}"></script>
    @stack('js')

    @if(config('app.env') == 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-572VKSR');</script>
    <!-- End Google Tag Manager -->
    @endif
</body>
</html>

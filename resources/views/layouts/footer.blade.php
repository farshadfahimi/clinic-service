<!-- Our Footer -->
<section class="ulockd-footer2">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-4">
                <div class="footer-fst-col">
                    @isset($siteInfo)
                    <div class="footer-logo">
                        @if(\LaravelLocalization::getCurrentLocale() == 'en')
                        <img src="{{ asset('assets/img/logo-en.png') }}" class="logo" alt="{{ $siteInfo->values->title }}">
                        @else
                        <img src="{{ asset('assets/img/logo2.png') }}" class="logo" alt="{{ $siteInfo->values->title }}">
                        @endif
                    </div>
                    @endisset

                    <br/>

                    @isset($siteInfo->values->desc)
                        {!! nl2br($siteInfo->values->desc) !!}
                    @endisset

                    @if($social)
                    <ul class="list-inline style2 ulockd-mrgn1220">
                        @if(isset($social->values->instagram) && $social->values->instagram != null)
                            <li ><a href="{{ $social->values->instagram }}"><i class="fab fa-instagram fa-2x"></i></a></li>
                        @endif

                        @if(isset($social->values->facebook) && $social->values->facebook != null)
                            <li><a href="{{ $social->values->facebook }}" target="_blank">
                                <i class="fab fa-facebook-f fa-2x"></i></a>
                            </li>
                        @endif

                        @if(isset($social->values->aparat) && $social->values->aparat != null)
                            <li><a href="{{ $social->values->aparat }}"><i class="fab fa-pinterest fa-2x"></i></a></li>
                        @endif

                        @if(isset($social->values->twitter) && $social->values->twitter != null)
                            <li><a href="{{ $social->values->twitter }}"><i class="fab fa-twitter fa-2x"></i></a></li>
                        @endif

                        @if(isset($social->values->youtube) && $social->values->youtube != null)
                            <li><a href="{{ $social->values->youtube }}"><i class="fab fa-youtube fa-2x"></i></a></li>
                        @endif

                        @if(isset($social->values->whatsapp) && $social->values->whatsapp != null)
                            @if(is_mobile())
                                <li><a href="https://api.whatsapp.com/send?phone={{ $social->values->whatsapp }}"><i class="fab fa-whatsapp fa-2x"></i></a></li>
                            @else
                                <li><a href="https://web.whatsapp.com/send?phone={{ $social->values->whatsapp }}"><i class="fab fa-whatsapp fa-2x"></i></a></li>
                            @endif
                        @endif

                        @if($social->values->telegram != null)
                            <li><a href="{{ $social->values->telegram }}"><i class="fab fa-telegram fa-2x"></i></a></li>
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
            
            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-2">
                <div class="footer-qlink">
                    <h3 class="text-uppercase"><span class="color-black33">@lang('message.footer.menu')</span></h3>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('/') }}">@lang('message.pages.home')</a></li>
                        <li><a href="{{ route('services.index') }}">@lang('message.welcome.services')</a></li>
                        <li><a href="{{ route('team') }}">@lang('message.pages.team')</a></li>
                        <li><a href="{{ route('about') }}">@lang('message.pages.about')</a></li>
                        <li><a href="{{ route('contactus') }}">@lang('message.pages.contact')</a></li>
                        <li><a href={{ route('threads-list') }}>@lang('message.pages.posts')</a></li>
                        {{-- <li><a href="{{ route('news.index') }}">@lang('message.pages.news')</a></li> --}}
                    </ul>
                </div>
            </div>

            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="footer-contact">
                    <h3 class="text-uppercase"><span class="color-black33">@lang('message.footer.contact_us')</span></h3>
                    @if($siteInfo)
                    <p><span>@lang('message.address') </span> : {{ $siteInfo->values->address }}</p>

                    <div class="ftr-phone">
                        <span> @lang('message.mobile') </span>: 
                        <a href="tel:{{ en_num($siteInfo->values->phone) }}"> {{ $siteInfo->values->phone }}</a>
                    </div>

                    @if(isset($siteInfo->values->companyPhone))
                        <div class="ftr-phone">
                            <span>@lang('message.phone') </span>: 
                            <a href="tel:{{ en_num($siteInfo->values->companyPhone) }}"> {{ $siteInfo->values->companyPhone }}</a>
                        </div>
                    @endif

                    <div class="ftr-fax"><span>@lang('message.fax') </span>: <a href="fax:{{ en_num($siteInfo->values->fax) }}">{{ $siteInfo->values->fax }}</a></div>
                    <div class="ftr-mail"><span>@lang('message.email') </span>: <a href="mailto:{{ $siteInfo->values->email }}"> {{ $siteInfo->values->email }}</a></div>
                    @endif
                </div>
            </div>

            <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <div class="footer-contact">
                    <h3 class="text-uppercase"><span class="color-black33"></span></h3>
                    @if($siteInfo)
                    {{ nl2br($siteInfo->values->description) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Our CopyRight -->
<section class="ulockd-l2-copy-right">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="color-white">Irsa Copyright© 2019 All right reserved</p>
            </div>
        </div>
    </div>
</section>
<header class="main-navbar-header">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top theme-color">
        <div class="menu-toggle-button">
            <a class="nav-link wave-effect" href="#" id="sidebarCollapse">
                <span class="ti-menu"></span>
            </a>
        </div>
        <a class="navbar-brand" href="{{ env('APP_URL') }}" target="_blank">کلینیک</a>

        <ul class="navbar-nav mr-auto navbar-top-links">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                    <b class="d-none d-sm-inline-block">{{ auth()->user()->name }}</b>
                </a>

                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img"><img src="{{ gravatar(auth()->user()->email) }}" alt="user"></div>
                            <div class="u-text">
                                <h4>{{ auth()->user()->name }}</h4>
                                {{-- <p class="text-muted">{{ fa_nums(auth()->user()->phone) }}</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a> --}}
                            </div>
                        </div>
                    </li>
                    <div class="dropdown-divider"></div>
                    {{-- <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <div class="dropdown-divider"></div>
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                    <div class="dropdown-divider"></div> --}}
                    <li>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="btn btn-sm"><i class="fa fa-power-off"></i> خروج</a>
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>
</header> 
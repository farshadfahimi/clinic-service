<div class="col-xs-12 col-sm-4 col-md-3">
    <article class="ulockd-blog-post ulockd-mrgn630">
        <div class="post-thumb">
            <div class="img-post-icon ulockd-bgthm">
                <i class="fa fa-image"></i>
            </div>

            <img class="img-responsive img-whp" src="{{ $news->cover }}" alt="{{ $news->title }}">
        </div>

        <div class="bp-details one">
            <h5 class="post-title" dir="auto">{{ $news->title }}</h5>
            
            <a href="{{ route('news.show', ['news' => $news->id]) }}" class="btn btn-default ulockd-btn-thm2">@lang('message.more')</a>
        </div>
    </article>
</div>
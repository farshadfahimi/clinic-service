<a href="{{ urldecode(route('services.show', ['name' => $service->name])) }}" class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 ulockd-pad395 text-center">
    <div class="ficon-box hvr-shadow" style="height:280px">
        <div class="ficon">
            <img src="{{ $service->icon }}" alt="{{ $service->name }}">
        </div>
        
        <div class="fib-details">
            <h4>{{ $service->name }}</h4>

            <p>{!! str_limit($service->description, 150) !!}</p>

            <p>
                @lang('message.more')
                <i class="fa fa-angle-double-left"></i>
            </p>
        </div>
    </div>
</a>
<div class="jumbotron">
    <h2 class="h2">{{ $item->name }}</h2>

    <p>{!! str_limit(strip_tags($item->body), 150) !!}</p>

    <p>
        <div class="card-footer">
            @if($item->data_type == 'conditions')
            <a href="{{ route('conditions.show', ['condition' => $item->id]) }}" class="btn btn-default ulockd-btn-thm2">@lang('message.more')</a>
            @elseif($item->data_type == 'treatments')
            <a href="{{ route('treatments.show', ['treatment' => $item->id]) }}" class="btn btn-default ulockd-btn-thm2">@lang('message.more')</a>
            @else
            <a href="{{ route('threads-show', ['channel' => $item->channel, 'thread' => $item->id]) }}" class="btn btn-default ulockd-btn-thm2">@lang('message.more')</a>
            @endif
        </div>
    </p>
</div>
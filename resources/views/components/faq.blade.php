<?php
    $average = (int) floor(count($faqs) / 2);
?>

<div class="ulockd-faq-content">
        <div class="ulockd-faq-title clearfix" dir="{{LaravelLocalization::getCurrentLocaleDirection()}}">
            <h2><span class="text-thm2">@lang('message.treatment.faqs')</span></h2>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel-group" id="faq_odd" role="tablist" aria-multiselectable="true">
                    @foreach ($faqs as $faq)
                        @if($loop->index <= $average)
                        <div class="panel panel-default">
                            <div class="panel-heading" {{ 'v-b-toggle.accordion-'.$faq->id }}>
                                <h4 class="panel-title">
                                    <a role="button" class="d-flex align-items-center" data-toggle="collapse" data-parent="#faq_odd" href="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                        <span>
                                            <i class="fa fa-angle-down icon-1"></i>
                                            <i class="fa fa-angle-up icon-2"></i>
                                        </span>

                                        <span>
                                            {{ $faq->title }}
                                        </span>
                                    </a>
                                </h4>
                            </div>
            
                            <b-collapse id="accordion-{{$faq->id}}" accordion="faq-accordin" class="panel-collapse collapse">
                                <div class="panel-body">
                                    {!! nl2br($faq->description) !!}
                                </div>
                            </b-collapse>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel-group" id="faq_even" role="tablist" aria-multiselectable="true">
                @foreach ($faqs as $faq)
                    @if($loop->index > $average)
                    <div class="panel panel-default">
                        <div class="panel-heading" {{ 'v-b-toggle.accordion-'.$faq->id }}>
                            <h4 class="panel-title">
                                <a role="button" class="d-flex align-items-center">
                                    <span class="">
                                        <i class="fa fa-angle-down icon-1"></i>
                                        <i class="fa fa-angle-up icon-2"></i>
                                    </span>

                                    <span>
                                        {{ $faq->title }}
                                    </span>
                                </a>
                            </h4>
                        </div>
        
                        <b-collapse id="accordion-{{$faq->id}}" accordion="faq-accordin" class="panel-collapse collapse">
                            <div class="panel-body">
                                {!! nl2br($faq->description) !!}
                            </div>
                        </b-collapse>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
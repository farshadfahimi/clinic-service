@if(is_mobile())
<a class="btn fixed-button" href="https://api.whatsapp.com/send?phone={{ $link }}" target="_blank">
    <i class="fab fa-whatsapp fa-3x"></i>
</a>
@else
<a class="btn fixed-button" href="https://web.whatsapp.com/send?phone={{ $link }}" target="_blank">
    <i class="fab fa-whatsapp fa-3x"></i>
</a>
@endif
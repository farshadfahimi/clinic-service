<div class="col-xs-12 col-sm-4 col-md-3">
    <article class="ulockd-blog-post ulockd-mrgn630">
        <div class="post-thumb">
            <div class="img-post-icon ulockd-bgthm">
                <i class="fa fa-image"></i>
            </div>

            <img class="img-responsive img-whp" src="{{ $post->cover }}" alt="{{ $post->title }}">

            <ul class="list-inline posted-date">
                @if($post->replyCount)
                    <li>
                        <a class="text-thm2" href="">
                            <span class="flaticon-chat text-thm2"></span>
                            {{ $post->replyCount }} 
                            @lang('message.comment')
                        </a>
                    </li>
                @endif
            </ul>
        </div>

        <div class="bp-details one">
            <h5 class="post-title" dir="auto">{{ $post->title }}</h5>
            
            <a href="{{ $post->path }}" class="btn btn-default ulockd-btn-thm2">@lang('message.more')</a>
        </div>
    </article>
</div>
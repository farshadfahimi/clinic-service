<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 fservice-box ulockd-pad395">
    <div class="db-thumb">
        <img class="img-responsive img-whp" src="{{ $department->cover }}" alt="{{ $department->name }}">
    </div>

    <div class="db-details" dir="auto">
        <h3>{{ $department->name }}</h3>

        <div class="wsixty"></div>

        <p>{{ $department->body }}</p>
        {{-- <a href={{ $department->link }}> More Info...</a> --}}
    </div>
</div>
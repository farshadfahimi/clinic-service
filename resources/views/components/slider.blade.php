<div class="item">
    <div class="mslider-caption text-left flip">
        <div class="mslider-details">
            <div class="slider-text1 color-white">{{ $slide->title }}</div>

            <div class="slider-text2"><p>{{ $slide->description }}</p></div>
            <div class="slider-text3"><p></p></div>

            @foreach ($slide->links as $link)
            <a href="{{ $link->link }}" class="btn btn-lg ulockd-btn-thm2 ulockd-home-btn hidden-xs">
                <span>{{ $link->title }}</span>
            </a>
            @endforeach
        </div>
    </div>

    @if(is_mobile())
    <img class="img-responsive img-whp" src="{{ $slide->mobile_image.'?query='.md5($slide->updated_at) }}" alt="{{ $slide->title }}">
    @else
    <img class="img-responsive img-whp" src="{{ $slide->image.'?query='.md5($slide->updated_at) }}" alt="{{ $slide->title }}">
    @endif
</div>
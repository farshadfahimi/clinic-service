@foreach ($posts as $post)
@if($loop->first)
<div class="col-sm-12 col-md-6">
    <div class="blog-npost ulockd-bgthm">
        <div class="blog-nthumb1">
            <img class="img-responsive" src="{{ $post->cover }}" alt="{{ $post->title }}">
        </div>

        <div class="blog-npdetails">
            <h3>{{ $post->title }}</h3>
        </div>
        
        <div class="blog-overlay text-center">
            <a href="{{ $post->path() }}">{{ edate($post->updated_at) }}</a>
        </div>
    </div>
</div>

@elseif($loop->last)
<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
    <div class="blog-nnpost ulockd-bgthm">
        <a href={{ route('threads-list') }}>@lang('message.blogs')</a>
    </div>
</div>

@else
<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
    <div class="blog-npost">
        <div class="blog-nthumb">
                <img class="img-responsive" src={{ $post->cover }} alt={{ $post->title }}>
        </div>
        <div class="blog-pdetails">
            <h4>{{ $post->title }}</h4>

            <p>
                {!! substr($post->body, 0, 250) !!}
            </p>
        </div>
        <div class="blog-overlay text-center">
            <a href="{{ $post->path() }}">22, Aug...</a>
        </div>
    </div>
</div>
@endif

@endforeach
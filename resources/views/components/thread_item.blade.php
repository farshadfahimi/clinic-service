<div class="col-sm-6 col-md-4">
    <article class="ulockd-blog-post">
        <div class="post-thumb">
            <a href="{{ $thread->path() }}">
                <img class="img-responsive img-whp" src="{{ $thread->poster }}" alt="{{ $thread->title }}">
            </a>
        </div>
        <div class="bp-details one text-left">
            <h3 class="post-title" dir="auto">{{ $thread->title }}</h3>

            <a href="{{ $thread->path() }}" class="btn btn-default ulockd-btn-thm2">Read More</a>
        </div>
    </article>
</div>
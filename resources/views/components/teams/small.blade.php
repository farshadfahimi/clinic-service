<div class="{{ isset($size) ? $size : 'col-xs-12 col-sm-6 col-md-3' }}">
    <div class="ulockd-team-two-member">
        <div class="ulockd-tm-thumb">
            <img class="img-responsive img-whp" src="{{ $team->avatar }}" alt="{{ $team->name }}">
            <div class="ulockd-team-two-mdetails">
            <div class="ulockd-tm-name">{{ $team->name }}</div>
            <h5 class="ulockd-tm-post">- {{ $team->expert }}</h5>
            @if($team->contact)
                <ul class="list-inline ulockd-tm-sicon ulockd-bgthm">
                        @isset($team->contact->facebook)
                        <li>
                            <a href={{ $team->contact->facebook }} target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->twitter)
                        <li>
                            <a href={{ $team->contact->twitter }} target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->telegram)
                        <li>
                            <a href={{ $team->contact->telegram }} target="_blank">
                                <i class="fa fa-telegram"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->instagram)
                        <li>
                            <a href={{ $team->contact->instagram }} target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    @endisset
                </ul>
            @endif
                <ul class="list-unstyled ulockd-tm-fpm ulockd-bgthm">
                    <li>
                        @lang('message.fax'): 
                        @isset($team->contact->fax)
                            {{ $team->contact->fax }}
                        @endisset
                    </li>

                    <li>
                        @lang('message.phone'): 
                        @isset($team->contact->phone)
                            {{ $team->contact->phone }}
                        @endisset
                    </li>

                    <li>
                        @lang('message.mail'): 
                        @isset($team->contact->email)
                            {{ $team->contact->email }}
                        @endisset
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
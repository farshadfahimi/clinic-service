<div class="{{ isset($size) ? $size : 'col-xs-12 col-sm-6 col-md-3' }}">
    <div class="flip-container ulockd-pad395">
        <div class="flipper">
            <div class="front" style="background: url({{ $team->avatar }}) 0 0 no-repeat;"></div>
            
            <div class="back">
                <div class="back-logo"></div>
                <h3 class="name">
                    <a href="{{ route('teams.show', ['name' => $team->name]) }}">{{ $team->name }}</a>
                </h3>
                <h5 class="back-title">{{ $team->expert }}</h5>
                <p>{{ $team->description }}</p>
                @if($team->contact)
                <ul class="list-unstyled">
                    @isset($team->contact->email)
                        <li>
                            <a href="{{ $team->contact->email }}">
                                <i class="fa fa-envelope color-black33"></i>
                                {{ $team->contact->email }}
                            </a>
                        </li>
                    @endisset
                </ul>

                <ul class="list-inline team-icon">
                    @isset($team->contact->facebook)
                        <li>
                            <a href="{{ $team->contact->facebook }}" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->twitter)
                        <li>
                            <a href="{{ $team->contact->twitter }}" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->telegram)
                        <li>
                            <a href="{{ $team->contact->telegram }}" target="_blank">
                                <i class="fab fa-telegram"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->instagram)
                        <li>
                            <a href="{{ $team->contact->instagram }}" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->phone)
                        <li>
                            <a href="tel:{{ $team->contact->phone }}">
                                <i class="fas fa-phone"></i>
                            </a>
                        </li>
                    @endisset

                    @isset($team->contact->mobile)
                        <li>
                            <a href="tel:{{ $team->contact->mobile }}">
                                <i class="fas fa-mobile"></i>
                            </a>
                        </li>
                    @endisset
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>
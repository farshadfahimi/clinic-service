@extends('layouts.app')

@section('body')
<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home" style="background-image: url({{$news->poster}})">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <h1 class="text-uppercase">{{ $news->title }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                    {{ Breadcrumbs::render('news-show', $news) }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Inner Pages Main Section -->
<section class="ulockd-service-details">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 ulockd-mrgn1210">
                    <div class="ulockd-pd-content" dir="auto">
                        {!! $news->body !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@extends('layouts.app')

@section('body')
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home" style="background-image: url('{{ $setting->posts_banner }}')">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <h1 class="text-uppercase">@lang('message.pages.news')</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
                        {{ Breadcrumbs::render('news') }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
	<section class="ulockd-ip-latest-news">
        <div class="container-fluid">
            <div class="row ulockd-mrgn1225">
                @foreach ($news as $item)
                    @include('components.news_item', ['news' => $item])
                @endforeach

                {{ $news->links() }}
            </div>
        </div>
    </section>
@endsection
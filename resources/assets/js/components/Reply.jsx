import React,{ Component } from 'react';
import { jdate } from '../services/helper';

export default class Reply extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="card mb-2">
                <div className="card-body bg-light">
                    <div className="media text-right">
                        <i className="fa fa-user-circle fa-2x ml-3"></i>
                        <div className="media-body">
                            <h5 className="mt-0">{ this.props.reply.name }</h5>
                            <small className="d-block text-mute mb-1">{ this.props.reply.email }</small>
                            {this.props.reply.body}
                        </div>
                    </div>
                </div>

                <div className="card-footer d-flex justify-content-between">
                    <span>
                        زمان ایجاد: {jdate(this.props.reply.creadt_at)}
                    </span>

                    {this.props.reply.approved != null &&
                    <span>
                        {this.props.reply.is_published ? 'تایید شده: ' : 'رد شده:'}
                        <i className="badge badge-success p-1">{this.props.reply.approved.name}</i>
                    </span>
                    }

                    <div>
                        <button className={"btn btn-sm "+ (this.props.reply.is_published ? 'btn-success' : 'btn-danger')} onClick={ e => this.props.onPublish(this.props.reply.id) }>
                            {this.props.reply.is_published ? 
                                <i className="fa fa-eye"></i>
                                :
                                <i className="fa fa-eye-slash"></i>
                            }
                        </button>

                        <button className="btn btn-sm btn-danger mr-2" onClick={ e => this.props.onDelete(this.props.reply.id) }>
                            <i className="fa fa-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
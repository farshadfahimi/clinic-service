require('./bootstrap');
require('./../../lib/js/bootstrap');
require('./../../lib/js/bootsnav');

window.Vue = require('vue');

import { Swiper, SwiperSlide } from 'vue-awesome-swiper';
import 'swiper/css/swiper.css';
import { 
    BCollapse, 
    BDropdown, 
    BDropdownDivider, 
    BDropdownItem, 
    BAvatar, 
    VBToggle,
    BNavbar,
    BNavItem,
    BNavItemDropdown
} from 'bootstrap-vue';

import LightBox from 'vue-it-bigger'

import GallerySwiper from './components/GallerySwiper'

Vue.component('gallery-swiper', GallerySwiper)

Vue.component('swiper', Swiper);
Vue.component('swiper-slide', SwiperSlide);
Vue.component('b-collapse', BCollapse);
Vue.component('b-dropdown', BDropdown);
Vue.component('b-dropdown-item', BDropdownItem);
Vue.component('b-avatar', BAvatar);
Vue.component('b-dropdown-divider', BDropdownDivider);
Vue.component('b-navbar', BNavbar);
Vue.component('b-nav-item', BNavItem);
Vue.component('b-nav-item-dropdown', BNavItemDropdown);
Vue.component('light-box', LightBox)

Vue.directive('b-toggle', VBToggle);



new Vue({
    el: '#app',
});
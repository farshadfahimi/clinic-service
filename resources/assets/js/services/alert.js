import swal from 'sweetalert2';
import { faNumber } from './helper';

export function ConfirmDialog(message, callback, icon = 'warning') {
    swal.fire({
        title: message,
        icon: icon,
        showCancelButton: true,
        confirmButtonText: 'بلی',
        cancelButtonText: 'خیر'
    }).then(result => {
        if(result.value) {
            callback()
        }
    })
}

export function successAlert(status, message = null){
    swal.fire({
        toast: true,
        position: 'bottom',
        type: 'success',
        title: faNumber(status),
        text: message ? message : 'عملیات با موفقیت انجام گردید',
        showConfirmButton: false,
        timer: 2000 
    });
}

export function notFound(status){
    swal.fire({
        toast: true,
        position: 'bottom',
        type: 'error',
        title: faNumber(status),
        text: 'خطا در دریافت اطلاعات',
        showConfirmButton: false,
        timer: 2000 
    });
}

export function storeError(status){
    swal.fire({
        toast: true,
        position: 'bottom',
        type: 'error',
        title: faNumber(status),
        text: 'خطا در ذخیره اطلاعات',
        showConfirmButton: false,
        timer: 2000 
    });
}

export function destroyError(status){
    swal.fire({
        toast: true,
        position: 'bottom',
        type: 'error',
        title: faNumber(status),
        text: 'امکان حذف وجود ندارد',
        showConfirmButton: false,
        timer: 2000 
    });
}


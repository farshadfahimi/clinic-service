export function api(url){
    return document.location.origin + '/api' + url
}

export function faNumber(input) {
    var chars = {
        "0": "۰",
        "1": "۱",
        "2": "۲",
        "3": "۳",
        "4": "۴",
        "5": "۵",
        "6": "۶",
        "7": "۷",
        "8": "۸",
        "9": "۹",
        "/": ","
    }

    return input.toString().replace(/[0-9]/g, c => chars[c]);
}

export function enNumber(input) {
    var chars = {
        "۰": "0",
        "۱": "1",
        "۲": "2",
        "۳": "3",
        "۴": "4",
        "۵": "5",
        "۶": "6",
        "۷": "7",
        "۸": "8",
        "۹": "9",
        ",": "/"
    }

    return input.toString().replace(/[۰۱۲۳۴۵۶۷۸۹,]/g, c => chars[c]);
}

export function jdate($date, $format = 'jYYYY/jM/jD'){
    var moment =   require('moment-jalaali');
    moment.loadPersian({usePersianDigits: true});

    $date = moment($date).format($format);

    if($date == moment()){
        return 'امروز';
    }else{
        return $date;
    }
}

export function tooman(input){
    return new Intl.NumberFormat("fa-IR").format(input);
}

export function percent(input){
    input /= 100;
    return new Intl.NumberFormat("fa-IR",{
        style: 'percent'
    }).format(input);
}

export function lang(lang){
    switch(lang){
        case 'en':
        case 'EN':
            return 'انگلیسی';
            break;
        case 'ar':
        case 'AR':
            return 'عربی';
            break;
        case 'fa':
        case 'FA':
            return 'فارسی';
            break;
        default:
            return 'همه';
            break;
    }
}
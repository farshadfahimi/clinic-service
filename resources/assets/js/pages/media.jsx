import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { filter, findIndex, map } from 'lodash';
import { api, faNumber } from '../services/helper';
import { ConfirmDialog, destroyError, storeError, successAlert } from '../services/alert';

export default class Media extends Component {
    constructor(props) {
        super(props)

        this.state = ({
            type: 'IMAGE',
            path: '',
            file: null,
            title: '',
            tags: [],
            busy: false,
            list: [],
            category_id: null,
            categories: [],
            mediaTags: [],
            category: {
                lang: 'FA',
                name: ''
            }
        })

        this.store = this.store.bind(this)
        this.storeCategory = this.storeCategory.bind(this)
        this.handleTags = this.handleTags.bind(this);
        this.fetchMedia = this.fetchMedia.bind(this);
        this.delete = this.delete.bind(this);
    }

    get images() {
        return filter(this.state.list, { type: 'IMAGE' });
    }

    get videos() {
        return filter(this.state.list, { type: 'VIDEO' });
    }

    handleImage(e){
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = () => {
            this.setState({
                file: reader.result
            })
        }
    }

    handleTags(newVal) {
        this.setState({
            tags: newVal
        })
    }

    async store() {
        this.setState({
            busy: true
        });

        await Axios.post(api('/media'), this.state).then(response => {
            successAlert(response.status);
            const { list } = this.state;

            list.push(response.data.data);

            this.setState({
                type: 'IMAGE',
                path: '',
                file: null,
                title: '',
                tags: [],
                list
            });
        }).catch(error => {
            storeError(error.response.status)
        }).finally(() => {
            this.setState({
                busy: false
            })
        })
    }

    async delete(id) {
        Axios.delete(api('/media/' + id))
            .then(response => {
                const i = findIndex(this.state.list, { id: id });
                const { list } = this.state;
                list.splice(i, 1);

                this.setState({
                    list
                })
            }).catch(e => destroyError(e.response.status));
    }

    async fetchMedia() {
        await Axios.get(api('/media'))
            .then(response => {
                this.setState({
                    list: response.data.data
                })
            }).catch(() => {  })
    }

    async fetchTags() {
        // await Axios.get(api('/media/tags'))
        //     .then(response => {
        //         this.setState({
        //             tagsList: response.data.data
        //         })
        //     }).catch(() => {});
    }

    async storeCategory() {
        this.setState({
            busy: true
        })

        await Axios.post(api('/media-category'), this.state.category)
            .then(response => {
                const categories = this.state.categories
                categories.push(response.data.data)

                this.setState({
                    categories
                })

                this.setState({
                    category: {
                        lang: 'FA',
                        name: ''
                    }
                })

                successAlert(200)
            })
            .catch(error => {
                console.log(error)
            }).finally(() => {
                this.setState({ busy: false })
            })
    }

    async componentDidMount() {
        await this.fetchMedia();

        await this.fetchTags();
    }

    render(){
        return (
            <div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header text-right">
                                افزودن فایل جدید
                            </div>

                            <div className="card-body">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="form-group text-right">
                                            <label htmlFor="">انتخاب دسته بندی</label>
                                            <select className="custom-select" onChange={ e => this.setState({ category_id: e.target.value }) } value={ this.state.category_id }>
                                                <option value="">انتخاب کنید</option>
                                                { this.state.categories.map(category => (
                                                    <option value={ category.id } key={`category-id-`+category.id}>{ category.name + ' - ' + category.lang }</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="form-group text-right">
                                            <label htmlFor="">نوع فایل</label>
                                            <select className="custom-select" onChange={ e => this.setState({ type: e.target.value }) } value={ this.state.type }>
                                                <option value="IMAGE">تصویر</option>
                                                <option value="VIDEO">ویدیو</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        { this.state.type == 'IMAGE' ?
                                            <div className="custom-file mt-4">
                                                <input type="file" name="cover" className="custom-file-input" id="cover"
                                                    onChange={ e => this.handleImage(e)}/>
                                                <label className="custom-file-label text-left cover" htmlFor="cover">انتخاب کاور</label>
                                            </div>
                                        :
                                            <div className="form-group text-right">
                                                <label htmlFor="">مسیر ویدیو</label>
                                                <input 
                                                    type="text" 
                                                    className="form-control"
                                                    onChange={ e => this.setState({ path: e.target.value }) } />
                                            </div>
                                        }
                                    </div>

                                    <div className="col-md-12">
                                        <div className="form-group text-right">
                                            <label htmlFor="">عنوان</label>
                                            <input type="text" className="form-control" value={ this.state.title } onChange={ e => this.setState({ title: e.target.value }) } />
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <button className="btn btn-sm btn-primary" disabled={ this.busy } onClick={ this.store }>ذخیره</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header text-right">
                                افزودن دسته بندی
                            </div>

                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group text-right">
                                            <label htmlFor="">انتخاب زبان</label>
                                            <select className="custom-select" onChange={ e => this.setState({ category: { ...this.state.category, lang: e.currentTarget.value } }) } value={ this.state.category.lang }>
                                                <option value="FA">فارسی</option>
                                                <option value="EN">انگلیسی</option>
                                                <option value="AR">عربی</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="form-group text-right">
                                            <label htmlFor="">عنوان گروه</label>
                                            <input type="text" className="form-control" onChange={ e => this.setState({ category: { ...this.state.category, name: e.currentTarget.value } }) } value={ this.state.category.name } />
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <button className="btn btn-primary" disabled={ this.busy } onClick={ this.storeCategory }>ذخیره</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card mt-3 text-right">
                    <div className="card-header">
                        لیست اطلاعات
                    </div>

                    <div className="card-body">
                        <nav>
                            <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                <a className="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-images" role="tab" aria-controls="nav-home" aria-selected="true">تصاویر</a>
                                <a className="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-video" role="tab" aria-controls="nav-profile" aria-selected="false">ویدیوها</a>
                            </div>
                        </nav>

                        <div className="tab-content mt-3" id="nav-tabContent">
                            <div className="tab-pane fade show active" id="nav-images" role="tabpanel" aria-labelledby="nav-home-tab">
                                {this.images.length ?
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>تصویر</th>
                                            <th>عنوان</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {map(this.images, (image, index) => (
                                        <tr key={`image-${index}`}>
                                            <td>{ faNumber(++index) }</td>
                                            <td>
                                                <img src={ image.path } alt="#"/>
                                            </td>

                                            <td>{ image.title }</td>

                                            <td>
                                                <button className="btn btn-sm text-danger" onClick={ () => ConfirmDialog('آیا از حذف این تصویر اطمینان دارید؟', () => this.delete(image.id)) }>
                                                    <i className="fa fa-trash-alt"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        ))}
                                    </tbody>
                                </table>
                                :
                                <div className="alert alert-info text-center">تصویری بارگزاری نشده است</div>
                                }
                            </div>
                            <div className="tab-pane fade" id="nav-video" role="tabpanel" aria-labelledby="nav-profile-tab">
                                {this.videos.length ?
                                    <table className="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>عنوان</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            { map(this.videos, (video, index) => (
                                            <tr>
                                                <td>{ faNumber(++index) }</td>
                                                <td>{ video.title }</td>
                                                <td>
                                                    <button className="btn btn-sm text-danger" onClick={ () => ConfirmDialog('آیا از حذف ویدیو اطمینان دارید', () => this.delete(id)) }>
                                                        <i className="fa fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            )) }
                                        </tbody>
                                    </table>
                                    :
                                    <div className="alert alert-info text-center">
                                        ویدیویی بارگزاری نشده است
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<Media/>, document.getElementById('media_container'));
import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { storeError, notFound, ConfirmDialog } from '../services/alert';
import { api, faNumber, lang } from '../services/helper';
import findIndex from 'lodash/findIndex';

export default class ServicesCategories extends Component
{
    constructor(props){
        super(props);

        this.state = {
            buzy: false,
            servicesCategories: [],
            servicesCategory:{
                lang: 'fa'
            },
            selectedDepartment: null
        }

        this.store = this.store.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    store(){
        Axios.post(api('/services-categories'), this.state.servicesCategory)
            .then( response => {
                let servicesCategories = this.state.servicesCategories;
                servicesCategories.unshift(response.data.data);

                this.setState({
                    servicesCategories: servicesCategories,
                    servicesCategory:{
                        lang: 'fa'
                    }
                })
            })
            .catch( error => {
                storeError(error.response.status);
            });
    }

    destroy(id){
        let servicesCategories = this.state.servicesCategories;
        let i = findIndex(servicesCategories,{id: id});
        servicesCategories.splice(i,1);

        Axios.delete(api('/services-categories/'+id))
            .then(response => {
                this.setState({
                    servicesCategories
                });
            })
            .catch( error => {
                destroyError(error.response.status);
            })
    }

    readFile(e){
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = () => {
            this.setState({
                servicesCategory:{
                    ...this.state.servicesCategory,
                    icon: reader.result
                }
            });
        }
    }

    componentDidMount(){
        Axios.get(api('/services-categories'))
            .then( response => {
                this.setState({
                    servicesCategories: response.data.data
                });
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }


    render(){
        return(
            <div className="container">
                <div className="card">
                    <div className="card-header text-right">
                        ایجاد دسته بندی جدید
                    </div>

                    <div className="card-body text-right">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" value={this.state.servicesCategory.name ? this.state.servicesCategory.name : ''} 
                                        onChange={ e => this.setState({
                                            servicesCategory:{
                                                ...this.state.servicesCategory,
                                                name: e.target.value
                                            }
                                        })}/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <label>آیکون</label>
                                <div className="input-group mb-3">
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="icon" onChange={ e => this.readFile(e) }/>
                                        <label className="custom-file-label text-left" htmlFor="icon">انتخاب فایل</label>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="lang">زبان</label>
                                    <select className="custom-select" id="lang" defaultValue={this.state.servicesCategory.lang ? this.state.servicesCategory.lang : 'fa'}
                                        onChange={ e => this.setState({
                                            servicesCategory:{
                                                ...this.state.servicesCategory,
                                                lang: e.target.value
                                            }
                                        }) }>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-">
                                    <label htmlFor="desc">توضیحات</label>
                                    <input type="text" className="form-control" value={this.state.servicesCategory.description ? this.state.servicesCategory.description : ''}
                                        onChange={ e => this.setState({
                                            servicesCategory:{
                                                ...this.state.servicesCategory,
                                                description: e.target.value
                                            }
                                        })}/>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <button className="btn btn-sm btn-primary" onClick={ this.store }>
                                    {this.state.buzy &&
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}

                                    {this.state.buzy ? 'منتظر بمانید' : 'ذخیره'}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.servicesCategories.length ? 
                <table className="table mt-3 bg-light">
                    <thead className="thead-dark text-right">
                        <tr>
                            <th>#</th>
                            <th>نام</th>
                            <th>زبان</th>
                            <th>توضیحات</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.servicesCategories.map( (service, i) => (
                            <tr key={service.id}>
                                <td className="text-right">{faNumber(++i)}</td>

                                <td className={service.lang != 'EN' ? 'text-right' : 'text-left'}>{service.name}</td>

                                <td className="text-right">
                                    {lang(service.lang)}
                                </td>

                                <td className={service.lang != 'EN' ? 'text-right' : 'text-left'}>
                                    {service.description}
                                </td>

                                <td>
                                    <a href={'services-categories/'+service.id+"/edit"} className="btn btn-sm btn-warning">
                                        <i className="fa fa-edit"></i>
                                    </a>

                                    <button className="btn btn-sm btn-danger mr-2" onClick={ e => ConfirmDialog('آیا از حذف دسته بندی اطمینان دارید؟', () => this.destroy(service.id)) }>
                                        <i className="fa fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                :
                <div className="alert alert-warning mt-3 text-center">خدمتی ایجاد نشده است</div>
                }

            </div>
        )
    }
}

ReactDOM.render(<ServicesCategories/>, document.getElementById('services_categories_container'));
import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import { api } from '../services/helper';
import { notFound, storeError, successAlert } from '../services/alert';
import Axios from 'axios';

class TreatmentsPricing extends Component
{
    constructor(props){
        super(props);

        this.state = {
            treatmentPricing:{
                description: '',
                prices: []
            },
            defaultPricing:{
                title: '',
                price: '',
                offer: ''
            }
        }

        this.store = this.store.bind(this);
        this.appendInfoItem = this.appendInfoItem.bind(this);
        this.updatevalue = this.updatevalue.bind(this);
        this.destroyValue = this.destroyValue.bind(this);
    }

    appendInfoItem(){
        let prices = this.state.treatmentPricing.prices;

        if(prices == null)
            prices = [this.state.defaultPricing];
        else
            prices.push(this.state.defaultPricing);

        this.setState({
            treatmentPricing: {
                ...this.state.treatmentPricing,
                prices: prices
            }
        })
    }

    updatevalue(target, i, value){
        let prices = this.state.treatmentPricing.prices;

        prices[i]={
            ...prices[i],
            [target]: value
        }
        
        this.setState({
            treatmentPricing: {
                ...this.state.treatmentPricing,
                prices: prices
            }
        })
    }

    destroyValue(i){
        let prices = this.state.treatmentPricing.prices;

        prices.splice(i, 1);

        this.setState({
            treatmentPricing: {
                ...this.state.treatmentPricing,
                prices: prices
            }
        })
    }

    store(){
        Axios.put(api('/treatments/'+this.props.treatmentId+'/pricing'), this.state.treatmentPricing)
            .then( response => {
                successAlert(response.status);
            })
            .catch( error => storeError(error.response.status));
    }

    componentDidMount(){
        Axios.get(api('/treatments/'+this.props.treatmentId+'/pricing'))
            .then( response => {
                this.setState({
                    treatmentPricing: response.data.data
                });
            })
            .catch( error => {  });
    }

    render(){
        return (
            <div className="row">
                <div className="col-md-6">
                    <div className="form-group">
                        <label htmlFor="description">توضیحات</label>
                        <textarea name="description" className="form-control" rows="15" value={this.state.treatmentPricing.description}
                             onChange={ e => this.setState({treatmentPricing:{
                                 ...this.state.treatmentPricing,
                                 description: e.target.value
                             }}) }>
                        </textarea>
                    </div>
                </div>

                <div className="col-md-6">
                    <label >هزینه مشاوره</label>
                    {this.state.treatmentPricing.prices.map((item, i) => (
                            <div className="row">
                                <div className="col-md-12">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" defaultValue={item.title} 
                                        onChange={ e => this.updatevalue('title', i, e.target.value) }/>
                                </div>
                                    
                                <div className="col-md-5">
                                    <label htmlFor="price">قیمت</label>
                                    <input type="text" className="form-control" defaultValue={item.price}
                                        onChange={ e => this.updatevalue('price', i, e.target.value) }/>
                                </div>

                                <div className="col-md-5">
                                    <label htmlFor="price">تخفیف</label>
                                    <input type="text" className="form-control" defaultValue={item.offer}
                                        onChange={ e => this.updatevalue('offer', i, e.target.value) }/>
                                </div>

                                <div className="col-md-2 mt-4">
                                    <button className="btn btn-sm btn-danger mt-2" onClick={ e => this.destroyValue(i)}>
                                        <i className="fa fa-trash-alt"></i>
                                    </button>
                                </div>
                            </div>
                    ))}
                    

                    <div className="col-12">
                        <button className="btn btn-sm btn-primary mt-2" onClick={this.appendInfoItem}>افزودن</button>
                    </div>
                </div>

                <button className="btn btn-sm btn-primary" onClick={this.store}>ذخیره</button>
            </div>
        );
    }
}

ReactDOM.render(<TreatmentsPricing treatmentId={document.getElementById("treatments_pricing").dataset.id}/>, document.getElementById('treatments_pricing'))
import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { api, jdate } from '../services/helper';
import { notFound, destroyError, ConfirmDialog } from '../services/alert';
import findIndex from 'lodash/findIndex';

export default class Threads extends Component{
    constructor(props){
        super(props);

        this.state = {
            threads: [],
        }
    }

    componentDidMount(){
        Axios.get(api('/channels/'+this.props.id+'/threads'))
            .then( response => {
                this.setState({
                    threads: response.data.data
                })
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }

    destroy(thread){
        Axios.delete(api('/channels/'+this.props.id+'/threads/'+thread.id))
        .then( response => {
            let threads = this.state.threads;
            let i = findIndex(threads, {id: thread.id});
            threads.splice(i, 1);

            this.setState({
                threads: threads
            });
        })
        .catch( error => {
            destroyError(error.response.status);
        });
    }

    render(){
        return(
            <div className="container">
                {this.state.threads.length ? 
                <table className="table table-striped text-right mt-3">
                    <thead className="thead-dark">
                        <tr className="text-right">
                            <th>#</th>
                            <th>عنوان</th>
                            <th>ایجاد کننده</th>
                            <th>آخرین ویرایش</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.threads.map( thread => (
                        <tr key={thread.id}>
                            <td>
                                <img src={thread.cover} className="img-thumbnail"/>
                            </td>
                            <td>{thread.title}</td>
                            <td>{thread.creator.name}</td>
                            <td>{jdate(thread.updated_at)}</td>
                            <td className="d-flex">
                                <a href={`threads/${thread.id}/seo`} className="btn btn-info btn-sm">
                                    seo
                                </a>

                                <a href={`threads/${thread.id}/replies`} className="btn btn-success btn-sm mr-2">
                                    <i className="far fa-comment-dots"></i>
                                </a>

                                <a href={`threads/${thread.id}/edit`} className="btn btn-warning btn-sm mr-2">
                                    <i className="fa fa-edit"></i>
                                </a>

                                <button className="btn btn-danger btn-sm mr-2" onClick={ e => ConfirmDialog('آیا از حذف اطمینان دارید', () => this.destroy(thread)) }>
                                    <i className="fa fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
                :
                <div className="alert alert-warning text-center mt-4">پستی یافت نشد</div>
                }
            </div>
        );
    }
}

ReactDOM.render(<Threads
    id={document.getElementById('threads_container').dataset.id}/>, document.getElementById('threads_container'));
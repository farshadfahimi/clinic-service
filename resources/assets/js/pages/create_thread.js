
let token = document.head.querySelector('meta[name="csrf-token"]');

tinymce.init({
    selector: '.body',
    plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media imagetools template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    toolbar_sticky: true,
    autosave_ask_before_unload: true,
    autosave_interval: "30s",
    autosave_restore_when_empty: false,
    autosave_retention: "2m",
    media_dimensions: false,
    height: 500,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_noneditable_class: "mceNonEditable",
    toolbar_drawer: 'sliding',
    contextmenu: "link image imagetools table",
    images_upload_credentials: false,
    automatic_uploads: true,
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
    
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/api/content/images');
        xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
        xhr.onload = function() {
          var json;
            
          if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }

          json = JSON.parse(xhr.responseText);
    
          if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
          }
    
          success(json.location);
        };
    
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
    
        xhr.send(formData);
      }
  });

function prograssBar(el){
    $('.'+el.id).css({
        "background-color": "#38c172",
        "color":    "#fff"
    })
}

window.prograssBar = prograssBar;
import React,{ Component,Fragment } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { notFound,storeError, destroyError, successAlert, ConfirmDialog } from '../services/alert';
import { api, faNumber, lang } from '../services/helper';
import findIndex from 'lodash/findIndex';

export default class Channels extends Component{
    constructor(props){
        super(props);

        this.state={
            channels:[],
            channel:{
                name: '',
                lang: 'fa'
            },
            selectedChannel: null,
            buzy: false
        }

        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
    }

    store(){
        this.setState({
            buzy: true
        });

        Axios.post(api('/channels'), this.state.channel)
            .then( response => {
                let channels = this.state.channels;
                channels.push(response.data.data);

                this.setState({
                    channels: channels,
                    buzy: false
                });
            })
            .catch( error => {
                storeError(error.response.status);

                this.setState({
                    buzy: false
                })
            });
    }
    
    update(){
        Axios.put(api('/channels/'+ this.state.selectedChannel.id ),this.state.selectedChannel)
            .then( response => {
                let channels = this.state.channels;
                let i = findIndex(channels,{id: this.state.selectedChannel.id});
                channels[i] = this.state.selectedChannel;

                this.setState({
                    selectedChannel: null,
                    channels: channels
                })

            })
            .catch( error => {
                console.log(error)
                storeError(error.response.status);
            })
    }

    destroy(id){
        Axios.delete(api('/channels/'+id))
        .then(response => {
            let channels = this.state.channels;
            let i = findIndex(channels, {id: id});

            channels.splice(i, 1);
            successAlert(response.status);
            this.setState({
                channels: channels
            });
        })
        .catch(error => {
            destroyError(error.response.status);
        })
    }

    componentDidMount(){
        Axios.get(api('/channels'))
        .then( response => {
            this.setState({
                channels: response.data.data,
            });
        })
        .catch( error => {
            notFound(error.response.status);
        });
    }

    render(){
        return(
            <Fragment>
                <div className="card text-right">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" value={this.state.channel.name} className="form-control"
                                        onChange={ e => this.setState({
                                            channel:{
                                                ...this.state.channel,
                                                name: e.target.value
                                                }
                                            }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="lang">زبان</label>
                                    <select className="custom-select" value={this.state.channel.lang} id="lang"
                                        onChange={ e => this.setState({
                                            channel:{
                                                ...this.state.channel,
                                                lang: e.target.value
                                            }
                                        }) }>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <button className="btn btn-primary" onClick={ this.store } disabled={this.state.buzy}>
                                    {!this.state.buzy ?
                                    <span>ذخیره</span>
                                    :
                                    <Fragment>
                                         <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        در حال ذخیره...
                                    </Fragment>
                                    }
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="card text-right">
                        {this.state.channels.length ? 
                        <div className="card-body">
                            <table className="table table-striped">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>عنوان</th>
                                        <th>زبان</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                {
                                    this.state.channels.map( (channel, i) => (
                                        <tr key={i}>
                                            <td>{ faNumber(++i) }</td>
                                            <td>
                                            {this.state.selectedChannel != null && this.state.selectedChannel.id == channel.id ? 
                                                <input type="text" className="form-control" defaultValue={this.state.selectedChannel.name}
                                                    onChange={ e => this.setState({
                                                        selectedChannel:{
                                                            ...this.state.selectedChannel,
                                                            name: e.target.value
                                                        }
                                                    })}/>
                                                :
                                                channel.name 
                                            }
                                            </td>
                                            <td>
                                                {this.state.selectedChannel != null && this.state.selectedChannel.id == channel.id ? 
                                                    <select className="custom-select" value={this.state.selectedChannel.lang} id="lang"
                                                        onChange={ e => this.setState({
                                                            selectedChannel:{
                                                                ...this.state.selectedChannel,
                                                                lang: e.target.value
                                                            }
                                                        }) }>
                                                        <option value="fa">فارسی</option>
                                                        <option value="ar">عربی</option>
                                                        <option value="en">انگلیسی</option>
                                                    </select>
                                                    :
                                                    lang(channel.lang)
                                                }
                                            </td>
                                            <td>
                                                {this.state.selectedChannel != null && this.state.selectedChannel.id == channel.id ?
                                                    <Fragment>
                                                        <button className="btn btn-sm btn-danger" onClick={ e => this.setState({ selectedChannel: null })}>
                                                            <i className="fa fa-times"></i>
                                                        </button>

                                                        <button className="btn btn-sm btn-success mr-2" onClick={ this.update }>
                                                            <i className="fa fa-check"></i>
                                                        </button>
                                                    </Fragment>
                                                    :
                                                    <Fragment>
                                                        <a href={'channels/' + channel.id + '/threads'} className="btn btn-sm btn-info">
                                                            <i className="fas fa-ellipsis-h"></i>
                                                        </a>

                                                        <button className="btn btn-sm btn-warning mr-2" onClick={ e => this.setState({selectedChannel: channel})}>
                                                            <i className="fa fa-edit"></i>
                                                        </button>

                                                        <button className="btn btn-sm btn-danger mr-2" onClick={ e => ConfirmDialog('آیا از حذف اطمینان دارید?', () => this.destroy(channel.id) ) }>
                                                            <i className="fa fa-trash-alt"></i>
                                                        </button>
                                                    </Fragment>
                                                }
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                        :
                            <div className="alert alert-warning text-center">دسته بندی ایجاد نشده است</div>
                        }
                    </div>
                </div>
            </Fragment>
        )
    }
}

ReactDOM.render(<Channels/>, document.getElementById('channels_container'));
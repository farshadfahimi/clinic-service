import Axios from 'axios';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { successAlert, storeError } from '../services/alert';
import { api, faNumber, lang } from '../services/helper';

export default class Tags extends Component {
    constructor(props) {
        super(props)

        this.state = ({
            busy: false,
            tag: '',
            lang: 'fa'
        })

        this.store = this.store.bind(this);
    }

    async store() {    
        const { tag, lang } = this.state;

        this.setState({
            busy: true
        });

        await Axios.post(api('/tags'), {
            tag,
            lang
        }).then(response => {
            successAlert(200)
        }).catch(error => {
            storeError(error.response.status)
        }).finally(() => { this.setState({ busy: false, tag: '' }) })
    }

    render() {
        return (
            <div>
                <div className="card text-right">
                    <div className="card-header">
                        افزودن برچسب جدید
                    </div>

                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="">عنوان برچسب</label>
                                    <input 
                                        type="text" 
                                        className="form-control"
                                        value={ this.state.tag }
                                        onChange={ e => this.setState({ tag: e.target.value })} />
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="">زبان</label>
                                    <select className="form-control" onChange={ e => this.setState({ lang: e.target.value }) }>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="col-12">
                            <button className="btn btn-sm btn-primary" onClick={ this.store } disabled={ this.busy }>ذخیره</button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

ReactDOM.render(<Tags/>, document.getElementById('tags_container'));
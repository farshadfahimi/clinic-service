import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import Reply from '../components/Reply';
import { notFound, destroyError, storeError } from '../services/alert';
import { api } from './../services/helper';
import findIndex from 'lodash/findIndex';

export default class Replies extends Component{
    constructor(props)
    {
        super(props);

        this.state = {
            replies: []
        };

        this.destroy = this.destroy.bind(this);
        this.publish = this.publish.bind(this);
    }

    componentDidMount()
    {
        Axios.get(api('/channels/'+this.props.channel+'/threads/'+this.props.thread+'/replies'))
            .then( response => {
                this.setState({
                    replies: response.data.data
                });
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }

    destroy(id){
        let replies = this.state.replies;
        let i = findIndex(replies,{id: id});
        replies.splice(i, 1);

        Axios.delete(api('/replies/'+id))
        .then( response => {
            this.setState({
                replies: replies
            });
        })
        .catch( error => {
            destroyError(error.response.status);
        });
    }

    publish(id){
        Axios.patch(api('/replies/'+id+'/publish'))
        .then(response => {
            let replies = this.state.replies;
            let i = findIndex(replies, {id: id});

            replies[i]=response.data.data,
            this.setState({
                replies: replies
            });
        })
        .catch( error => {
            storeError(error.response.status)
        });
    }

    render(){
        return (
            <div className="container">
            {this.state.replies.length ? 
                this.state.replies.map( reply => (
                    <Reply key={reply.id} reply={reply}
                    onDelete={ this.destroy }
                    onPublish={ this.publish }/>
                ))
            :
                <div className="alert alert-warning text-center">پاسخی داده نشده است</div>
            }
            </div>
        );
    }
}

ReactDOM.render(<Replies
    channel={document.getElementById('replies_container').dataset.channel}
    thread={document.getElementById('replies_container').dataset.thread}
    />, 
    document.getElementById('replies_container'));
import React,{ Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { api, faNumber } from '../services/helper';
import { notFound, storeError, successAlert, destroyError } from '../services/alert';
import Axios from 'axios';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import findIndex from 'lodash/findIndex';
import find from 'lodash/find';

export default class Faq extends Component
{
    constructor(props){
        super(props);

        this.state=({
            faqs: [],
            selectedFaq: null,
            newFaq: {
                title: '',
                description: ''
            }
        })

        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
        this.destroy = this.destroy.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    path(){
        return api('/'+ this.props.type + '/'+this.props.id + '/faqs');
    }

    store(){
        Axios.post(this.path(), {
            ...this.state.newFaq,
            sort: this.state.faqs.length
        })
            .then(response => {
                let faqs = this.state.faqs;
                faqs.push(response.data.data);

                this.setState({
                    newFaq: {
                        title: '',
                        description: ''
                    },
                    faqs: faqs
                }, () =>{
                    successAlert(response.status);
                });
            })
            .catch( error => {
                storeError(error.response.status);
            })

    }

    update(){
        let i = findIndex(this.state.faqs, {id: this.state.selectedFaq.id});
        Axios.put(this.path() +'/'+this.state.selectedFaq.id, this.state.selectedFaq)
            .then( response => {
                let faqs = this.state.faqs;
                faqs[i] = this.state.selectedFaq;

                this.setState({
                    faqs: faqs,
                    selectedFaq: null
                },() => {
                    $('#edit_faq').modal('hide')
                });

            })
            .catch( error => {
                storeError(error.response.status);
            })
    }

    destroy(id){
        let i = findIndex(this.state.faqs, {id: id});

        Axios.delete(this.path() +'/'+ id)
            .then( response => {
                let faqs = this.state.faqs;
                faqs.splice(i, 1);

                this.setState({
                    faqs: faqs
                });
            })
            .catch( error => {
                destroyError(error.response.status);
            })
    }

    onDragEnd(result){
        const { destination, source, draggableId } = result;

        if(!destination) 
            return;

        if(
            destination.draggableId === source.draggableId &&
            destination.index === source.index
        )
            return;
        
        let newFaqs = Array.from(this.state.faqs);
        newFaqs.splice(source.index, 1);
        const item = find(this.state.faqs, {id: parseInt(draggableId)});
        newFaqs.splice(destination.index, 0, {
            ...item,
            sort: destination.index
        })

        this.setState({
            faqs: newFaqs
        }, () => {
            Axios.patch(this.path() + '/sort', {
                faqs: this.state.faqs
            }).then( response => {
                successAlert(response.status);
            }).catch( error => {
                storeError(error.response.status);
            });
        });

    }

    componentDidMount(){
        Axios.get(this.path())
            .then( response => {
                this.setState({
                    faqs: response.data.data
                })
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }

    render(){
        return (
            <div className="container text-right">
                {/* Start edit Modal */}
                <div className="modal fade" id="edit_faq" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">ویرایش</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {this.state.selectedFaq != null ?
                        <div className="modal-body">
                            <div className="form-group">
                                <label htmlFor="title">عنوان</label>
                                <input type="text" className="form-control" value={ this.state.selectedFaq.title }
                                    onChange={ e => this.setState({
                                        selectedFaq:{
                                            ...this.state.selectedFaq,
                                            title: e.target.value
                                        }
                                    }) }/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="desc">توضیحات</label>
                                <textarea className="form-control" rows="10" value={this.state.selectedFaq.description}
                                    onChange={ e => this.setState({
                                        selectedFaq:{
                                            ...this.state.selectedFaq,
                                            description: e.target.value
                                        }
                                    })}>
                                </textarea>
                            </div>
                        </div>
                        :null}
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={ e => this.setState({
                                selectedFaq: null
                            }) }>انصراف</button>
                            <button type="button" className="btn btn-primary" onClick={ this.update }>ذخیره</button>
                        </div>
                        </div>
                    </div>
                </div>
                {/* End edit modal */}

                <div className="card">
                    <div className="card-header">
                        سوال جدید
                    </div>

                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="title">متن سوال</label>
                                    <input type="text" className="form-control" value={this.state.newFaq.title}
                                        onChange={ e => this.setState({
                                            newFaq:{
                                                ...this.state.newFaq,
                                                title: e.target.value
                                            }
                                        })}/>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <div className="label">پاسخ</div>
                                    <textarea className="form-control" rows="5"
                                        value={ this.state.newFaq.description }
                                        onChange={ e => this.setState({
                                            newFaq:{
                                                ...this.state.newFaq,
                                                description: e.target.value
                                            }
                                        })}>
                                        </textarea>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <button className="btn btn-primary" onClick={ this.store }>ذخیره</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="bg-light mt-3 d-flex">
                    {this.state.faqs.length ? 
                        <DragDropContext onDragEnd={ this.onDragEnd }>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>عنوان</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <Droppable droppableId={'faq'}>
                                {( provided ) => (
                                <tbody {...provided.droppableProps} ref={provided.innerRef}>
                                    {this.state.faqs.map( (faq, index) => (
                                        <Draggable key={faq.id} draggableId={faq.id.toString()} index={index}>
                                            {(provided) => (
                                                <tr {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef}>
                                                    <td>
                                                        { faNumber(++index) }
                                                    </td>
                                                    <td>
                                                        {faq.title}
                                                    </td>
                        
                                                    <td>
                                                        <button type="button" className="btn btn-sm btn-warning" data-toggle="modal" data-target="#edit_faq"
                                                            onClick={ e => this.setState({
                                                                selectedFaq: faq
                                                            })}>
                                                            <i className="fa fa-edit"></i>
                                                        </button>

                                                        <button type="button" className="btn btn-sm btn-danger mr-1" onClick={ e => this.destroy(faq.id) }>
                                                            <i className="fa fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </tbody>
                                )}
                                </Droppable>
                            </table>
                        </DragDropContext>
                        :
                    <div className="col-12 text-center alert alert-warning d-block">
                        سوالی یافت نشد
                    </div>
                    }
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Faq 
        type={document.getElementById('faq_container').dataset.type}
        id={document.getElementById('faq_container').dataset.id}/>,
     document.getElementById('faq_container'));

Faq.defaultProps = {
    type: 'treatments'
}
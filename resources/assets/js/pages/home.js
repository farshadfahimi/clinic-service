import 'owl.carousel'
// Owl-main-slider-carousel
$(document).ready(function(){
    $('.main-slider2').owlCarousel({
        animateIn: 'pulse',
        loop:true,
        margin:0,
        dots: false,
        nav:true,
        rtl:true,
        autoplayHoverPause:false,
        autoplay: false,
        autoHeight:true,
        smartSpeed: 2000,
        navText: [
            '<i class="fa fa-long-arrow-left"></i>',
            '<i class="fa fa-long-arrow-right"></i>'
        ],
        responsive: {
            0: {
                items: 1,
                center: false
            },
            480:{
                items:1,
                center: false
            },
            600: {
                items: 1,
                center: false
            },
            768: {
                items: 1
            },
            992: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    })
});

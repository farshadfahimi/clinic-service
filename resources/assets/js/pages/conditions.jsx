import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { notFound, storeError, destroyError, ConfirmDialog, successAlert } from '../services/alert';
import { faNumber, api, lang } from '../services/helper';
import filter from 'lodash/filter';
import findIndex from 'lodash/findIndex';

const defaultCondition = {
    lang: 'fa',
    name: '',
    parent_id: 0,
    is_published: true
}

export default class Conditions extends Component{
    constructor(props){
        super(props);

        this.state = ({
            condition: defaultCondition,
            conditions: [],
            filters: {},
            filterConditions: []
        })

        this.setCondition = this.setCondition.bind(this);
        this.store = this.store.bind(this);
        this.destroy = this.destroy.bind(this);
        this.setFilter = this.setFilter.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
    }

    componentDidMount(){
        Axios.get(api('/conditions'))
            .then( response => {
                this.setState({
                    conditions: response.data.data,
                    filterConditions: filter(response.data.data, (item) => { return item.parent == null })
                });
            })
            .catch( error => {
                notFound(error.response.status);
            })
    }

    store(){
        let condition = this.state.condition;
        condition.parent_id == 0 && delete condition.parent_id;
        Axios.post(api('/conditions'), condition)
            .then( response => {
                let conditions = this.state.conditions;
                conditions.unshift(response.data.data);

                this.setState({
                    conditions: conditions,
                    condition: defaultCondition
                })

                successAlert(200)
            })
            .catch( error => {
                storeError(error.response.status);
            });
    }

    destroy(id){
        let conditions = this.state.conditions;
        let i = findIndex(conditions, {id: id});
        conditions.splice(i, 1);

        Axios.delete(api('/conditions/'+id))
            .then( response => {
                this.setState({
                    conditions: conditions
                })

                successAlert(200, 'اطلاعات با موفقیت حذف گردید')
            })
            .catch( error => {
                destroyError(error.response.status);
            });
    }

    setCondition(target, value){
        this.setState({
            condition:{
                ...this.state.condition,
                [target]: value
            }
        });
    }

    changeStatus(condition){
        let { conditions } = this.state;
        let i = findIndex(conditions, {id: condition.id});
        conditions[i].is_published = !conditions[i].is_published;
        
        this.setState({
            conditions: conditions
        });

        Axios.patch(api('/conditions/' + condition.id + '/is_published'), {
            is_published: condition.is_published
        }).then( response => { successAlert(200) })
            .catch( error => {
                storeError(error.response.status);

                conditions[i].is_published = !conditions[i].is_published;
        
                this.setState({
                    conditions: conditions
                });
            });
    }

    setFilter(target, value){
        this.setState({
            filters:{
                ...this.state.filters,
                [target]: value
            }
        }, () => {
            this.applyFilter()
        });
    }

    applyFilter(){
        let { conditions, filters } = this.state;
        let data = conditions;
        
        if(filters.name && filters.name != ''){
            data = filter(conditions, (condition) => {
                return condition.name.indexOf(filters.name) >= 0 
            });
        }

        if(filters.lang){
            data = filter(conditions, {lang: filters.lang});
        }

        this.setState({
            filterConditions: data
        });
    }

    render(){
        return(
            <div className="container">
                <div className="card text-right mb-3">
                    <div className="card-header">
                        بیماری ها
                    </div>
                    {/* start create condition */}
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" value={ this.state.condition.name } onChange={ e => this.setCondition('name', e.target.value) }/>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="form-group">
                                    <label htmlFor="">زبان</label>
                                    <select className="custom-select" value={ this.state.condition.lang } id="lang"
                                        onChange={ e => this.setCondition('lang', e.target.value)}>
                                        <option value="">انتخاب کنید</option>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="form-group">
                                    <label htmlFor="">بیماری</label>
                                    <select className="custom-select" value={ this.state.condition.parent_id } id="parent"
                                        onChange={ e => this.setCondition('parent_id', e.target.value) }>
                                        <option value="0">هیچ کدام</option>
                                        {filter(this.state.conditions,{parent: null, lang: this.state.condition.lang.toUpperCase()}).map( condition => (
                                            <option value={condition.id}>{ condition.name }</option>
                                        ))}
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group form-check">
                                    <input type="checkbox" className="form-check-input" id="is_published" checked={ this.state.condition.is_published }
                                    onChange={ e => this.setCondition('lang', e.target.checked)}/>
                                    <label className="form-check-label mr-3" htmlFor="is_published">نمایش</label>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <button type="button" className="btn btn-sm btn-primary" disabled={this.state.condition.name == ''}
                                    onClick={ this.store }>اضافه کردن</button>
                            </div>
                        </div>
                    </div>
                    {/* end create condition */}
                </div>

                {/* start search section */}
                {this.state.conditions.length ? 
                <div className="card mb-1">
                    <div className="card-header text-right">جستجو در نتایج

                        <button className="btn btn-sm btn-warning float-left" 
                            onClick={() => this.setState({filters: {}, filterConditions: this.state.conditions})}>حذف فیلتر ها</button>
                    </div>

                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group text-right">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" onChange={ e => this.setFilter('name', e.target.value) }/>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="form-group text-right">
                                    <label htmlFor="">زبان</label>
                                    <select className="custom-select" id="lang" defaultValue="fa"
                                        onChange={ e => this.setFilter('lang', e.target.value.toUpperCase())}>
                                        <option value="">انتخاب کنید</option>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                :
                null}
                {/* end search section */}

                {this.state.filterConditions.length ? 
                <table className="table table-striped text-right">
                    <thead className="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>دسته بندی</th>
                            <th>زبان</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.filterConditions.map( (condition, index) => (
                            <tr key={index}>
                                <td>{ faNumber(++index) }</td>
                                <td>{ condition.name }</td>
                                <td>{ condition.parent ? condition.parent.name : '' }</td>
                                <td>{ lang(condition.lang) }</td>
                                <td>
                                    <a href={`${location.href}/${condition.id}/edit`} className="btn btn-sm btn-warning ml-2">
                                        <i className="fa fa-edit"></i>
                                    </a>

                                    <a href={`${location.href}/${condition.id}/seo`} className="btn btn-sm btn-primary ml-2">
                                        seo
                                    </a>

                                    <button type="button" className={`btn btn-sm ml-2 ${condition.is_published ? 'btn-success' : 'btn-danger'}`} 
                                        onClick={ e => this.changeStatus(condition)}>
                                        {condition.is_published ?
                                            <i className="fa fa-eye"></i>
                                            :
                                            <i className="fa fa-eye-slash"></i>
                                        }
                                    </button>

                                    <a href={`${location.href}/${condition.id}/faq`} className="btn btn-sm btn-info ml-2">
                                        <i className="fa fa-question"></i>
                                    </a>

                                    <button type="button" className="btn btn-sm btn-danger" onClick={ e => ConfirmDialog('آیا از حذف اطمینان دارید؟', () => this.destroy(condition.id)) }>
                                        <i className="fa fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                :
                <div className="alert alert-warning text-center">لیست خالی میباشد</div>
                }
            </div>
        )
    }
}

ReactDOM.render(<Conditions/>, document.getElementById('conditions_container'));
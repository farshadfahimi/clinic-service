import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { notFound, storeError, destroyError, ConfirmDialog, successAlert } from '../services/alert';
import { faNumber, api, lang } from '../services/helper';
import filter from 'lodash/filter';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';

const defaultTreatment = {
    lang: 'FA',
    name: '',
    parent_id: 0,
    is_published: true
}

export default class Treatments extends Component{
    constructor(props){
        super(props);

        this.state = ({
            treatment: defaultTreatment,
            treatments: [],
            treatmentsFilter: [],
            filters: {},
            parents: [],
        });

        this.setTreatment = this.setTreatment.bind(this);
        this.store = this.store.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.destroy = this.destroy.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.setParents = this.setParents.bind(this);
    }

    componentDidMount(){
        Axios.get(api('/treatments'))
            .then( response => {
                this.setState({
                    treatments: response.data.data,
                    treatmentsFilter: filter(response.data.data, (item) => { return item.parent == null })
                }, () => {
                    this.setParents();
                });

            })
            .catch( error => {
                notFound(error.response.status);
            })
    }

    setParents(){
        let parents = this.state.treatments.filter( treatment => {
            return treatment.parent_id == 0;
        });

        this.setState({
            parents: parents
        });
    }

    setFilter(target, value){
        this.setState({
            filters:{
                ...this.state.filters,
                [target]: value
            }
        }, () => {
            this.applyFilter()
        });
    }

    applyFilter(){
        let { treatments, filters } = this.state;
        let data = treatments;
        
        if(filters.name && filters.name != ''){
            data = filter(treatments, (treatment) => {
                return treatment.name.indexOf(filters.name) >= 0 
            });
        }

        if(filters.lang){
            data = filter(treatments, {lang: filters.lang});
        }

        if(filters.parent_id)
            data = filter(treatments, {parent_id: parseInt(filters.parent_id)  })

        this.setState({
            treatmentsFilter: data
        });
    }

    store(){
        let treatment = this.state.treatment;
        treatment.parent_id == 0 && delete treatment.parent_id;
        Axios.post(api('/treatments'), treatment)
            .then( response => {
                let treatments = this.state.treatments;
                treatments.unshift(response.data.data);

                this.setState({
                    treatments: treatments,
                    treatmentsFilter: treatments,
                    treatment: defaultTreatment
                })

                successAlert(200)
            })
            .catch( error => {
                storeError(error.response.status);
            });
    }

    destroy(id){
        let treatments = this.state.treatments;
        let i = findIndex(treatments, {id: id});
        treatments.splice(i, 1);

        Axios.delete(api('/treatments/'+id))
            .then( response => {
                this.setState({
                    treatments: treatments,
                    treatmentsFilter: treatments
                })

                successAlert(200, 'اطلاعات با موفقیت حذف گردید')
            })
            .catch( error => {
                destroyError(error.response.status);
            });
    }

    setTreatment(target, value){
        this.setState({
            treatment:{
                ...this.state.treatment,
                [target]: value
            }
        });
    }

    changeStatus(treatment){
        let { treatments } = this.state;
        let i = findIndex(treatments, {id: treatment.id});
        treatments[i].is_published = !treatments[i].is_published;
        
        this.setState({
            treatments: treatments
        });

        Axios.patch(api('/treatments/' + treatment.id + '/is_published'), {
            is_published: treatment.is_published
        }).then( response => { successAlert(200) })
            .catch( error => {
                storeError(error.response.status);

                treatments[i].is_published = !treatments[i].is_published;
        
                this.setState({
                    treatments: treatments
                });
            });
    }

    get categories()
    {
        return this.state.filters.lang ? filter(this.state.parents,{ lang: this.state.filters.lang.toUpperCase() }) : this.state.parents;
    }

    render(){
        return(
            <div className="container">
                <div className="row">
                    {/* start create treatments */}
                    <div className="col-md-6">
                        <div className="card text-right mb-3">
                        <div className="card-header">
                            راه های درمان
                        </div>

                        <div className="card-body">
                            <div className="form-group">
                                <label htmlFor="title">عنوان</label>
                                <input type="text" className="form-control" value={ this.state.treatment.name } onChange={ e => this.setTreatment('name', e.target.value) }/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="">زبان</label>
                                <select className="custom-select" defaultValue={ this.state.treatment.lang } id="lang"
                                    onChange={ e => this.setTreatment('lang', e.target.value)}>
                                    <option value="FA">فارسی</option>
                                    <option value="AR">عربی</option>
                                    <option value="EN">انگلیسی</option>
                                </select>
                            </div>

                            <div className="form-group">
                                <label htmlFor="">درمان اصلی</label>
                                <select className="custom-select" defaultValue={ this.state.treatment.parent_id } id="parent"
                                    onChange={ e => this.setTreatment('parent_id', e.target.value) }>
                                    <option value="0">هیچ کدام</option>
                                    {filter(this.state.treatments,{parent_id: 0, lang: this.state.treatment.lang }).map( treatment => (
                                        <option value={treatment.id} key={treatment.id}>{ treatment.name }</option>
                                    ))}
                                </select>
                            </div>

                            <div className="form-group form-check">
                                <input type="checkbox" className="form-check-input" id="is_published" checked={ this.state.treatment.is_published }
                                onChange={ e => this.setTreatment('is_published', e.target.checked)}/>
                                <label className="form-check-label mr-3" htmlFor="is_published">نمایش</label>
                            </div>

                            <button type="button" className="btn btn-sm btn-primary" disabled={this.state.treatment.name == ''}
                            onClick={ this.store }>اضافه کردن</button>
                        </div>
                    </div>
                    </div>
                    {/* end create treatments */}
                    
                    {/* start search section */}
                    {this.state.treatments.length ?
                    <div className="col-md-6">
                        <div className="card mb-1">
                            <div className="card-header text-right">جستجو در نتایج

                                <button className="btn btn-sm btn-warning float-left" 
                                    onClick={() => this.setState({filters: {}, treatmentsFilter: this.state.treatments})}>حذف فیلتر ها</button>
                            </div>

                            <div className="card-body">
                                <div className="form-group text-right">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" onChange={ e => this.setFilter('name', e.target.value) }/>
                                </div>

                                <div className="form-group text-right">
                                    <label htmlFor="">زبان</label>
                                    <select className="custom-select" id="lang"
                                        defaultValue=""
                                        onChange={ e => this.setFilter('lang', e.target.value)}>
                                        <option value="">انتخاب کنید</option>
                                        <option value="FA">فارسی</option>
                                        <option value="AR">عربی</option>
                                        <option value="EN">انگلیسی</option>
                                    </select>
                                </div>

                                <div className="form-group text-right">
                                    <label htmlFor="">درمان اصلی</label>
                                    <select className="custom-select" id="lang"
                                        defaultValue=""
                                        onChange={ e => this.setFilter('parent_id', e.target.value)}>
                                        <option value="">انتخاب کنید</option>
                                        {this.categories.map( parent => (
                                            <option value={parent.id} key={parent.id}>{ parent.name }</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    :null }
                    {/* end search section */}
                </div>

                {this.state.treatmentsFilter.length ? 
                <table className="table table-striped text-right">
                    <thead className="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>دسته بندی</th>
                            <th>زبان</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.treatmentsFilter.map( (treatment, index) => (
                        <tr key={treatment.id}>
                            <td>{ faNumber(++index) }</td>

                            <td>{ treatment.name }</td>

                            <td>
                                { treatment.parent ? treatment.parent.name : '' }
                            </td>

                            <td>{ lang(treatment.lang) }</td>

                            <td>
                                <a href={`${location.href}/${treatment.id}/seo`} className="btn btn-sm btn-primary ml-2" title="سئو">
                                    seo
                                </a>

                                <a href={`${location.href}/${treatment.id}/edit`} className="btn btn-sm btn-warning ml-2" title="ویرایش">
                                    <i className="fa fa-edit"></i>
                                </a>


                                <a href={`${location.href}/${treatment.id}/pricing`} className="btn btn-sm btn-info ml-2" title="قیمت گذاری">
                                    <i className="fa fa-money-bill-wave"></i>
                                </a>

                                <a href={`${location.href}/${treatment.id}/faq`} className="btn btn-sm btn-info ml-2" title="سوالات متداول">
                                    <i className="fa fa-question"></i>
                                </a>

                                <a href={`${location.href}/${treatment.id}/requests`} className="btn btn-sm btn-info ml-2" title="درخواست مشاوره">
                                    <i className="fa fa-comments"></i>
                                </a>

                                <a href={`${location.href}/${treatment.id}/comments`} className="btn btn-sm btn-info ml-2" title="ثبت نظر">
                                    <i className="fa fa-comment"></i>
                                </a>

                                <button type="button" title={treatment.is_published ? 'نمایش' : 'عدم نمایش'} className={`btn btn-sm ml-2 ${treatment.is_published ? 'btn-success' : 'btn-danger'}`} 
                                    onClick={ e => this.changeStatus(treatment)}>
                                    {treatment.is_published ?
                                        <i className="fa fa-eye"></i>
                                        :
                                        <i className="fa fa-eye-slash"></i>
                                    }
                                </button>

                                <button type="button" title="حذف" className="btn btn-sm btn-danger" onClick={ e => ConfirmDialog('آیا از حذف اطمینان دارید؟', () => this.destroy(treatment.id)) }>
                                    <i className="fa fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
                :
                <div className="alert alert-warning text-center">لیست خالی میباشد</div>
                }
            </div>
        )
    }
}

ReactDOM.render(<Treatments/>, document.getElementById('treatments_container'));
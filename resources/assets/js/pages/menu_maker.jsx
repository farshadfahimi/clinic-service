import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { api } from '../services/helper';
import Select from 'react-select';

export default class ManuMaker extends Component {
    constructor(props) {
        super(props);

        this.state = ({
            conditions: [],
            treatments: [],
            items: {},
            columns: {
                'column-0': {
                    id: 'column-0',
                    itemsId: []
                }
            },
            columnOrders: ['column-0']
        });

        this.onDragEnd = this.onDragEnd.bind(this);
        this.addCondition = this.addCondition.bind(this);
        this.addNewColumn = this.addNewColumn.bind(this);
    }

    componentDidMount() {
        Axios.get(api('/conditions'))
            .then(response => {
                response.data.data.map(item => {
                    item.label = item.name;
                    item.value = item.id;
                });
                this.setState({
                    conditions: response.data.data
                })
            })
            .catch(error => {
                console.log(error);
            });

        Axios.get(api('/treatments'))
            .then(response => {
                this.setState({
                    treatments: response.data.data
                })
            })
            .catch(error => {
                console.log(error);
            });
    }

    addCondition(value) {
        let { items } = this.state;

        items = {
            ...items,
            ["item-" + value.id]: {
                id: value.id,
                label: value.label,
                childs: {}
            }
        }

        this.setState({
            items: items
        }, () => {
            this.setState({
                columns: {
                    ...this.state.columns,
                    'column-0': {
                        ...this.state.columns['column-0'],
                        itemsId: Object.keys(this.state.items)
                    }
                }
            })
        });
    }

    addNewColumn(item) {
        if(!Object.keys(item.childs).length)
            return;

        let { columns } = this.state;
        const id = Object.keys(columns).length;
        
        columns = {
            ...columns,
            ['column-' + id]:{
                id: 'column-' + id,
                parent: 'item-' + item.id,
                itemsId: Object.keys(item.childs)
            }
        }
        
        this.setState({
            columns: columns,
            columnOrders: Object.keys(columns)
        })
    }

    onDragEnd(result) {
        const { destination, source, draggableId, combine } = result;
        const { items } = this.state;
        const start = this.state.columns[source.droppableId];

        if (combine != null) {
            const newItemsId = Array.from(start.itemsId);
            // find the dragged object
            let droppedItem = items[newItemsId[source.index]];
            // find the drop item
            let draggedItem = items[combine.draggableId];
            // put data in dropped
            draggedItem.childs = {
                ...draggedItem.childs,
                ['item-' + droppedItem.id]: droppedItem
            }

            delete items[newItemsId[source.index]]

            newItemsId.splice(source.index, 1);
            this.setState({
                columns: {
                    ...this.state.columns,
                    [start.id]: {
                        ...this.state.columns[start.id],
                        itemsId: newItemsId
                    }
                },
                items: items
            });

            return;

        }

        if (!destination)
            return;


        const finished = this.state.columns[destination.droppableId];

        if (destination.droppableId === source.droppableId && destination.index === source.index)
            return;


        if (start === finished) {
            const newItemsId = Array.from(start.itemsId);
            newItemsId.splice(source.index, 1);
            newItemsId.splice(destination.index, 0, draggableId);

            this.setState({
                columns: {
                    ...this.state.columns,
                    [start.id]: {
                        ...this.state.columns[start.id],
                        itemsId: newItemsId
                    }
                }
            });

            return;
        }

        const startItemsIds = Array.from(start.itemsId);
        startItemsIds.splice(source.index, 1);

        const startColumn = {
            ...start,
            itemsId: startItemsIds
        }

        const finishedItemsIds = Array.from(finished.itemsId);
        finishedItemsIds.splice(destination.index, 0, draggableId);

        const finishedColumn = {
            ...finished,
            itemsId: finishedItemsIds
        };

        this.setState({
            columns: {
                ...this.state.columns,
                [startColumn.id]: startColumn,
                [finishedColumn.id]: finishedColumn
            }
        })
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <div className="form-group text-right">
                        <label htmlFor="condition">جستجوی بیماری</label>
                        <Select options={this.state.conditions} onChange={this.addCondition} />
                    </div>
                </div>

                <div className="col-12">
                    <DragDropContext
                        onDragEnd={this.onDragEnd}>
                        <div className="col-md-6">
                            {this.state.columnOrders.map(columnItem => {
                                const column = this.state.columns[columnItem];
                                
                                return <Droppable droppableId={column.id}
                                    key={column.id}
                                    isCombineEnabled={true}>
                                    {(provided, snapshot) => (
                                        <ul className={`list-group p-4 ${snapshot.isDraggingOver ? 'bg-primary' : ''}`}
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}>
                                            {column.itemsId.map((item, index) => {
                                                return <ColumnItem key={item} item={this.state.items[item]} index={index} 
                                                    onDoubleClick={ e => this.addNewColumn(this.state.items[item]) }/>
                                            })}

                                            {provided.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            })}
                        </div>
                    </DragDropContext>
                </div>
            </div>
        );
    }
}


export class ColumnItem extends Component {
    render() {
        return <Draggable draggableId={'item-' + this.props.item.id} index={this.props.index}>
            {(provided, snapshot) => (
                <li onDoubleClick={ this.props.onDoubleClick }
                    className={`list-group-item ${snapshot.isDragging ? 'bg-secondary' : ''}`}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}>
                    {this.props.item.label}
                </li>
            )}
        </Draggable>;
    }
}



ReactDOM.render(<ManuMaker />, document.getElementById('menu_content'))
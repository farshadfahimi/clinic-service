import React,{ Component } from 'react';
import Select from 'react-select/creatable';
import ReactDOM from 'react-dom';
import { api, faNumber, lang } from '../services/helper';
import { notFound, storeError, destroyError, ConfirmDialog } from '../services/alert';
import Axios from 'axios';
import findIndex from 'lodash/findIndex';

export default class DepartmentList extends Component{
    constructor(props)
    {
        super(props);

        this.state = ({
            selectedDepartment: null,
            departments:[],
            department: {
                lang: 'fa'
            },
            tags: []
        })

        this.store = this.store.bind(this);
        this.delete = this.delete.bind(this);
        this.update = this.update.bind(this);
        this.setDepartment = this.setDepartment.bind(this);
    }

    setDepartment(target, value){
        this.setState({
            department:{
                ...this.state.department,
                [target]: value
            }
        })
    }

    delete(id){
        let departments = this.state.departments;
        let i = findIndex(departments,{id: id});
        departments.splice(i,1);

        Axios.delete(api('/departments/'+id))
            .then(response => {
                this.setState({
                    departments
                });
            })
            .catch( error => {
                destroyError(error.response.status);
            })
    }

    store(){
        Axios.post(api('/departments'), this.state.department)
        .then( response => {
            let departments = this.state.departments;
            departments.unshift(response.data.data);

            this.setState({
                departments: departments,
                department: {
                    lang: 'fa'
                }
            })  
        })
        .catch( error => {
            storeError(error.response.status);
        })
    }

    update(){
        let departments = this.state.departments;
        let i = findIndex(departments,{id: this.state.selectedDepartment.id});

        Axios.put(api('/departments/'+this.state.selectedDepartment.id), this.state.selectedDepartment )
        .then( response => {
            departments[i] = this.state.selectedDepartment;

            this.setState({
                selectedDepartment: null,
                departments: departments
            });

            $('#edit_department').modal('hide')
        })
        .catch( error => {
            storeError(error.response.status);
        });
    }

    handleImage(e, target){
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = () => {
            this.setState({
                [target]:{
                    ...this.state[target],
                    cover: reader.result
                }
            })
        }
    }

    componentDidMount()
    {
        Axios.get(api('/departments'))
            .then( response => {
                this.setState({
                    departments: response.data.data
                })
            })
            .catch( error => {
                notFound(error.response.status);
            });

        Axios.get(api('/tags'))
            .then( response => {
                this.setState({
                    tags: response.data.data
                })
            })
            .catch( error => {
                notFound(error.response.status);
            })
    }

    render(){
        return (
        <div>
            <div className="modal fade" id="edit_department" tabIndex="-1" role="dialog" aria-modal="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">ویرایش شعبه</h5>
                        <button type="button" className="mr-auto btn" data-dismiss="modal" aria-label="Close" onClick={ e => this.setState({ selectedDepartment: null }) }>
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {this.state.selectedDepartment != null &&
                    <div className="modal-body text-right">
                            <div className="form-group">
                                <label htmlFor="title">عنوان</label>
                                <input type="text" className="form-control" defaultValue={ this.state.selectedDepartment.name } 
                                    onChange={ e => this.setState({
                                        selectedDepartment: {
                                            ...this.state.selectedDepartment,
                                            name: e.target.value
                                        }
                                    }) }/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="phone">شماره تماس</label>
                                <input type="text" className="form-control" defaultValue={ this.state.selectedDepartment.phone }
                                    onChange={ e => this.setState({
                                        selectedDepartment: {
                                            ...this.state.selectedDepartment,
                                            phone: e.target.value
                                        }
                                    }) }/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="address">آدرس</label>
                                <input type="text" className="form-control" defaultValue={ this.state.selectedDepartment.address }
                                    onChange={ e => this.setState({
                                        selectedDepartment: {
                                            ...this.state.selectedDepartment,
                                            address: e.target.value
                                        }
                                    })}/>
                            </div>

                            <div className="form-group">
                                <select className="custom-select" value={this.state.selectedDepartment.lang} id="lang"
                                    onChange={ e => this.setState({
                                        selectedDepartment:{
                                            ...this.state.selectedDepartment,
                                            lang: e.target.value
                                        }
                                    }) }>
                                    <option value="fa">فارسی</option>
                                    <option value="ar">عربی</option>
                                    <option value="en">انگلیسی</option>
                                </select>
                            </div>

                            <div className="custom-file">
                                <input type="file" name="cover" className="custom-file-input" id="cover"
                                    onChange={ e => this.handleImage(e, 'selectedDepartment')}/>
                                <label className="custom-file-label text-left cover" htmlFor="cover">انتخاب کاور</label>
                                <small>سایز فایل: ۳۳۳*۵۰۰</small>
                            </div>
                    </div>
                    }

                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={ e => this.setState({selectedDepartment: null}) }>بستن</button>
                        <button type="button" className="btn btn-primary mr-2" onClick={ this.update }>ذخیره</button>
                    </div>

                    </div>
                </div>
            </div>

            <div className="card text-right mb-3">
                <div className="card-header">
                    ایجاد شعبه جدید
                </div>

                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="name">عنوان</label>
                                <input type="text" className="form-control" 
                                    onChange={ e => this.setDepartment('name', e.target.value) }/>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="name">شماره تماس</label>
                                <input type="text" className="form-control"
                                    onChange={ e => this.setDepartment('phone', e.target.value) }/>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="name">آدرس</label>
                                <input type="text" className="form-control"
                                    onChange={ e => this.setDepartment('address', e.target.value) }/>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <label htmlFor="">تگ</label>
                            <Select
                                isClearable={true}
                                isMulti={true}
                                options={ this.state.tags }
                                onChange={ (selected, action) => this.setDepartment('tags', selected) }
                            />
                        </div>
                        <div className="col-md-6">
                            <div className="custom-file">
                                <input type="file" name="cover" className="custom-file-input" id="cover"
                                    onChange={ e => this.handleImage(e, 'department')}/>
                                <label className="custom-file-label text-left cover" htmlFor="cover">انتخاب کاور</label>
                                <small>سایز فایل: ۳۳۳*۵۰۰</small>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">زبان</span>
                                </div>

                                <select className="custom-select" value={this.state.department.lang} id="lang"
                                    onChange={ e => this.setDepartment('lang', e.target.value) }>
                                    <option value="fa">فارسی</option>
                                    <option value="ar">عربی</option>
                                    <option value="en">انگلیسی</option>
                                </select>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <button className="btn btn-sm btn-primary" onClick={ this.store }>ذخیره</button>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        {this.state.departments.length > 0 ?
            <table className="table table-striped">
                <thead className="thead-dark">
                    <tr className="text-right">
                        <th>#</th>
                        <th>عنوان شعبه</th>
                        <th>شماره تماس</th>
                        <th>آدرس</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    {this.state.departments.map( department =>{
                        return (
                            <tr className="text-right" key={department.id}>
                                <td>
                                    <img src={department.cover} className="img-thumbnail"/>
                                </td>

                                <td>{ department.name }</td>

                                <td>{ faNumber(department.phone) }</td>

                                <td>{ faNumber(department.address) }</td>

                                <td className="text-left">
                                    {this.state.edit != department.id ?
                                        <div className="form-group">
                                            <button className="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit_department" onClick={() => this.setState({selectedDepartment: department})}>ویرایش</button>
                                            <button className="btn btn-sm btn-danger mr-2" onClick={() => ConfirmDialog('آیا از حذف شعبه اطمینان دارید؟', () => this.delete(department.id))}>حذف</button>
                                        </div>
                                        :
                                        <div className="form-group">
                                            <button className="btn btn-sm btn-outline-success" onClick={() => { this.setDepartmentName(department.id) }}>تایید</button>
                                            <button className="btn btn-sm btn-outline-danger mr-2" onClick={this.setDefaultName}>انصراف</button>
                                        </div>
                                    }
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            :
            <div className="alert alert-warning text-center">
                شعبه ای ایجاد نشده است
            </div>
        }
        </div>
        );
    }
}

ReactDOM.render(
    <DepartmentList/>,document.getElementById('departments_container')
)
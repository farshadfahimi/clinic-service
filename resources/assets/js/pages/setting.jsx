import React,{ Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { api } from '../services/helper';
import { notFound, storeError, successAlert } from '../services/alert';
import findIndex from 'lodash/findIndex';

export default class Settings extends Component{
    constructor(props){
        super(props);

        this.state = {
            settings: [],
            defaultSetting: {
                id: null,
                lang: 'fa',
                title: '',
                keywords: '',
                description: '',
                email: '',
                phone: '',
                fax: '',
                address: '',
                desc: '',
                companyPhone: '',
            },
            selectedSetting: {
                id: null,
                lang: 'fa',
                title: '',
                keywords: '',
                description: '',
                email: '',
                phone: '',
                fax: '',
                address: '',
                desc: '',
                companyPhone: ''
            },
            buzy: false
        }

        this.store = this.store.bind(this);
    }

    componentDidMount(){
        Axios.get(api('/settings'))
            .then( response => {
                let i = findIndex(response.data.data, {lang: 'fa'});
                let selectedSetting = response.data.data[i]

                this.setState({
                    settings: response.data.data,
                    selectedSetting: selectedSetting ? selectedSetting : this.state.defaultSetting
                });
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }

    store(){
        this.setState({
            buzy: true
        }, () => {
            Axios.post(api('/settings'), this.state.selectedSetting)
                .then( response => {
                    this.setState({
                        buzy: false,    
                    });

                    successAlert(response.status);
                })
                .catch( error => {
                    storeError(error.response.status);
                });
        });
    }

    setLanguage(lang){
        let i = findIndex(this.state.settings, {lang: lang});
        
        this.setState({
            selectedSetting: i >= 0 ? 
                this.state.settings[i] 
                : 
                {...this.state.defaultSetting, lang: lang}
        });
    }

    render(){
        return(
            <div className="container">
                <div className="card">
                    <div className="card-body">
                        <div className="row text-right">
                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="lang">زبان</label>
                                    <select className="custom-select" defaultValue={this.state.selectedSetting.lang} id="lang"
                                        onChange={ e => this.setLanguage(e.target.value) }>
                                        <option value="fa">فارسی</option>
                                        <option value="ar">عربی</option>
                                        <option value="en">انگلیسی</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="title">عنوان</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.title}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                title: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="keywords">کلمات کلیدی</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.keywords}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                keywords: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="description">توضیحات</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.description}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                description: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="email">ایمیل</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.email}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                email: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="phone">شماره موبایل</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.phone}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                phone: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="fax">فکس</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.fax}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                fax: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="fax">تلفن ثابت</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.companyPhone}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                companyPhone: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="address">آدرس</label>
                                    <input type="text" className="form-control" value={this.state.selectedSetting.address}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                address: e.target.value
                                            }
                                        }) }/>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="desc">توضیحات پایین صفحه</label>
                                    <textarea id="desc" className="form-control" value={this.state.selectedSetting.desc}
                                        onChange={ e => this.setState({
                                            selectedSetting:{
                                                ...this.state.selectedSetting,
                                                desc: e.target.value
                                            }
                                        })}></textarea>
                                </div>
                            </div>
                                    
                            <div className="col-12">
                                <button className="btn btn-primary" type="button" disabled={this.state.buzy} onClick={ this.store }>
                                    {this.state.buzy ?
                                        <Fragment>
                                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            در حال ذخیره اطلاعات
                                        </Fragment>
                                        :
                                        'ذخیره'
                                    }
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Settings/>, document.getElementById('settings_container'));
import React,{ Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import findIndex from 'lodash/findIndex';
import { storeError, notFound, destroyError, successAlert, ConfirmDialog } from '../services/alert';
import { api, lang, jdate } from '../services/helper';
import Swal from 'sweetalert2';

export default class Employees extends Component{
    constructor(props){
        super(props);

        this.state = {
            employees: [],
            selectedEmployee: null,
            employee: {
                name: '',
                expert: '',
                lang: 'fa',
                avatar: null,
                description: '',
                values: []
            },
            defaultValue:{
                title: '',
                value: ''
            }
        }

        this.uploadImage = this.uploadImage.bind(this);
        this.setEmployee = this.setEmployee.bind(this);
        this.setSelectedEmployee = this.setSelectedEmployee.bind(this);
        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
        this.destroy = this.destroy.bind(this);
        this.updatevalue = this.updatevalue.bind(this);
        this.storeValues = this.storeValues.bind(this);
        this.appendInfoItem = this.appendInfoItem.bind(this);
        this.updateContact = this.updateContact.bind(this);
        this.storeContact = this.storeContact.bind(this);
    }

    componentDidMount(){
        Axios.get(api('/employees'))
            .then( response => {
                this.setState({
                    employees: response.data.data
                })
            })
            .catch( error => {
                notFound(error.response.status);
            });
    }

    appendInfoItem(){
        let values = this.state.selectedEmployee.values;

        if(values == null)
            values = [this.state.defaultValue];
        else
            values.push(this.state.defaultValue);

        this.setState({
            selectedEmployee: {
                ...this.state.selectedEmployee,
                values: values
            }
        })
    }

    store(){
        Axios.post(api('/employees'), this.state.employee)
            .then( response => {
                let { employees } = this.state;
                employees.shift(response.data.data);

                this.setState({
                    employees
                });
                
                $('#new_employees').modal('hide');

                successAlert(response.status);
            })
            .catch( error => {
                storeError(error.response.status);
            });
    }

    update(){
        Axios.put(api('/employees/'+this.state.selectedEmployee.id), this.state.selectedEmployee)
            .then( response => {
                let { employees } = this.state;
                let i = findIndex(employees, {id: this.state.selectedEmployee.id});
                employees[i] = this.state.selectedEmployee;
                this.setState({
                    employees: employees,
                    selectedEmployee: null
                });

                $('#employee_update').modal('hide');
                successAlert(response.status);
            })
            .catch( error => {
                storeError(error.response.status);
            })
    }

    setEmployee(target, value){
        this.setState({
            employee: {
                ...this.state.employee,
                [target]: value
            }
        })
    }

    setSelectedEmployee(target, value){
        this.setState({
            selectedEmployee: {
                ...this.state.selectedEmployee,
                [target]: value
            }
        })
    }

    storeValues(){
        let employees = this.state.employees;
        let i = findIndex(employees,{id: this.state.selectedEmployee.id});
        employees[i] = this.state.selectedEmployee

        Axios.patch(api('/employees/'+this.state.selectedEmployee.id+'/values'),{
            values: this.state.selectedEmployee.values
        }).then( response => {
                this.setState({
                    selectedEmployee: null,
                    employees: employees
                })        
            })
            .catch( error => {
                storeError(error.response.status);
            });
    }

    async destroy(id){
        let employees = this.state.employees;
        let i = findIndex(employees,{id: id});

        await Axios.delete(api('/employees/'+id))
            .then( response => {
                employees.splice(i, 1);

                this.setState({
                    employees: employees,
                });
            })
            .catch( error => {
                destroyError(error.response.status);
            });
    }

    destroyValue(i){
        let values = this.state.selectedEmployee.values;

        values.splice(i, 1);

        this.setState({
            selectedEmployee: {
                ...this.state.selectedEmployee,
                values: values
            }
        })
    }

    updatevalue(target, i, value){
        let values = this.state.selectedEmployee.values;

        values[i]={
            ...values[i],
            [target]: value
        }
        
        this.setState({
            selectedEmployee: {
                ...this.state.selectedEmployee,
                values: values
            }
        })
    }

    updateContact(target, value){
        this.setState({
            selectedEmployee: {
                ...this.state.selectedEmployee,
                contact:{
                    ...this.state.selectedEmployee.contact,
                    [target]: value
                }
            }
        })
    }

    storeContact(){
        let employees = this.state.employees;
        let i = findIndex(employees,{id: this.state.selectedEmployee.id});
        employees[i] = this.state.selectedEmployee

        Axios.patch(api('/employees/'+this.state.selectedEmployee.id+'/contact'),{
            values: this.state.selectedEmployee.contact
        }).then( response => {
            this.setState({
                employees: employees,
                selectedEmployee: null
            })
        })
        .catch( error => {
            storeError(error.response.status);
        });
    }

    uploadImage(e, type){
        let reader = new FileReader()
        reader.readAsDataURL(e.target.files[0]);

        reader.onload = (e) => {
            if(type == 'new')
                this.setEmployee('avatar', reader.result)
            else
                this.setSelectedEmployee('avatar', reader.result);
        }
    }

    render(){
        return(
            <div className="container">
                <button type="button" className="btn btn-primary w-100 mb-3" data-toggle="modal" data-target="#new_employees">افزودن نیروی جدید</button>
                {this.state.employees.length ? 
                <table className="table table-striped text-right">
                    <thead className="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>نام</th>
                            <th>زبان</th>
                            <th>آخرین به روز رسانی</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                    {this.state.employees.map( employee => (
                        <tr key={employee.id}>
                            <td>
                                <img src={employee.avatar} alt={employee.name}/>
                            </td>
                            <td>{ employee.name }</td>
                            <td>{ lang(employee.lang) }</td>
                            <td>{ jdate(employee.updated_at) }</td>
                            <td>
                                <button className="btn btn-sm btn-info ml-2" data-toggle="modal" data-target="#employee_info" onClick={ e => this.setState({
                                    selectedEmployee: employee
                                }) }>
                                    <i className="fa fa-info"></i>
                                </button>

                                <button className="btn btn-sm btn-warning ml-2" data-toggle="modal" data-target="#employee_update" onClick={ e => this.setState({
                                    selectedEmployee: employee
                                }) }>
                                    <i className="fa fa-edit"></i>
                                </button>

                                <button className="btn btn-sm btn-primary ml-2" data-toggle="modal" data-target="#employee_social" onClick={ e => this.setState({
                                    selectedEmployee: employee
                                }) }>
                                    <i className="fas fa-address-card"></i>
                                </button>

                                <button className="btn btn-sm btn-danger" onClick={ e => ConfirmDialog('آیا از حذف متخصص اطمینان دارید', () => this.destroy(employee.id)) }>
                                    <i className="fa fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                :
                <div className="alert alert-warning text-center">متخصصی یافت نشد</div>
                }

                {/* Start Store Modal */}
                <div className="modal fade" id="new_employees" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">افزودن متخصص</h5>

                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body text-right">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group" >
                                        <label htmlFor="lang">زبان</label>
                                        <select className="custom-select" id="lang" value={this.state.employee.lang}
                                            onChange={ e => this.setEmployee('lang', e.target.value)}>
                                            <option value="fa">فارسی</option>
                                            <option value="ar">عربی</option>
                                            <option value="en">انگلیسی</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>نام</label>
                                        <input type="text" className="form-control"
                                            value={this.state.employee.name}
                                            onChange={ e => this.setEmployee('name', e.target.value) }/>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label>تخصص</label>
                                        <input type="text" className="form-control"
                                            value={this.state.employee.expert}
                                            onChange={ e => this.setEmployee('expert', e.target.value )}/>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <label >تصویر</label>
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="avatar"
                                            onChange={ e => this.uploadImage(e, 'new')}/>
                                        <label className="custom-file-label text-left" htmlFor="avatar">انتخاب تصویر</label>
                                        <small>سایز فایل: ۴۵۰*۴۵۰</small>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <label>توضیحات</label>
                                    <textarea rows="2" className="form-control"
                                    value={this.state.employee.description}
                                    onChange={ e => this.setEmployee('description', e.target.value)}></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary ml-2" data-dismiss="modal">انصراف</button>
                            <button type="button" className="btn btn-primary" onClick={ this.store }>ذخیره</button>
                        </div>
                        </div>
                    </div>
                </div>
                {/* End Store Modal */}

                {/* Start Employee info Modal */}
                <div className="modal fade" id="employee_info" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">ورود اطلاعات</h5>

                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {this.state.selectedEmployee != null &&
                        <div className="modal-body text-right">
                            {this.state.selectedEmployee.values != null ?
                            <div className="row text-row">
                                {this.state.selectedEmployee.values.map( (item, i) => (
                                    <Fragment key={i}>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label htmlFor="title">عنوان</label>
                                                <input type="text" className="form-control" defaultValue={item.title}
                                                    onChange={ e => this.updatevalue('title', i, e.target.value) }/>
                                            </div>
                                        </div>

                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label htmlFor="title">توضیحات</label>
                                                <input type="text" className="form-control" defaultValue={item.value}
                                                    onChange={ e => this.updatevalue('value', i, e.target.value) }/>
                                            </div>
                                        </div>

                                        <div className="col-md-2 pt-4">
                                            <button className="btn btn-sm btn-danger mt-2" onClick={ e => this.destroyValue(i) }>
                                                <i className="fa fa-trash-alt"></i>
                                            </button>
                                        </div>
                                    </Fragment>
                                ))}
                            </div>
                            :null}

                            <button className="btn btn-sm btn-primary mt-3 w-100" onClick={ this.appendInfoItem }>
                            افزودن مشخصه جدید</button>
                        </div>
                        }
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary ml-2" data-dismiss="modal"
                                onClick={ e => this.setState({
                                    selectedEmployee: null
                                }) }>انصراف</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.storeValues }>ذخیره</button>
                        </div>
                        </div>
                    </div>
                </div>
                {/* End Employee info Modal */}

                {/* Start Employee Social Network Modal */}
                <div className="modal fade" id="employee_social" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">راه های ارتباطی</h5>

                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {this.state.selectedEmployee != null &&
                        <div className="modal-body text-right">
                            <div className="row text-row">
                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fab fa-facebook"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" 
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.facebook : '' }
                                            onChange={ e => this.updateContact('facebook', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fab fa-twitter"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" 
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.twitter : '' }
                                            onChange={ e => this.updateContact('twitter', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fab fa-telegram"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" 
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.telegram : '' }
                                            onChange={ e => this.updateContact('telegram', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fab fa-instagram"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" 
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.instagram : '' }
                                            onChange={ e => this.updateContact('instagram', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fas fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" 
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.email : '' }
                                            onChange={ e => this.updateContact('email', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control"  dir="auto"
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.phone : '' }
                                            onChange={ e => this.updateContact('phone', e.target.value)}/>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="input-group mb-3">
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                <i className="fas fa-mobile"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control" dir="auto"
                                            value={ this.state.selectedEmployee.contact != null ? this.state.selectedEmployee.contact.mobile : '' }
                                            onChange={ e => this.updateContact('mobile', e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary ml-2" data-dismiss="modal"
                                onClick={ e => this.setState({
                                    selectedEmployee: null
                                }) }>انصراف</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.storeContact }>ذخیره</button>
                        </div>
                        </div>
                    </div>
                </div>
                {/* End employee Social Network Modal */}

                {/* Start Edit Employee Modal */}
                <div className="modal fade" id="employee_update" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">ویرایش اطلاعات متخصص</h5>

                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body text-right">
                            {this.state.selectedEmployee != null ?
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group" >
                                        <label htmlFor="lang">زبان</label>
                                        <select className="custom-select" id="lang" value={this.state.selectedEmployee.lang}
                                            onChange={ e => this.setSelectedEmployee('lang', e.target.value)}>
                                            <option value="all">همه</option>
                                            <option value="fa">فارسی</option>
                                            <option value="ar">عربی</option>
                                            <option value="en">انگلیسی</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>نام</label>
                                        <input type="text" className="form-control"
                                            value={this.state.selectedEmployee.name}
                                            onChange={ e => this.setSelectedEmployee('name', e.target.value) }/>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label>تخصص</label>
                                        <input type="text" className="form-control"
                                            value={this.state.selectedEmployee.expert}
                                            onChange={ e => this.setSelectedEmployee('expert', e.target.value )}/>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <label >تصویر</label>
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="avatar"
                                            onChange={ e => this.uploadImage(e, 'update')}/>
                                        <label className="custom-file-label text-left" htmlFor="avatar">انتخاب تصویر</label>
                                        <small>سایز فایل: ۴۵۰*۴۵۰</small>
                                    </div>
                                </div>

                                <div className="col-md-12">
                                    <label>توضیحات</label>
                                    <textarea rows="2" className="form-control"
                                    value={this.state.selectedEmployee.description}
                                    onChange={ e => this.setSelectedEmployee('description', e.target.value)}></textarea>
                                </div>
                            </div>
                            : 
                            null
                            }
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary ml-2" data-dismiss="modal">انصراف</button>
                            <button type="button" className="btn btn-primary" onClick={ this.update }>ذخیره</button>
                        </div>
                        </div>
                    </div>
                </div>
                {/* End Edit Employee Modal */}
            </div>
        );
    }
}

ReactDOM.render(<Employees/>, document.getElementById('employees_container'));
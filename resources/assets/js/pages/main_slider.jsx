import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import Axios from 'axios';
import { notFound, destroyError, storeError, ConfirmDialog } from '../services/alert';
import { lang, api } from '../services/helper';
import findIndex from 'lodash/findIndex';

export default class MainSlider extends Component{
    constructor(props){
        super(props);

        this.state = {
            sliders: [],
            selectedSlide: null,
            newLink: {
                title: '',
                link: ''
            }
        }

        this.destroy = this.destroy.bind(this);
        this.setLink = this.setLink.bind(this);
        this.storeLinks = this.storeLinks.bind(this);
    }

    componentDidMount(){
        Axios.get(api('/pages/main-sliders'))
        .then( response => {
            this.setState({
                sliders: response.data.data
            });
        })
        .catch( error => {
            notFound(error.response.status);
        });
    }

    destroy(id){
        let sliders = this.state.sliders;
        let i = findIndex(sliders,{id: id});
        Axios.delete(api('/pages/main-sliders/'+id))
            .then( response => {
                sliders.splice(i, 1);

                this.setState({
                    sliders: sliders,
                })
            })
            .catch( error => {
                destroyError(error.response.status);
            })
    }

    setModelData(){
        document.getElementById('slide_id').value = this.state.selectedSlide.id; 
        document.getElementById('slide_title').value = this.state.selectedSlide.title; 
        document.getElementById('slide_description').value = this.state.selectedSlide.description;
        document.getElementById('LANG_' + this.state.selectedSlide.lang).selected = true;

    }

    setLink(index, target, value){
        let links = this.state.selectedSlide.links;
        
        links[index] = {
            ...links[index],
            [target]: value
        }

        this.setState({
            selectedSlide: {
                ...this.state.selectedSlide,
                links: links
            }
        })
    }

    storeLinks(){
        let sliders = this.state.sliders;
        let i = findIndex(sliders,{id: this.state.selectedSlide.id});
        sliders[i] = this.state.selectedSlide;

        Axios.patch(api('/pages/main-sliders/'+ this.state.selectedSlide.id +'/links'),{ 
                links: this.state.selectedSlide.links
            })
            .then( response => {
                this.setState({
                    sliders: sliders
                }, () => {
                    $('#slider_link').modal('hide');
                });

            })
            .catch( error => {
                storeError(error);
            });

    }

    render(){
        return(
            <div className=" mt-3">
                <div className="modal fade bd-example-modal-xl" tabIndex="-1" role="dialog" id="slider_link" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">لینک ها</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            {this.state.selectedSlide != null ?

                            <div className="modal-body">
                                <div className="d-block">
                                    <button className="btn btn-sm btn-primary"
                                        onClick={ e => {
                                            this.state.selectedSlide.links.push(this.state.newLink);

                                            this.setState({
                                                selectedSlide:{
                                                    ...this.state.selectedSlide,
                                                }
                                        }) }}>افزودن لینک جدید</button>
                                </div>

                                {this.state.selectedSlide.links.length ?
                                    this.state.selectedSlide.links.map( (link, index) => {
                                return(
                                    <div className="row mt-3" key={index}>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <input type="text" className="form-control" value={link.title}
                                                    placeholder="عنوان"
                                                    onChange={ e => this.setLink(index, 'title', e.target.value) }/>
                                            </div>
                                        </div>

                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <input type="text" className="form-control" value={link.link}
                                                    placeholder="لینک"
                                                    onChange={ e => this.setLink(index, 'link', e.target.value) }/>
                                            </div>
                                        </div>

                                        <div className="col-md-2">
                                            <div className="form-group">
                                                <button className="btn btn-sm btn-danger" onClick={ e => {
                                                    this.state.selectedSlide.links.splice(index, 1)

                                                    this.setState({
                                                        selectedSlide:{
                                                            ...this.state.selectedSlide
                                                        }
                                                    })
                                                }}>
                                                    <i className="fa fa-trash-alt"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                )
                                }) : null}
                            </div>
                            :null}
                            <div className="modal-footer">
                                <button className="btn btn-primary btn-sm" onClick={ this.storeLinks }>ذخیره</button>
                                <button className="btn btn-secondary btn-sm mr-3" data-dismiss="modal">انصراف</button>
                            </div>
                        </div>
                    </div>
                </div>

                {this.state.sliders.length ? 
                    <table className="table table-striped text-right">
                        <thead className="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>عنوان</th>
                                <th>زبان</th>
                                <th>توضیحات</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {this.state.sliders.map( slide => (
                            <tr key={slide.id}>
                                <td>
                                    <img src={slide.image} alt="#"/>
                                </td>

                                <td>
                                    {slide.title}
                                </td>

                                <td>
                                    { lang(slide.lang) }
                                </td>

                                <td>
                                    { slide.description }
                                </td>

                                <td>
                                    <button className="btn btn-info btn-sm ml-2" data-toggle="modal" data-target="#slider_link"
                                        onClick={ e => {
                                            this.setState({
                                                selectedSlide: slide
                                            })
                                        }}>
                                            <i className="fa fa-link"></i>
                                    </button>
                                    <button className="btn btn-warning btn-sm" data-toggle="modal" data-target="#update_slide"
                                        onClick={ e => {
                                            this.setState({selectedSlide: slide},
                                                () => {
                                                    this.setModelData();
                                                });

                                        }}>
                                        <i className="fa fa-edit"></i>
                                    </button>

                                    <button className="btn btn-danger btn-sm mr-2" onClick={ e => ConfirmDialog('آیا از حذف اطمینان دارید؟', ()=> this.destroy(slide.id)) }>
                                        <i className="fa fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                :
                <div className="alert alert-warning text-center">اسلایدی یافت نشد</div>
                }
            </div>
        )
    }
}

ReactDOM.render(<MainSlider/>, document.getElementById('sliders_container'));
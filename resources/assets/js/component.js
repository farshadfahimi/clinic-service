
window.Vue = require('vue');

Vue.component('menu-generator', require('./pages/MenuGenerator.vue').default);
Vue.component('nested-draggable', require('./components/NestedDraggable.vue').default);
Vue.component('checked-request', require('./components/CheckedRequest.vue').default)

new Vue({
    el: '#app',
});
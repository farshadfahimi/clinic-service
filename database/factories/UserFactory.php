<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Users::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});


$factory->define(App\Models\Threads::class,function(Faker $faker){
    return[
       'user_id'=>function(){
          return factory('App\Models\Users')->create()->id;
       },
       'channel_id'=>function(){
          return factory('App\Models\Channels')->create()->id;
       },
       'title'=>$faker->sentence,
       'body'=>$faker->paragraph
    ];
 });
 
 $factory->define(App\Models\Channels::class,function(Faker $faker){
    $name=$faker->name;
 
    return [
       'name'=>$name,
       'slug'=>$name
    ];
 });
 
 $factory->define(App\Models\Replies::class,function(Faker $faker){
    return [
       'approved_by'=>function(){
          return factory('App\Models\Users')->create()->id;
       },
       'thread_id'=>function(){
          return factory('App\Models\Threads')->create()->id;
       },
       'name'  => $faker->name,
       'email' => $faker->email,
       'body'=>$faker->paragraph
    ];
 });

 $factory->define(App\Models\Comments::class, function(Faker $faker){
   return [
      'treatments_id'   => function(){
         return factory('App\Models\Treatment')->create()->id;
      },
      'name'   => $faker->name,
      'before' => 'png',
      'after'  => 'png',
      'description'  => $faker->sentence
   ];
 });
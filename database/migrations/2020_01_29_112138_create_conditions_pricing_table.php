<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionsPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatments_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('treatments_id');
            $table->text('description')->nullable();
            $table->json('prices');
            $table->timestamps();

            $table->foreign('treatments_id')->references('id')->on('treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatments_pricings');
    }
}

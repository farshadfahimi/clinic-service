<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Users;

class ImplementRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = Users::whereIn('id', ['1'])->get();

        $operator = Role::create([
            'name'  =>  'operator'
        ]);

        $content = Role::create([
            'name'  =>  'content-manager'
        ]);

        $admin = Role::create([
            'name'  =>  'clinic-manager'
        ]);

        $condition = Role::create([
            'name'  =>  'conditions-manager'
        ]);

        $treatment = Role::create([
            'name'  =>  'treatments-manager'
        ]);

        foreach($users as $user)
        {
            $user->assignRole([
                $operator->name, 
                $content->name, 
                $admin->name,
                $condition->name,
                $treatment->name,
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

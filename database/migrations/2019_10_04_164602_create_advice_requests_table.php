<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdviceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('gender', ['MS', 'MRS', 'MR', 'MX'])->nullable();
            $table->unsignedInteger('treatments_id');
            $table->enum('lang', ['FA', 'AR', 'EN'])->default('FA');
            $table->string('first_name', 70);
            $table->string('last_name', 70);
            $table->string('phone', 20);
            $table->string('email', 250)->nullable();
            $table->text('description');
            $table->timestamp('checked_at')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('treatments_id')->references('id')->on('treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}

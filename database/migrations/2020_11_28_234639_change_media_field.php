<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Media;

class ChangeMediaField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function(Blueprint $table) {
            $table->enum('type', ['VIDEO', 'IMAGE'])->default('IMAGE');
            $table->string('path')->nullable();
        });

        $files = Media::all();

        foreach($files as $item)
        {
            Media::where('id', $item->id)
                ->update([
                    'path'  =>  $item->file
                ]);
        }

        Schema::table('media', function(Blueprint $table) {
            $table->dropColumn('extension');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

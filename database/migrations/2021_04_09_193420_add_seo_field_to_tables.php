<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('threads', function (Blueprint $table) {
            $table->dropColumn(['keywords', 'description', 'og_title', 'og_description', 'og_locale', 'og_author']);
        });

        Schema::table('conditions', function(Blueprint $table) {
            $table->json('seo')->nullable()->default(null);
        });

        Schema::table('treatments', function(Blueprint $table) {
            $table->json('seo')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('lang', ['EN', 'AR', 'FA'])->default('FA');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('media', function(Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable()->after('id');

            $table->foreign('category_id')->references('id')->on('media_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_categories');
    }
}

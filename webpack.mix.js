const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/assets/js/pages/treatments_pricing.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/admin.js', 'public/assets/js')
   .react('resources/assets/js/pages/channels.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/faq.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/threads.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/tags.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/replies.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/departments.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/treatments.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/media.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/conditions.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/employees.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/create_thread.js', 'public/assets/js/admin')
   .react('resources/assets/js/pages/main_slider.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/services_categories.jsx', 'public/assets/js/admin')
   .react('resources/assets/js/pages/setting.jsx', 'public/assets/js/admin')
   .js('resources/assets/js/component.js', 'public/assets/js/admin')
   .js('resources/assets/js/welcome.js', 'public/assets/js')
   .js('resources/lib/js/bootsnav.js', 'public/assets/js')
   .js('resources/assets/js/app.js', 'public/assets/js')

   .extract()

   .sass('resources/assets/sass/admin.scss', 'public/assets/css')
   .sass('resources/assets/sass/app.scss', 'public/assets/css')
   .sass('resources/assets/sass/rtl.scss', 'public/assets/css')
   .styles([
      'resources/lib/css/bootstrap.min.css',
      'resources/lib/css/animate.css',
      'resources/lib/css/bootsnav.css',
      'resources/lib/css/style.css',
      'resources/lib/css/theme-color.css',
      'resources/lib/css/responsive.css',
   ], 'public/assets/css/all.css')
   .copyDirectory('resources/lib/images', 'public/assets/img')
   .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/assets/fonts')
   .copy('resources/lib/iran_sans', 'public/assets/fonts')
   .copy('resources/lib/iran', 'public/assets/fonts')
   .copyDirectory('resources/assets/img', 'public/assets/img')
   .options({
      processCssUrls: false
   })

if(mix.inProduction())
   mix.version();
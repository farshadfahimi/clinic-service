<?php

Breadcrumbs::for('home', function($trail){
        $trail->push(\Lang::get('message.pages.home'), route('/'));
});

/* Web */
Breadcrumbs::for('services', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.services'), route('services.index'));
});

Breadcrumbs::for('teamus', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.team'), route('team'));
});

Breadcrumbs::for('service', function($trail, $service){
    $trail->parent('services');
    $trail->push($service->name);
});

Breadcrumbs::for('threads', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.posts'), route('threads-list'));
});

// news
Breadcrumbs::for('news', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.news'), route('news.index'));
});

// news page
Breadcrumbs::for('news-show', function($trail, $news){
    $trail->parent('news');
    $trail->push($news->title);
});

Breadcrumbs::for('thread', function($trail, $thread){
    $trail->parent('threads');
    $trail->push($thread->title);
});

Breadcrumbs::for('conditions', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.conditions'), route('conditions.index'));
});

Breadcrumbs::for('condition', function($trail, $condition){
    $trail->parent('conditions');
    if($condition->parent)
        $trail->push($condition->parent->name, route('conditions.index', ['condition' => $condition->parent->id]));
        
    $trail->push($condition->name);
});

/* Treatments */
Breadcrumbs::for('treatments', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.treatments'), route('treatments.index'));
});

/* Treatment */
Breadcrumbs::for('treatment', function($trail, $treatment){
    $trail->parent('treatments');
    if($treatment->parent)
        $trail->push($treatment->parent->name, route('treatments.index', ['treatment' => $treatment->parent->id]));

    $trail->push($treatment->name);
});

/* Employees */
Breadcrumbs::for('employees', function($trail){
    $trail->parent('home');
    $trail->push(\Lang::get('message.pages.employees'), route('teams.index'));
});
// Employee
Breadcrumbs::for('employee', function($trail, $employee){
    $trail->parent('employees');
    $trail->push($employee->name);
});


/* Admin */

Breadcrumbs::for('panels', function($trail){
    $trail->push('داشبورد', route('admin.panel'));
});

/* Channels list */
Breadcrumbs::for('channels', function($trail){
    $trail->parent('panels');
    $trail->push('دسته بندی', route('admin.channels.index'));
});

/* threads of channel */
Breadcrumbs::for('channel', function($trail, $channel){
    $trail->parent('channels');
    $trail->push($channel->name, route('admin.channels.threads.index', $channel->id));
});

/* Create Thread */
Breadcrumbs::for('create-thread', function($trail, $channel){
    $trail->parent('channel', $channel);
    $trail->push('پست جدید', route('admin.channels.threads.create', $channel->id));
});

/* Thread seo */
Breadcrumbs::for('admin-thread-seo', function($trail, $channel, $thread){
    $trail->parent('channel', $channel);
    $trail->push($thread->title);
    $trail->push('سئو', route('admin.channels.threads.seo', ['id' => $channel->id, 'thread' => $thread->id]));
});

/* Edit thread */
Breadcrumbs::for('admin-thread-edit', function($trail, $channel, $thread){
    $trail->parent('channel', $channel);
    $trail->push($thread->title);
    $trail->push('ویرایش', route('admin.channels.threads.seo', ['id' => $channel->id, 'thread' => $thread->id]));
});

/* Thread replies */
Breadcrumbs::for('admin-thread-replies', function($trail, $channel, $thread){
    $trail->parent('channel', $channel);
    $trail->push($thread->title);
    $trail->push('پاسخ کاربران', route('admin.channels.threads.seo', ['id' => $channel->id, 'thread' => $thread->id]));
});

// departments
Breadcrumbs::for('admin-departments', function($trail){
    $trail->parent('panels');
    $trail->push('لیست شعب', route('admin.departments.index'));
});

// Employees
Breadcrumbs::for('admin-employees', function($trail){
    $trail->parent('panels');
    $trail->push('لیست متخصص ها', route('admin.employees.index'));
});

/* admin services categories list */
Breadcrumbs::for('admin-categories-services', function($trail){
    $trail->parent('panels');
    $trail->push('دسته بندی خدمات', route('admin.services-categories.index'));
});

/* admin conditions */
Breadcrumbs::for('admin-conditions-list', function($trail){
    $trail->parent('panels');
    $trail->push('لیست بیماری ها', route('admin.conditions.index'));
});

/* admin conditions */
Breadcrumbs::for('admin-condition', function($trail, $condition){
    $trail->parent('admin-conditions-list');
    $trail->push($condition->name, route('admin.conditions.edit', ['id' => $condition->id]));
});

/* admin conditions Faq */
Breadcrumbs::for('admin-condition-faq', function($trail, $condition){
    $trail->parent('admin-condition', $condition);
    $trail->push('سوالات متداول');
});

/* admin conditions Seo */
Breadcrumbs::for('admin-condition-seo', function($trail, $condition){
    $trail->parent('admin-condition', $condition);
    $trail->push('سئو');
});

/* admin treatments */
Breadcrumbs::for('admin-treatments-list', function($trail){
    $trail->parent('panels');
    $trail->push('راه های درمان', route('admin.treatments.index'));
});

/* admin specific treatment */
Breadcrumbs::for('admin-treatment', function($trail, $treatment){
    $trail->parent('admin-treatments-list');
    $trail->push($treatment->name, route('admin.treatments.edit', ['id' => $treatment->id]));
});

/* admin specific treatment Faq */
Breadcrumbs::for('admin-treatment-faq', function($trail, $treatment){
    $trail->parent('admin-treatment', $treatment);
    $trail->push('سوالات متداول');
});

/* admin specific treatment Seo */
Breadcrumbs::for('admin-treatment-seo', function($trail, $treatment){
    $trail->parent('admin-treatment', $treatment);
    $trail->push('سئو');
});

/* Admin specific treatment comments list */
Breadcrumbs::for('admin-treatment-comment-list', function($trail, $treatment){
    $trail->parent('admin-treatment', $treatment);
    $trail->push('لیست نظرات', route('admin.treatments.comments.index', ['id' => $treatment->id]));
});

/* create comment for treatments */
Breadcrumbs::for('admin-treatment-comment-create', function($trail, $treatment){
    $trail->parent('admin-treatment-comment-list', $treatment);
    $trail->push('نظر جدید');
});

/* Admin settings */
Breadcrumbs::for('admin-settings', function($trail){
    $trail->parent('panels');
    $trail->push('تنظیمات سراسری', route('admin.settings.index'));
});

/* Admin social settings */
Breadcrumbs::for('admin-social', function($trail){
    $trail->parent('admin-settings');
    $trail->push('شبکه های اجتماعی', route('admin.settings.social'));
});

/* Admin social settings */
Breadcrumbs::for('admin-home-slider', function($trail){
    $trail->parent('admin-settings');
    $trail->push('تنظیمات اسلایدر صفحه اصلی', route('admin.settings.home-main-slider'));
});

// Admin menu maker
Breadcrumbs::for('admin-menu-maker', function($trail){
    $trail->parent('admin-settings');
    $trail->push('منو ساز', route('admin.settings.menu'));
});

// global banner
Breadcrumbs::for('admin-banner', function($trail){
    $trail->parent('admin-settings');
    $trail->push('بنر صفحات اصلی', route('admin.settings.banner'));
});

Breadcrumbs::for('admin-about', function($trail){
    $trail->parent('admin-settings');
    $trail->push('صفحه درباره ما', route('admin.settings.about-page'));
});


// users-list
Breadcrumbs::for('admin-users-list', function($trail){
    $trail->parent('panels');
    $trail->push('لیست کاربران', route('admin.users.index'));
});

// users-edit
Breadcrumbs::for('admin-users-edit', function($trail, $user){
    $trail->parent('admin-users-list');
    $trail->push('اطلاعات کاربری', route('admin.users.edit', ['user' => $user->id]));
});

// users create
Breadcrumbs::for('admin-users-create', function($trail){
    $trail->parent('panels');
    $trail->push('افزودن کاربر', route('admin.users.create'));
});

// news create
Breadcrumbs::for('admin-news-create', function($trail){
    $trail->parent('panels');
    $trail->push('خبر جدید', route('admin.news.create'));
});

/* start admin users-opinions */
Breadcrumbs::for('admin-users-opinions', function($trail) {
    $trail->parent('panels');
    $trail->push('نظر کاربران', route('admin.users-opinions.index'));
});

/* admin create users-opinions */
Breadcrumbs::for('admin-users-opinions-create', function($trail) {
    $trail->parent('admin-users-opinions');
    $trail->push('افزودن نظر جدید', route('admin.users-opinions.create'));
});

// news edit page
Breadcrumbs::for('admin-news-edit', function($trail){
    $trail->parent('panels');
    $trail->push('ویرایش خبر', route('admin.news.create'));
});

// start tags page
Breadcrumbs::for('admin-tags-index', function($trail) {
    $trail->parent('panels');
    $trail->push('برچسبها', route('admin.tags.index'));
});
// end tags pages




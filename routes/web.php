<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localize', 'localizationRedirect']], function () use($router) {
    $router->get('/', 'PageController@index')->name('/');
    $router->get('/about', 'PageController@about')->name('about');
    $router->get('/contact-us', 'PageController@contactus')->name('contactus');
    $router->get('/teamus', 'PageController@teams')->name('team');
    $router->get('/search', 'PageController@search')->name('search');

    $router->group(['prefix' => '/services', 'as' => 'services.'], function() use($router) {
        $router->get('/', 'ServiceController@index')->name('index');
        $router->get('/{name}', 'ServiceController@show')->name('show');
    });

    // Posts
    $router->get('/threads', 'ThreadController@index')->name('threads-list');
    $router->get('/threads/{thread}', 'ThreadController@show')->name('thread-show');
    $router->get('/threads/{channel}','ThreadController@index')->name('threads-channel');
    $router->get('/threads/{channel}/{thread}','ThreadController@show')->name('threads-show');
    $router->post('/threads/{thread}/reply', 'ReplyController@store')->name('reply.create');

    // news
    $router->resource('/news', 'NewsController')->only(['index', 'show'])
        ->names([
            'index' =>  'news.index',
            'show'  =>  'news.show'
        ]);

    // Treatments
    $router->group(['prefix' => '/treatments', 'as' => 'treatments.'], function() use($router) {
        $router->get('/', 'TreatmentController@index')->name('index');
        $router->get('/{treatment}', 'TreatmentController@show')->name('show');
        $router->post('/{id}/advice', 'TreatmentController@store_request')->name('advice-request');
    });

    // Conditions
    $router->group(['prefix' => '/conditions', 'as' => 'conditions.'], function() use($router) {
        $router->get('/', 'ConditionController@index')->name('index');
        $router->get('/{condition}', 'ConditionController@show')->name('show');
    });

    // Doctors
    $router->group(['prefix' => '/doctors', 'as' => 'teams.'], function() use($router) {
        $router->get('/', 'TeamController@index')->name('index');
        $router->get('/{name}', 'TeamController@show')->name('show');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/* start admin routes */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:operator|admin'], 'as' => 'admin.'], function() use($router){
    $router->get('/', 'PanelController@panel')->name('panel');

    $router->resource('/users-opinions', 'UsersOpinionController');

    /* Departments */
    $router->group(['prefix' => '/departments', 'as' => 'departments.', 'middleware' => 'role:content-manager'], function() use($router) {
        $router->get('/', 'DepartmentController@index')->name('index');
    });

    /* Services */
    $router->group(['prefix' => 'services-categories', 'as' => 'services-categories.', 'middleware' => 'role:content-manager'], function() use($router) {
        $router->get('/', 'ServiceCategoryController@index')->name('index');
        $router->get('/{id}/edit', 'ServiceCategoryController@edit')->name('edit');
        $router->put('/{id}', 'ServiceCategoryController@update')->name('update');
    });

    /* conditions */
    $router->group(['prefix' => 'conditions', 'as' => 'conditions.', 'middleware' => 'role:conditions-manager'], function() use($router) {
        $router->get('/', 'ConditionController@index')->name('index');
        
        $router->group(['prefix' => '/{id}'], function() use($router){
            $router->put('', 'ConditionController@update')->name('update');
            $router->get('/edit', 'ConditionController@edit')->name('edit');
            $router->get('/faq', 'ConditionController@faq')->name('faq');
            $router->get('/seo', 'ConditionController@seo')->name('seo');
            $router->patch('/seo', 'ConditionController@storeSeo');
        });
    });
    
    /* treatments */
    $router->group(['prefix' => '/treatments', 'as' => 'treatments.', 'middleware' => 'role:treatments-manager'], function() use($router) {
        $router->get('/', 'TreatmentController@index')->name('index');
        $router->get('/treatment-requests', 'TreatmentController@requestList')->name('requests');
        
        $router->group(['prefix' => '/{id}'], function() use($router) {
            $router->put('/', 'TreatmentController@update')->name('update');
            $router->get('/edit', 'TreatmentController@edit')->name('edit');
            $router->get('/faq', 'TreatmentController@faq');
            $router->get('/seo', 'TreatmentController@seo')->name('seo');
            $router->patch('/seo', 'TreatmentController@storeSeo')->name('seo');
            $router->get('/requests', 'TreatmentController@adviseList');

            $router->group(['prefix' => '/comments', 'as' => 'comments.'], function() use($router) {
                $router->get('/', 'TreatmentController@fetchCommentsList')->name('index');
                $router->post('/', 'TreatmentController@storeComment')->name('store');
                $router->post('/{comment_id}/edit', 'TreatmentController@updateComment')->name('update');
                $router->get('/create', 'TreatmentController@createComment')->name('create');
                $router->get('/{comment_id}/edit', 'TreatmentController@editComment')->name('edit');
                $router->delete('{comment_id}', 'TreatmentController@destroyComment')->name('delete');
            });


            $router->get('/pricing', 'TreatmentController@pricing')->name('pricing');
        });
    });

    $router->group(['prefix' => '/news', 'as' => 'news.'], function() use($router) {
        $router->get('/', 'NewsController@index')->name('index');
        $router->get('/create', 'NewsController@create')->name('create');
        $router->post('/', 'NewsController@store')->name('store');
        $router->put('/{id}', 'NewsController@update')->name('update');
        $router->get('/{id}/edit', 'NewsController@edit')->name('edit');
        $router->delete('/{id}', 'NewsController@destroy')->name('delete');
    });

    /* Settings */
    $router->group(['prefix' => 'settings', 'as' => 'settings.', 'middleware' => 'role:clinic-manager'], function() use($router) {
        $router->get('/', 'SettingController@index')->name('index');
        $router->post('/', 'SettingController@store_setting')->name('store-setting');
        $router->get('/social', 'SettingController@social')->name('social');
        $router->post('/social', 'SettingController@store_social')->name('store-social');
        $router->get('/home_slider', 'PageController@home_main_slider')->name('home-main-slider');
        $router->post('/home_slider', 'PageController@store_main_slider')->name('store-main-slider');
        $router->put('/home_slider', 'PageController@update_main_slider')->name('update-main-slider');
        
        $router->get('/welcome', 'PageController@welcome_page')->name('welcome-page');
        
        // menu
        $router->get('/menu', 'PageController@menu')->name('menu');
        
        // banner
        $router->get('/banner', 'PageController@banner')->name('banner');
        $router->post('/banner', 'PageController@storeBanner')->name('store_banner');

        // about page
        $router->get('/about', 'SettingController@about')->name('about-page');
        $router->post('/about', 'SettingController@storeAbout');
    });

    /* tags */
    $router->group(['prefix' => 'tags', 'as' => 'tags.'], function() use($router) {
        $router->get('/', 'TagController@list')->name('index');
    });

    /* Media */
    $router->group(['prefix' => 'media', 'as' => 'media.', 'middleware' => 'role:content-manager'], function() use($router) {
        $router->get('/', 'MediaController@index')->name('index');
        $router->post('/', 'MediaController@store')->name('store');
    });

    /* Employyers */
    $router->group(['prefix' => 'emplyees', 'as' => 'employees.', 'middleware' => 'role:clinic-manager'], function() use($router) {
        $router->get('/', 'EmployeeController@index')->name('index');
    });
    
    // Blog
    $router->group(['prefix' => '/channels', 'as' => 'channels.', 'middleware' => 'role:content-manager'], function() use($router){
        $router->get('/', 'ChannelController@index')->name('index');

        $router->group(['prefix' => '/{id}', 'as' => 'threads.'],function() use($router){
            $router->get('/threads', 'ThreadsController@index')->name('index');
            $router->get('/threads/create', 'ThreadsController@create')->name('create');
            $router->post('/threads', 'ThreadsController@store')->name('store');
            $router->get('/threads/{thread}/seo', 'ThreadsController@seo')->name('seo');
            $router->patch('/threads/{thread}/seo', 'ThreadsController@storeSeo');
            $router->get('/threads/{thread}/edit', 'ThreadsController@edit')->name('edit');
            $router->get('/threads/{thread}/replies', 'ThreadsController@replies')->name('replies');
            $router->put('/threads/{thread}', 'ThreadsController@update')->name('update');
        });
    });

    $router->get('/replies', 'ReplyController@index')->name('replies');
    $router->post('/replies/{reply}', 'ReplyController@store')->name('replies.store');
    $router->delete('/replies/{reply}', 'ReplyController@destroy')->name('replies.delete');

    // Users
    $router->resource('/users', 'UserController')->names([
        'index' => 'users.index',
        'show'  => 'users.show',
        'create'  => 'users.create',
        'edit'  => 'users.edit',
        'udpate'  => 'users.update',
    ])->middleware('role:clinic-manager');

    $router->post('/users/{user}/security', 'UserController@updateSecurity')->name('users.security')->middleware('role:clinic-manager');
    $router->post('/users/{user}/roles', 'UserController@updateRole')->name('users.roles')->middleware('role:clinic-manager');
});
<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Admin'], function () use($router) {
    $router->get('/tags', 'TagController@index');
    /* Departments */
    $router->group(['prefix' => '/departments'], function() use($router){
        $router->put('/', 'DepartmentController@store');
        $router->get('/', 'DepartmentController@get_departments');
        $router->patch('/{id}', 'DepartmentController@update');
        $router->delete('/{id}', 'DepartmentController@destroy');
    });

    /* Services Categories */
    $router->group(['prefix' => '/services-categories'], function () use($router) {
        $router->get('/', 'ServiceCategoryController@index');
        $router->post('/', 'ServiceCategoryController@store');
        $router->delete('/{id}', 'ServiceCategoryController@destroy');
    });

    /* Services */
    $router->group(['prefix' => '/services'], function() use($router){
        $router->get('/', 'ServiceController@get_all_main_services');
        $router->put('/', 'ServiceController@store');
        $router->patch('/{id}', 'ServiceController@update');
        $router->delete('/{id}', 'ServiceController@destroy');

        $router->get('/{id}/prices', 'ServiceController@get_service_prices');
        $router->post('/{id}/prices', 'ServiceController@store_service_price');
        $router->patch('/{id}/prices/{price_id}', 'ServiceController@update_service_price');
        $router->patch('/{id}/prices/{price_id}/is_published', 'ServiceController@update_price_publishment');
    });

    /* Treatments */
    $router->group(['prefix' => '/treatments'], function() use($router){
        $router->get('/', 'TreatmentController@list');
        $router->post('/', 'TreatmentController@store');

        $router->group(['prefix' => '/{id}'], function() use($router) {
            $router->delete('/', 'TreatmentController@destroy');
            $router->patch('/is_published', 'TreatmentController@changeStatus');

            $router->group(['prefix' => '/faqs'], function() use($router) {
                $router->post('/', 'TreatmentController@store_faq');
                $router->get('/', 'TreatmentController@faq_list');
                $router->patch('/sort', 'TreatmentController@changeFaqSort');
                $router->put('/{question_id}', 'TreatmentController@update_faq');
                $router->delete('/{question_id}', 'TreatmentController@destroy_faq');
            });

            $router->group(['prefix' => '/pricing'], function() use($router) {
                $router->get('/', 'TreatmentController@fetchConditionPrice');
                $router->put('/', 'TreatmentController@storeTreatmentPrice');
            });
        });
    });

    $router->group(['prefix' => '/requests'], function() use($router) {
        $router->patch('/{id}/checked', 'TreatmentController@checkedRequest');
    });

    /* Conditions */
    $router->group(['prefix' => '/conditions'], function() use($router){
        $router->get('/', 'ConditionController@list');
        $router->post('/', 'ConditionController@store');
        
        $router->group(['prefix' => '/{id}'], function() use($router) {
            $router->delete('/', 'ConditionController@destroy');
            $router->patch('/is_published', 'ConditionController@changeStatus');

            $router->group(['prefix' => '/faqs'], function() use($router) {
                $router->post('/', 'ConditionController@store_faq');
                $router->get('/', 'ConditionController@faq_list');
                $router->put('/{question_id}', 'ConditionController@update_faq');
                $router->delete('/{question_id}', 'ConditionController@destroy_faq');
            });
        });
    });
    
    /* Employees */
    $router->group(['prefix' => '/employees'], function() use($router) {
        $router->get('/', 'EmployeeController@list');
        $router->post('/', 'EmployeeController@store');
        $router->put('/{id}', 'EmployeeController@update');
        $router->delete('/{id}', 'EmployeeController@destroy');
        $router->patch('/{id}/values', 'EmployeeController@update_values');
        $router->patch('/{id}/contact', 'EmployeeController@update_contact');
        $router->patch('/{id}', 'UserController@update');
    });

    /* Channels and Threads */
    $router->group(['prefix' => '/channels'], function() use($router) {
        $router->get('/', 'ChannelController@list');
        $router->post('/', 'ChannelController@store');
        
        $router->group(['prefix' => '/{id}'], function() use($router) {
            $router->put('/', 'ChannelController@update');
            $router->delete('/', 'ChannelController@destroy');

            $router->group(['prefix' => '/threads'], function() use($router) {
                $router->get('/', 'ThreadsController@index');
                $router->get('/{thread}/replies', 'RepliesController@index');
                $router->delete('/{thread}', 'ThreadsController@destroy');
            });
        });
    });

    /* Replies */
    $router->group(['prefix' => '/replies'], function() use($router) {
        $router->delete('/{reply}', 'RepliesController@destroy');
        $router->patch('/{reply}/publish', 'RepliesController@publish');
    });

    /* Departments */
    $router->group(['prefix' => '/departments'], function() use($router) {
        $router->get('/', 'DepartmentController@index');
        $router->post('/', 'DepartmentController@store');
        $router->put('/{department}', 'DepartmentController@update');
    });

    $router->post('/media-category', 'MediaController@storeCategory');

    $router->group(['prefix' => '/media'], function() use($router) {
        $router->get('/', 'MediaController@index');
        $router->post('/', 'MediaController@store');
        $router->delete('/{id}', 'MediaController@destroy');
    });

    /* Tags */
    $router->group(['prefix' => '/tags'], function() use($router) {
        $router->post('/', 'TagController@store');
    });

    /* Settings */
    $router->get('/settings', 'SettingController@index');
    $router->post('/settings', 'SettingController@store_general');
    $router->get('/settings/menu', 'SettingController@fetchMenu');
    $router->post('/settings/menu', 'SettingController@storeMenu');

    /* Pages */
    $router->get('/pages/main-sliders', 'PageController@home_main_slider');
    $router->put('/pages/main-sliders/{id}', 'PageController@update_main_slider');
    $router->patch('/pages/main-sliders/{id}/links', 'PageController@store_main_slider_link');
    $router->delete('/pages/main-sliders/{id}', 'PageController@destroy_main_slider');

    $router->post('/content/images', 'MediaController@storeContentImage');
});

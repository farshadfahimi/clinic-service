<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Services extends Model
{
    public $timestamps = false;

    protected $fillable= [
        'id',
        'lang',
        'name',
        'icon',
        'description',
        'body'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($model){
            Storage::delete(Storage::disk('public')->path('/icons/'). $model->id.".png");
        });
    }

    public function getIconAttribute(){
        return Storage::disk('public')->url('/icons/'.$this->id.".png");
    }
}

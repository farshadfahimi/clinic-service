<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Employees extends Model
{
    protected $fillable = [
        'lang',
        'name',
        'expert',
        'description',
        'values',
        'contact'
    ];

    protected static function boot(){
        parent::boot();

        static::deleting(function($employee){
            Storage::disk('public')->delete(['employees/'.$employee->id.'.png', 'employees/'.$employee->id.'_thumb.png']);
        });
    }

    public function getAvatarAttribute(){
        try{
            return Storage::disk('public')->url('employees/'.$this->attributes['id'].'.png');
        }catch(\Exception $e){
            return null;
        }
    }

    public function getThumbAttribute(){
        try{
            return Storage::disk('public')->url('employees/'.$this->attributes['id'].'_thumb.png');
        }catch(\Exception $e){
            return null;
        }
    }

    public function getValuesAttribute(){
        if(!isset($this->attributes['values']))
            return null;
        return json_decode($this->attributes['values']);
    }

    public function getContactAttribute(){
        if(!isset($this->attributes['contact']))
            return null;
        return json_decode($this->attributes['contact']);
    }
}

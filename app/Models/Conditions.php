<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Conditions extends Model
{
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'lang',
        'body',
        'cover',
        'poster',
        'is_pubished'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($condition){
            Storage::disk('public')->delete(['conditions_cover/'.$condition->id.".{$condition->getOriginal('cover')}", 'conditions_poster/'.$condition->id.".{$condition->getOriginal('poster')}"]);
        });
    }


    public function variations(){
        return $this->hasMany(self::class, 'id', 'parent_id')
            ->orWhere('parent_id', $this->parent_id)->published();
    }

    public function questions(){
        return $this->morphToMany(Questions::class, 'questionables');
    }

    
    public function childs(){
        return $this->hasMany(Conditions::class, 'parent_id');
    }

    public function parent(){
        return $this->hasOne(Conditions::class, 'id', 'parent_id');
    }

    public function getCoverAttribute(){
        try{
            if($this->attributes['cover'])
                return Storage::disk('public')->url('conditions_cover/'.$this->attributes['id'].'.'.$this->attributes['cover']);
            return null;
        }catch(\Exception $e){
            return null;
        }
    }

    public function getPosterAttribute(){
        try{
            if($this->attributes['poster'])
                return Storage::disk('public')->url('conditions_poster/'.$this->attributes['id'].'.'.$this->attributes['poster']);
            return null;
        }catch(\Exception $e){
            return null;
        }
    }

    public function getSeoAttribute()
    {
        return json_decode($this->attributes['seo']);
    }

    public function scopePublished($query){
        return $query->where('is_published', 1);
    }
}

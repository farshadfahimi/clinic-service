<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Settings extends Model
{
    protected $fillable = [
        'name',
        'lang',
        'values'
    ];

    // protected $primaryKey = 'name';

    public function getValuesAttribute(){
        return json_decode($this->attributes['values']) == null ? $this->attributes['values'] : json_decode($this->attributes['values']);
    }

    public function getPostsBannerAttribute()
    {
        return Storage::disk('public')->url('irsa/posts.jpg');
    }

    public function getconditionsBannerAttribute()
    {
        return Storage::disk('public')->url('irsa/conditions.jpg');
    }

    public function getTreatmentsBannerAttribute()
    {
        return Storage::disk('public')->url('irsa/treatments.jpg');
    }

    public function getTeamBannerAttribute()
    {
        return Storage::disk('public')->url('irsa/team.jpg');
    }

    public function getServicesBannerAttribute()
    {
        return Storage::disk('public')->url('irsa/services.jpg');
    }
}

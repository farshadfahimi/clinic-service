<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Comments extends Model
{
    protected $fillable = [
        'treatments_id',
        'before',
        'after',
        'name',
        'description'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($model){
            Storage::disk('comments')->deleteDirectory($model->id);
        });
    }

    public function treatment(){
        return $this->belongsTo(Comments::class, 'treatments_id', 'id');
    }

    public function getBeforeImageAttribute(){
        return Storage::disk('comments')->url($this->id.'/before.'.$this->before);
    }

    public function getAfterImageAttribute(){
        return Storage::disk('comments')->url($this->id.'/after.'.$this->after);
    }
}

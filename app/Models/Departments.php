<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use \Spatie\Tags\HasTags;

class Departments extends Model
{
    use HasTags;
    
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($department){
            Storage::disk('public')->delete('departments/'.$department->id.'.png');
        });
    }

    protected $fillable= [
        'name',
        'phone',
        'address',
        'lang',
        'tags'
    ];

    public function getCoverAttribute(){
        try{
            return  Storage::disk('public')->url('departments/'.$this->attributes['id'].'.png');
        }catch(\Exception $e){
            return null;
        }
    }
}

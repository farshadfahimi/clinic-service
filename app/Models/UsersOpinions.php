<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersOpinions extends Model
{
    protected $fillable = [
        'lang',
        'name',
        'body'
    ];

    public function getLanguageAttribute() {
        switch($this->lang) {
            case 'FA':
                return 'فارسی';
            break;
            case 'EN':
                return 'انگلیسی';
            break;
            case 'AR':
                return 'عربی';
            break;
        }
    }
}

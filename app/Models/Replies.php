<?php

   namespace App\Models;

   use Illuminate\Database\Eloquent\Model;

   class Replies extends Model
   {
      protected $fillable=[
         'parent_id',
         'thread_id',
         'approved_by',
         'name',
         'email',
         'body',
         'is_published'
      ];

      protected $with = ['owner', 'parent'];

      protected $appends=['favoritesCount','isFavorited'];

       protected static function boot()
       {
           parent::boot();

           static::created(function($reply){
                $reply->thread->increment('replies_count');
           });

           static::deleted(function($reply){
               $reply->thread->decrement('replies_count');
           });
       }


       public function owner()
      {
         return $this->belongsTo(Users::class,'user_id');
      }

      public function thread()
      {
         return $this->belongsTo(Threads::class);
      }

      public function approvedBy(){
         return $this->belongsTo(Users::class, 'approved_by');
      }

      public function path()
      {
         return $this->thread->path()."#reply-{$this->id}";
      }

      public function parent(){
         return $this->belongsTo(self::class, 'parent_id');
      }
   }

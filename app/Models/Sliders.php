<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Sliders extends Model
{
    protected $fillable = [
        'title',
        'description',
        'lang',
        'links',
        'image'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($slider){
            Storage::disk('public')->delete('sliders/'.$slider->id.'.'.$slider->getOriginal('image'));
        });
    }

    public function getImageAttribute(){
        try{
            return Storage::disk('public')->url('sliders/'.$this->attributes['id'].'.'.$this->attributes['image']);
        }catch(\Exceptin $e){
            return null;
        }
    }

    public function getMobileImageAttribute(){
        try{
            return Storage::disk('public')->url('sliders/'.$this->attributes['id'].'_mobile.'.$this->attributes['image']);
        }catch(\Exceptin $e){
            return null;
        }
    }

    public function getLinksAttribute(){
        return json_decode($this->attributes['links']);
    }
}

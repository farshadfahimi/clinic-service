<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentsPricing extends Model
{
    protected $fillable = [
        'treatments_id',
        'description',
        'prices'
    ];

    public function getPricesAttribute(){
        return json_decode($this->getOriginal('prices'), true);
    }
}

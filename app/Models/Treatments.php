<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Models\Requests as AdviceRequests;

class Treatments extends Model
{
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'lang',
        'body',
        'cover',
        'poster',
        'is_published'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($treatment){
            Storage::disk('public')->delete(['treatments_cover/'.$treatment->id.".{$treatment->getOriginal('cover')}", 'treatments_poster/'.$treatment->id.".{$treatment->getOriginal('poster')}"]);
        });
    }

    public function getSeoAttribute()
    {
        return json_decode($this->attributes['seo']);
    }

    public function variations(){
        return $this->hasMany(self::class, 'id', 'parent_id')
        ->where(function($query) {
            $query->where('parent_id', $this->parent_id == 0 ? $this->id : $this->parent_id);
        }, null, null, 'or')
        ->published();
    }
    
    public function childs(){
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function getCoverAttribute(){
        try{
            return Storage::disk('public')->url('treatments_cover/'.$this->attributes['id'].'.'.$this->attributes['cover']);
        }catch(\Exception $e){
            return null;
        }
    }

    public function getPosterAttribute(){
        try{
            return Storage::disk('public')->url('treatments_poster/'.$this->attributes['id'].'.'.$this->attributes['poster']);
        }catch(\Exception $e){
            return null;
        }
    }

    public function questions(){
        return $this->morphToMany(Questions::class, 'questionables');
    }

    public function requests(){
        return $this->hasMany(AdviceRequests::class);
    }

    public function pricing(){
        return $this->hasOne(TreatmentsPricing::class, 'treatments_id');
    }

    public function parent(){
        return $this->hasOne(Treatments::class, 'id', 'parent_id');
    }

    public function comments(){
        return $this->hasMany(Comments::class, 'treatments_id');
    }

    public function scopePublished($query){
        return $query->where('is_published', 1);
    }
}

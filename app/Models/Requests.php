<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    protected $fillable = [
        'gender',
        'treatments_id',
        'lang',
        'first_name',
        'last_name',
        'phone',
        'email',
        'description',
        'checked_at',
        'country',
        'city'
    ];

    public function treatment() {
        return $this->belongsTo(Treatments::class, 'treatments_id');
    }
}

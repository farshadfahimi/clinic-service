<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use \Spatie\Tags\HasTags;
use App\Models\MediaCategory;

class Media extends Model
{
    use HasTags;
    
    protected $fillable = [
        'extension',
        'title',
        'path',
        'type',
        'category_id'
    ];

    protected $appends = [
        'file'
    ];

    protected static function boot() {
        parent::boot();

        static::deleted(function($model) {
            if($model->type == "IMAGE")
                Storage::disk('public')->delete('media/'.$model->id.'.jpg');
        });
    }

    public function scopeImage($query) {
        return $query->where('type', 'IMAGE');
    }

    public function getFileAttribute(){
        return Storage::disk('public')->url('/media/'.$this->id.'.jpg');

    }

    public function category() {
        return $this->belongsTo(MediaCategory::class, 'category_id');
    }
}

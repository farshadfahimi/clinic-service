<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model {

    protected $fillable = [
        'user_id',
        'replies_count',
        'title',
        'body',
        'cover',
        'poster'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($news) {
            Storage::disk('public')->delete(['news_cover/'.$news->id.".{$news->getOriginal('cover')}", 'news_poster/'.$news->id.".{$news->getOriginal('poster')}"]);
        });
    }

    public function getPathAttribute()
    {
        return "/".strtolower(\LaravelLocalization::getCurrentLocale())."/news/{$this->id}";
    }

    public function creator()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function getCoverAttribute(){
        try{
            return Storage::disk('public')->url('news_cover/'.$this->attributes['id'].'.'.$this->attributes['cover']);
        }catch(\Exception $e){
            return null;
        }
    }

    public function getPosterAttribute(){
        try{
            return Storage::disk('public')->url('news_poster/'.$this->attributes['id'].'.'.$this->attributes['poster']);
        }catch(\Exception $e){
            return null;
        }
    }
}

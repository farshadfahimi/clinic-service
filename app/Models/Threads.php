<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Tags\HasTags;

class Threads extends Model 
{
    use HasTags;

    protected $fillable = [
        'user_id',
        'channel_id',
        'replies_count',
        'title',
        'body',
        'cover',
        'poster'
    ];
    
    protected $with = ['channel'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($thread) {
            Storage::disk('public')->delete(['posts_cover/'.$thread->id.".{$thread->getOriginal('cover')}", 'posts_poster/'.$thread->id.".{$thread->getOriginal('poster')}"]);
            $thread->replies->each->delete();
        });
    }

    public function getPathAttribute()
    {
        return "/".strtolower(\LaravelLocalization::getCurrentLocale())."/threads/{$this->channel->id}/{$this->id}";
    }

    public function getSeoAttribute()
    {
        return json_decode($this->attributes['seo']);
    }

    public function replies()
    {
        return $this->hasMany(Replies::class, 'thread_id');
    }

    public function creator()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

    /**
     * @param $reply
     * @return Model
     */
    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        return $reply;
    }

    public function channel()
    {
        return $this->belongsTo(Channels::class);
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function getReplyCountAttributes()
    {
        return $this->replies()->count();
    }

    public function getCoverAttribute(){
        try{
            return Storage::disk('public')->url('posts_cover/'.$this->attributes['id'].'.'.$this->attributes['cover']);
        }catch(\Exception $e){
            return null;
        }
    }

    public function getPosterAttribute(){
        try{
            return Storage::disk('public')->url('posts_poster/'.$this->attributes['id'].'.'.$this->attributes['poster']);
        }catch(\Exception $e){
            return null;
        }
    }
}

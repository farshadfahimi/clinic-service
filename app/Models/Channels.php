<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channels extends Model
{
   protected $fillable = [
      'name',
      'slug',
      'lang'
   ];

   public function getRouteKeyName()
   {
      return 'slug';
   }

   public function threads()
   {
      return $this->hasMany(Threads::class);
   }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Settings;
use App\Models\Treatments;
use App\Models\Conditions;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts/app', function($view){
            $treatment = Settings::where('name', 'menu-treatments')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->first();

            $condition = Settings::where('name', 'menu-conditions')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->first();

            $channels = Settings::where('name', 'menu-channels')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->first();
            
            $setting = Settings::where('name', 'general')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->first();

            $social = Settings::where('name', 'social')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->first();

            $view->with([
                'siteInfo'  =>  $setting,
                'social'    =>  $social,
                'menuTreatment'=>  $treatment,
                'menuCondition'=>  $condition,
                'menuChannel'  =>  $channels
            ]);            
        });
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Farshad
 * Date: 6/27/2018
 * Time: 13:58
 */

namespace App\Filters;

use Illuminate\Http\Request;
use App\Models\Users;
use Spatie\Tags\Tag;

class ThreadFilters extends Filters {

    protected $filters = ['by', 'popular', 'unanswered', 'tag'];

    /**
     * Filter query by  a given username
     * @param $username
     *
     * @return mixed
     */
    protected function by($username)
    {
        $user = Users::where('name', $username)->firstorfail();

        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Filter the query according to most popular threads
     *
     * @return mixed
     */
    protected function popular()
    {
        $this->builder->getQuery()->orders = [];

        return $this->builder->orderBy('replies_count', 'desc');
    }

    public function unanswered()
    {
        return $this->builder->where('replies_count',0);
    }

    protected function tag($item)
    {
        $tags = Tag::where('slug->fa', $item)->pluck('id')->toArray();

        return $this->builder->join('taggables', 'taggables.taggable_id', '=', 'threads.id')->whereIn('tag_id', $tags);
    }
}
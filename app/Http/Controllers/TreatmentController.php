<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Models\Treatments;
use App\Models\Settings;

class TreatmentController extends Controller
{
    public function index(Request $request){
        $query = Treatments::where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
            ->orderBy('created_at', 'desc')
            ->published();

        $treatment = null;

        if($request->treatment) {
            $query->where('parent_id', $request->treatment);
            $treatment = Treatments::find($request->treatment);
        }

        $treatments = $query->paginate(20);

        SEOMeta::setTitle(\Lang::get('message.pages.treatments'));
        
        $setting = new Settings;

        return view('treatments.index', compact('treatments', 'treatment', 'setting'));
    }

    public function show($id){
        $treatment = Treatments::where('id', $id)
        ->published()
        ->with('questions', 'childs', 'pricing', 'comments', 'parent')->first();
        
        if(!$treatment)
            abort(404);

        $questions = $treatment->questions->sortBy('sort');
        
        if($treatment->seo)
            SEOMeta::setTitle($treatment->seo->title)->setDescription($treatment->seo->description)->setKeywords($treatment->seo->keywords);
        else
            SEOMeta::setTitle($treatment->name);
        return view('treatments.show', compact('treatment', 'questions'));
    }

    public function store_request(Request $request, $id){
        $this->validate($request, [
            'gender'        =>  'nullable|in:mr,ms,mrs,mx',
            'first_name'    =>  'required|string',
            'last_name'     =>  'required|string',
            'phone'         =>  'required',
            'email'         =>  'nullable|email',
            'description'   =>  'required|min:20',
            'country'       =>  'required',
            'city'          =>  'required'
        ]);

        if($treatment = Treatments::find($id)){
            $treatment->requests()->create([
                'gender'    =>  $request->gender,
                'lang'          =>  strtoupper(\LaravelLocalization::getCurrentLocale()),
                'first_name'      =>  $request->first_name,
                'last_name'      =>  $request->last_name,
                'phone'     =>  $request->phone,
                'email'     =>  $request->email,
                'description'   =>  $request->description,
                'country'       =>  $request->country,
                'city'          =>  $request->city
            ]);

            return back();
        }else{
            abort(404);
        }
    }

}

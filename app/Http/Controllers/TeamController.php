<?php

namespace App\Http\Controllers;


use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Models\Employees;

class TeamController extends Controller
{
    public function index(){
        $employees = Employees::where('lang', strtolower(strtoupper(\LaravelLocalization::getCurrentLocale())))
            ->get();

        SEOMeta::setTitle(\Lang::get('message.pages.employees'));

        return view('teams.index', compact('employees'));
    }

    public function show($name){
        $name = urldecode($name);

        $employee = Employees::where('name', $name)
            ->where('lang', strtolower(strtoupper(\LaravelLocalization::getCurrentLocale())))
            ->first();

            
        if(!$employee)
            abort(404);

        SEOMeta::setTitle($employee->name);

        return view('teams.show', compact('employee'));
    }
}

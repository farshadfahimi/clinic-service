<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Lang;
use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Threads;
use App\Models\Conditions;
use App\Models\Treatments;
use App\Models\Sliders;
use App\Models\Employees;
use App\Models\Departments;
use App\Models\Settings;
use App\Models\Media;
use App\Models\News;
use App\Models\UsersOpinions;

class PageController extends Controller
{
    public function index(){
        $lang = \LaravelLocalization::getCurrentLocale();

        $servicesCategories = Services::where('lang', strtoupper($lang))->get();
        
        $threads = Threads::select('threads.*')->orderBy('id', 'desc')
            ->join('channels', 'threads.channel_id', '=', 'channels.id')
            ->where('channels.lang', strtoupper($lang))
            ->limit(4)->get();

        $slides = Sliders::where('lang', $lang)->get();

        $news = News::where('lang', $lang)
            ->orderBy('id', 'desc')
            ->limit(4)->get();

        $teams = Employees::where(function($q) use($lang) {
            $q->where('lang', $lang)
                ->orWhereNull('lang');
        })->get();

        $departments = Departments::where('lang', $lang)->get();

        $gallery = Media::orderBy('id', 'desc')->limit(10)->get();

        $opinions = UsersOpinions::where('lang', $lang)->get();

        SEOMeta::setTitle(Lang::get('message.title'));
        SEOMeta::setDescription(Lang::get('message.description'));

        return view('welcome', compact('threads', 'servicesCategories', 'slides', 'teams', 'departments', 'gallery', 'news', 'opinions'));
    }

    public function about(){
        $page = 'about-'.\LaravelLocalization::getCurrentLocale();

        $about = Settings::where('lang', \LaravelLocalization::getCurrentLocale())
            ->where('name', $page)
            ->first();

        return view('pages.about', compact('about'));
    }

    public function teams(){
        $teams = Employees::where('lang', \LaravelLocalization::getCurrentLocale())->orderBy('name')->get();
        $setting = new Settings;

        return view('pages.team', compact('teams', 'setting'));
    }

    public function contactus()
    {
        $setting = Settings::where('name', 'general')
                ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
                ->first();
        return view('pages.contactus', compact('setting'));
    }

    public function search(Request $request)
    {
        $threads = \DB::table('threads')->selectRaw("id, title as name, body, 'threads' as data_type, channel_id as channel")->where('title', 'like', "%$request->q%");

        $conditions = \DB::table('conditions')->selectRaw("id, name, body, 'conditions' as data_type, null as channel")->where('name', 'like', "%$request->q%");
        $data = \DB::table('treatments')->selectRaw("id, name, body, 'treatments' as data_type, null as channel")->where('name', 'like', "%$request->q%")
            ->union($conditions)
            ->union($threads)
            ->paginate(25);
            
        return view('pages.search', compact('data'));
    }
}

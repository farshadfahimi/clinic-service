<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Conditions;
use App\Models\Questions;
use App\Http\Controllers\Controller;
use App\Http\Resources\ConditionsResource;
use App\Http\Resources\QuestionsResource;

class ConditionController extends Controller
{
    public function index()
    {
        return view('admin.conditions.index');
    }

    public function list(Request $request)
    {
        $query = Conditions::orderBy('lang', 'desc')->orderBy('id', 'desc');

        if($request->has('lang'))
            $query->where('lang', strtoupper($request->lang));
        
        return ConditionsResource::collection($query->get());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'parent_id' =>  'sometimes|exists:treatments,id',
            'lang'  =>  'required|in:fa,en,ar',
            'is_published'  =>  'required|boolean'
        ]);

        $condition = Conditions::create([
            'name'      =>  $request->name,
            'parent_id' =>  $request->has('parent_id') ? $request->parent_id : 0,
            'slug'      =>  preg_replace('/(\s+)/', '-', $request->name),
            'lang'      =>  strtoupper($request->lang),
            'is_published'  =>  $request->is_published
        ]);

        return new ConditionsResource($condition);
    }

    public function edit($id){
        if($condition = Conditions::find($id)){
            $conditions = Conditions::select('id', 'name', 'lang')
                ->where('parent_id', 0)
                ->where('lang', strtoupper($condition->lang))
                ->where('id', '!=', $id)
                ->get();


            return view('admin.conditions.edit', compact('condition', 'conditions'));
        }
        else
            abort(404);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name'  =>  'required',
            'parent_id' =>  'nullable',
            'body'  =>  'required'
        ]);

        if($condition = Conditions::find($id)){
            $condition->name = $request->name;
            $condition->slug = preg_replace('/(\s+)/', '-', $request->name);
            $condition->parent_id = $request->parent_id;
            $condition->body = $request->body;
            $condition->reason_title = $request->reason_title;
            $condition->reason_description = $request->reason_description;
            $condition->signs_title = $request->signs_title;
            $condition->signs_description = $request->signs_description;
            $condition->approach_title = $request->approach_title;
            $condition->approach_description = $request->approach_description;

            if($request->hasFile('cover')){
                $condition->cover = $request->file('cover')->extension();
                $request->file('cover')->storeAs('conditions_cover', $condition->id.'.'.$request->file('cover')->extension(), 'public');
            }
    
            if($request->hasFile('page_cover')){
                $condition->poster = $request->file('page_cover')->extension();
                $request->file('page_cover')->storeAs('conditions_poster', $condition->id.'.'.$request->file('page_cover')->extension(), 'public');
            }

            $condition->save();
            return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
        }

        abort(404);
    }

    public function changeStatus(Request $request, $id){
        if($condition = Conditions::find($id)){
            $condition->is_published = $request->is_published;
            $condition->save();
        }else
            abort(404);
    }

    public function destroy($id){
        Conditions::where('id', $id)->delete();
    }

    public function seo($id)
    {
        $condition = Conditions::where('id', $id)->first();

        return view('admin.conditions.seo', compact('condition'));
    }

    public function storeSeo(Request $request, $id)
    {
        Conditions::where('id', $id)->update([
            'seo' => json_encode([
                'title' => $request->title,
                'description' => $request->description,
                'keywords'  =>  $request->keywords
            ])
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function faq($id){
        $condition = Conditions::find($id);
        
        return view('admin.conditions.faq', compact('condition'));
    }

    public function faq_list($id){
        $condition = Conditions::with('questions')->find($id);

        return QuestionsResource::collection($condition->questions);
    }

    public function store_faq(Request $request, $id){
        $this->validate($request, [
            'title' =>  'required',
            'description'   =>  'required'
        ]);

        if($condition = Conditions::find($id)){
            $question = $condition->questions()->create([
                'title' => $request->title,
                'description'   =>  $request->description
            ]);
            
            return new QuestionsResource($question);
        }else{
            abort(404);
        }

    }

    public function update_faq(Request $request, $id, $question_id){
        $this->validate($request, [
            'title' =>  'required',
            'description'   =>  'required'
        ]);

        if($condition = Conditions::find($id)){
            $condition->questions()->where('questions_id', $question_id)->update([
                'title' => $request->title,
                'description'   =>  $request->description
            ]);
        }else{
            abort(404);
        }
    }

    public function destroy_faq($id, $question_id){
        if($condition = Conditions::find($id)){
            $condition->questions()->detach($question_id);

            Questions::where('id', $question_id)->delete();
        }else{
            abort(404);
        }
    }
}

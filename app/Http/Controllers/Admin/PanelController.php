<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Channels;
use App\Models\Comments;
use App\Models\Conditions;
use App\Models\Treatments;
use App\Models\Threads;
use App\Models\Users;
use App\Models\News;
use App\Models\Replies;
use App\Models\Requests;

class PanelController extends Controller
{
    public function panel(){
        $channels = Channels::count();
        $comments = Comments::count();
        $conditions = Conditions::count();
        $treatments = Treatments::count();
        $threads = Threads::count();
        $users = Users::count();
        $news = News::count();
        $replies = Replies::count();
        $requests = Requests::count();

        return view('admin.panel', compact('channels', 'comments', 'conditions', 'treatments', 'threads', 'users', 'news', 'replies', 'requests'));
    }
}

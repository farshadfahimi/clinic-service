<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Spatie\Tags\Tag as Tags;
use App\Http\Resources\TagsResource;

class TagController extends Controller
{
    public function index(Request $request){
        if($lang = $request->get('lang'))
            app()->setLocale($lang);

        $tags = Tags::ordered()->get();
        
        return TagsResource::collection($tags);
    }

    public function list()
    {
        return view('admin.tags.index');
    }

    /**
     * Store the new resource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tag'    =>  'required|string',
            'lang'   =>  'required'   
        ]);

        $tag = Tags::findOrCreate($request->tag, null, $request->lang);

        return new TagsResource($tag);
    }
}

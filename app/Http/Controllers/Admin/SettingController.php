<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Settings;
use App\Http\Controllers\Controller;
use App\Http\Resources\GeneralSettingResource;
use App\Http\Resources\MenuResource;

class SettingController extends Controller
{
    public function index(){
        if(request()->wantsJson()){
            $settings = Settings::where('name', 'general')->get();

            return GeneralSettingResource::collection($settings);
        }
        return view('admin.settings.index');
    }

    public function store_general(Request $request){
        $this->validate($request, [
            'lang'  =>  'required|in:fa,en,ar'
        ]);
        
        Settings::updateOrCreate(
            ['name' => 'general', 'lang' => strtoupper($request->lang)],
            [
                'values'    =>  json_encode([
                    'title'         =>  $request->title,
                    'keywords'      =>  $request->keywords,
                    'description'   =>  $request->description,
                    'description'   =>  $request->description,
                    'email'         =>  $request->email,
                    'phone'         =>  $request->phone,
                    'fax'           =>  $request->fax,
                    'address'       =>  $request->address,
                    'desc'          =>  $request->desc,
                    'companyPhone'  =>  $request->companyPhone
                ], JSON_UNESCAPED_UNICODE)
            ]
        );
    }

    public function social(){
        $socialFa = Settings::where('name', 'social')
            ->where('lang', 'FA')->first();

        $socialAr = Settings::where('name', 'social')
            ->where('lang', 'AR')->first();

        $socialEn = Settings::where('name', 'social')
            ->where('lang', 'EN')->first();

        return view('admin.settings.social', compact('socialFa', 'socialAr', 'socialEn'));
    }

    public function store_social(Request $request){
        Settings::updateOrCreate(
            ['name' =>  'social', 'lang' => $request->lang],
            ['values' => json_encode([
                'instagram' =>  $request->instagram,
                'facebook'  =>  $request->facebook,
                'twitter'   =>  $request->twitter,
                'youtube'   =>  $request->youtube,
                'whatsapp'  =>  $request->whatsapp,
                'aparat' =>  $request->aparat,
                'telegram'  =>  $request->telegram,
            ], JSON_UNESCAPED_UNICODE)]
        );

        return back();
    }

    public function fetchMenu(Request $request){
        $this->validate($request, [
            'lang'  =>  'required',
            'type'  =>  'required'
        ]);

        $setting = Settings::where('name', 'menu-'.$request->type)
            ->where('lang', $request->lang)->first();

        return new MenuResource($setting);
    }

    public function storeMenu(Request $request){
        $this->validate($request, [
        'lang'  =>  'required|in:fa,en,ar',
        'type'  =>  'required|in:conditions,treatments,channels',
        'menu'  =>  'nullable|array'
        ]);

        Settings::updateOrCreate([
            'lang'  =>  strtoupper($request->lang),
            'name'  =>  'menu-'.$request->type,
        ], ['values' => $request->menu == null ? null : json_encode($request->menu, JSON_UNESCAPED_UNICODE)]);
    }

    /**
     * Return the about page
     */
    public function about() {
        $fa = Settings::where('name', 'about-fa')->first();
        $en = Settings::where('name', 'about-en')->first();
        $ar = Settings::where('name', 'about-ar')->first();

        return view('admin.settings.about', compact('fa', 'ar', 'en'));
    }

    /**
     * Store the about page data
     */
    public function storeAbout(Request $request) {
        Settings::updateOrCreate([
            'lang'  =>  'FA',
            'name'  =>  'about-fa'
        ],[
            'values'    =>  $request->fa
        ]);

        Settings::updateOrCreate([
            'lang'  =>  'EN',
            'name'  =>  'about-en'
        ],[
            'values'    =>  $request->en
        ]);

        Settings::updateOrCreate([
            'lang'  =>  'AR',
            'name'  =>  'about-ar'
        ],[
            'values'    =>  $request->ar
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Channels;
use App\Http\Resources\ChannelsResource;
use App\Http\Controllers\Controller;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        return view('admin.channels.index');
    }

    public function list(Request $request){
        $query = Channels::orderBy('id', 'desc');

        if($request->lang)
            $query->where('lang', strtoupper($request->lang));
        
        $channels = $query->get();

        return ChannelsResource::collection($channels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'lang'  =>  'required|in:fa,en,ar'
        ]);

        $channel = Channels::create([
            'name'  =>  $request->name,
            'slug'  =>  preg_replace('/\s/', '-', $request->name),
            'lang'  =>  strtoupper($request->lang)
        ]);

        return new ChannelsResource($channel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'lang'  =>  'required|in:fa,en,ar'
        ]);

        Channels::where('id', $id)->update([
            'name'  =>  $request->name,
            'slug'  =>  preg_replace('/\s/', '-', $request->name),
            'lang'  =>  strtolower($request->lang)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Channels::where('id', $id)
            ->delete();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Http\Resources\MediaResource;
use App\Http\Resources\MediaCategoryResources;
use App\Models\MediaCategory;
use App\Models\Media;

class MediaController extends Controller
{
    public function index(){
        if(request()->wantsJson()) {
            $media = Media::orderBy('id', 'desc')->with('category')->get();
            
            return MediaResource::collection($media);
        }

        return view('admin.media.index');
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'nullable',
            'tags'  => 'nullable|array'
        ]);

        try {
            DB::beginTransaction();

            $media = Media::create([
                'title' =>  $request->title,
                'type'  =>  $request->type,
                'path'  =>  $request->path,
                'category_id'   =>  $request->category_id
            ]);

            $media->syncTags(collect($request->tags)->pluck('value')->toArray());

            if($request->type == "IMAGE") {
                Image::make($request->file)->save(Storage::disk('public')->path('media/'.$media->id.'.jpg'));

                $media->path = Storage::disk('public')->url('media/'.$media->id.'.jpg');
                $media->save();
            }

            DB::commit();

            return new MediaResource($media);
        }catch(Exception $e) {
            DB::rollback();
            \Log::warning($e->getMessage());

            return response()->status(500)->json([
                'status'    =>  500,
                'error' =>  $e->getMessage()
            ]);
        }
    }

    public function storeContentImage(Request $request){
        $path = $request->file('file')->store(date('y-m'), 'content');

        return response(['location' => Storage::disk('content')->url($path)]);
    }

    public function destroy($id) {
        Media::where('id', $id)->delete();
    }

    public function storeCategory(Request $request) {
        $categroy = MediaCategory::create([
            'lang' => $request->lang,
            'name'  =>  $request->name
        ]);

        return new MediaCategoryResources($categroy);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Replies;
use App\Http\Controllers\Controller;

class ReplyController extends Controller
{
    public function index()
    {
        $replies = Replies::orderBy('id', 'desc')
            ->where('parent_id', 0)->with('thread', 'parent')->paginate(15);

        return view('admin.channels.replies', compact('replies'));
    }

    public function store(Replies $reply, Request $request)
    {
        $this->validate($request, [
            'body'  =>  'required'
        ]);

        Replies::create([
            'parent_id' =>  $reply->id,
            'thread_id' =>  $reply->thread_id,
            'body'      =>  $request->body,
            'email'     =>  auth()->user()->email,
            'approved_by'   =>  auth()->id(),
            'name'      =>  auth()->user()->name,
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function destroy(Replies $reply) {
        $reply->delete();

        return back()->with('success', 'اطلاعات با موفقیت حذف گردید');;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServicesResource;

class ServiceCategoryController extends Controller
{
    /**
     * @return view
     */
    public function index(){
        if(request()->wantsJson()){
            $categories = Services::orderBy('id', 'desc')->get();

            return ServicesResource::collection($categories);
        }

        return view('admin/services/category');
    }

    public function edit($id){
        $category = Services::find($id);
        return view('admin.services.category_edit', compact('category'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'  =>  'required|string|max:100',
            'lang'  =>  'required|in:fa,ar,en',
            'icon'  =>  'nullable|string',
            'description'   =>  'nullable|max:250'
        ]);

        try{
            DB::beginTransaction();

            $service = Services::create([
                'name'  =>  $request->name,
                'lang'  =>  strtoupper($request->lang),
                'description'   =>  $request->description
            ]);
    
            \Image::make($request->icon)->resize(64, 64, function($constraint){
                $constraint->aspectRatio();
            })->save(Storage::disk('public')->path('icons/'.$service->id.".png"));

            DB::commit();

            return new ServicesResource($service);
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }
    }

    /**
     * update the resource
     * 
     * @param Illuminate\Http\Request $request
     * @param integer $id
     * 
     * @return App\Models\Services
     */
    public function update(Request $request, $id){
        $this->validate($request,[
            'name'          =>  'required|string|max:100',
            'icon'          =>  'nullable',
            'lang'          =>  'required|in:fa,ar,en',
            'description'   =>  'nullable',
            'body'          =>  'nullable|string'
        ]);

        if($service = Services::find($id)){
            $service->update([
                'name'  =>  $request->name,
                'lang'  =>  strtoupper($request->lang),
                'description'   =>  $request->description,
                'body'          =>  $request->body
            ]);
            
            if($request->icon != null)
                \Image::make($request->icon)->resize(64, 64, function($constraint){
                    $constraint->aspectRatio();
                })->save(Storage::disk('public')->path('icons/'.$service->id.".png"));
        }

        return back();
    }

    /**
     * Destroy one resource
     */
    public function destroy($id){
        try{

            Services::where('id', $id)->delete();
        }catch(\Exception $e){
            abort(500, $e->getMessage());
        }
    }
}

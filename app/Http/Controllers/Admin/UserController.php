<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    /**
     * Retrive the list of users
     * 
     * 
     */
    public function index()
    {
        $users = Users::orderBy('id', 'desc')->paginate(20);

        return view('admin.users.index', compact('users'));
    }

    public function edit(Users $user)
    {
        $userRoles = $user->getRoleNames();
        $roles = Role::all();
        return view('admin.users.edit', compact('user', 'roles', 'userRoles'));
    }

    public function update(Request $request, Users $user)
    {
        $this->validate($request, [
            'name'  =>  'required',
            'email' =>  'required|unique:users,email,'.$user->id
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return back();
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        Users::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
        ]);

        return back();
    }

    public function updateSecurity(Request $request, Users $user)
    {
        $this->validate($request, [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user->password = \Hash::make($request->password);
        $user->save();

        return back();
    }

    public function updateRole(Request $request, Users $user)
    {
        $user->syncRoles($request->roles);

        Cache::flush();

        return back();
    }
}

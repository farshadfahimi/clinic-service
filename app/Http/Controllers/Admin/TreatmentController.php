<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Treatments;
use App\Models\Requests;
use App\Models\Comments;
use App\Models\Questions;
use App\Models\TreatmentsPricing;
use App\Http\Resources\QuestionsResource;
use App\Http\Resources\TreatmentsResource;
use App\Http\Resources\TreatmentPricingResource;
use Illuminate\Support\Facades\DB;

class TreatmentController extends Controller
{
    public function index()
    {
        return view('admin.treatments.index');
    }

    public function list(Request $request)
    {
        $query = Treatments::orderBy('lang', 'desc')->orderBy('id', 'desc');

        if($request->has('lang'))
            $query->where('lang', strtoupper($request->lang));

        return TreatmentsResource::collection($query->get());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          =>  'required',
            'parent_id'     =>  'sometimes|exists:treatments,id',
            'lang'          =>  'required|in:FA,EN,AR',
            'is_published'  =>  'required|boolean'
        ]);

        $treatments = Treatments::create([
            'name'      =>  $request->name,
            'parent_id' =>  $request->has('parent_id') ? $request->parent_id : 0,
            'slug'      =>  preg_replace('/(\s+)/', '-', $request->name),
            'lang'      =>  strtoupper($request->lang),
            'is_published'  =>  $request->is_published
        ]);

        return new TreatmentsResource($treatments);
    }

    public function edit($id){
        if($treatment = Treatments::find($id)){
            $treatments = Treatments::select('id', 'name', 'lang')
                ->where('parent_id', 0)
                ->where('lang', strtoupper($treatment->lang))
                ->where('id', '!=', $id)
                ->get();

            return view('admin.treatments.edit', compact('treatment', 'treatments'));
        }

        abort(404);
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name'  =>  'required',
            'parent_id' =>  'nullable',
            'body'  =>  'required'
        ]);

        if($treatment = Treatments::find($id)){
            $treatment->name = $request->name;
            $treatment->slug = preg_replace('/(\s+)/', '-', $request->name);
            $treatment->parent_id = $request->parent_id;
            $treatment->body = $request->body;

            if($request->hasFile('cover')){
                $treatment->cover = $request->file('cover')->extension();
                $request->file('cover')->storeAs('treatments_cover', $treatment->id.'.'.$request->file('cover')->extension(), 'public');
            }
    
            if($request->hasFile('page_cover')){
                $treatment->poster = $request->file('page_cover')->extension();
                $request->file('page_cover')->storeAs('treatments_poster', $treatment->id.'.'.$request->file('page_cover')->extension(), 'public');
            }

            $treatment->save();
            return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
        }

        abort(404);

    }

    public function changeStatus(Request $request, $id){
        if($treatment = Treatments::find($id)){
            $treatment->is_published = $request->is_published;
            $treatment->save();
        }else
            abort(404);
    }

    public function destroy($id){
        Treatments::where('id', $id)->delete();
    }

    public function faq($id){
        $treatment = Treatments::find($id);
        
        return view('admin.treatments.faq', compact('treatment'));
    }

    public function faq_list($id){
        $treatment = Treatments::with('questions')->find($id);

        return QuestionsResource::collection($treatment->questions);
    }

    public function store_faq(Request $request, $id){
        $this->validate($request, [
            'title' =>  'required',
            'description'   =>  'required',
            'sort'          =>  'required|integer'
        ]);

        if($treatment = Treatments::find($id)){
            $question = $treatment->questions()->create([
                'title' => $request->title,
                'description'   =>  $request->description,
                'sort'          =>  $request->sort
            ]);
            
            return new QuestionsResource($question);
        }else{
            abort(404);
        }

    }

    public function update_faq(Request $request, $id, $question_id){
        $this->validate($request, [
            'title' =>  'required',
            'description'   =>  'required'
        ]);

        if($treatment = Treatments::find($id)){
            $treatment->questions()->where('questions_id', $question_id)->update([
                'title' => $request->title,
                'description'   =>  $request->description
            ]);
        }else{
            abort(404);
        }
    }


    public function changeFaqSort(Request $request, $id){
        $this->validate($request, [
            'faqs'  =>  'required|array',
            'faqs.*.id' =>  'required|integer',
            'faqs.*.sort'   =>  'required|integer'
        ]);

        try{
            DB::beginTransaction();

            foreach($request->faqs as $faq){
                Questions::where('id', $faq['id'])->update([
                    'sort'  =>  $faq['sort']
                ]);
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            abort(500, $e->getMessage());
        }
    }

    public function destroy_faq($id, $question_id){
        if($treatment = Treatments::find($id)){
            $treatment->questions()->detach($question_id);

            Questions::where('id', $question_id)->delete();
        }else{
            abort(404);
        }
    }

    public function pricing($id){
        $treatment = Treatments::find($id);

        return view('admin.treatments.pricing', compact('treatment'));
    }

    public function fetchConditionPrice($id){
        $price = TreatmentsPricing::where('treatments_id', $id)->first();

        if($price)
            return new TreatmentPricingResource($price);
        else
            abort(404);
    }

    public function storeTreatmentPrice(Request $request, $id){
        $this->validate($request, [
            'description'   =>  'required',
            'prices'        =>  'required|array',
            'prices.*.title'    =>  'required',
            'prices.*.price'    =>  'required',
            'prices.*.offer'    =>  'nullable',
        ]);

        TreatmentsPricing::updateOrCreate(['treatments_id' => $id], [
            'description'   =>  $request->description,
            'prices'        =>  json_encode($request->prices, JSON_UNESCAPED_UNICODE)
        ]);
    }

    public function fetchCommentsList($id){
        if($treatment = Treatments::find($id)){
            $comments = $treatment->comments()->paginate(20);

            return view('admin.treatments.comments.index', compact('comments', 'treatment'));
        }

        abort(404);
    }

    public function createComment($id){
        if($treatment = Treatments::find($id)){
            return view('admin.treatments.comments.create', compact('treatment'));
        }

        abort(404);
    }

    public function storeComment(Request $request, $id){
        $this->validate($request, [
            'name'          =>  'required',
            'before'        =>  'required|file',
            'after'         =>  'required|file',
            'description'   =>  'required'
        ]);

        if($treatment = Treatments::find($id)){
            $comment = $treatment->comments()->create([
                'name'          =>  $request->name,
                'before'        =>  $request->file('before')->extension(),
                'after'         =>  $request->file('after')->extension(),
                'description'   =>  $request->description
            ]);


            if($request->hasFile('before')){
                $request->file('before')->storeAs($comment->id, 'before.'.$comment->before, 'comments');
            }
    
            if($request->hasFile('after')){
                $request->file('after')->storeAs($comment->id, 'after.'.$comment->after, 'comments');
            }

            return back();
        }

        abort(404);
    }

    public function seo($id)
    {
        $treatment = Treatments::where('id', $id)->first();

        return view('admin.treatments.seo', compact('treatment'));
    }

    public function storeSeo(Request $request, $id)
    {
        Treatments::where('id', $id)->update([
            'seo' => json_encode([
                'title' => $request->title,
                'description' => $request->description,
                'keywords'  =>  $request->keywords
            ])
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function updateComment(Request $request, $id, $comment_id)
    {
        $this->validate($request, [
            'name'          =>  'required',
            'before'        =>  'nullable|file',
            'after'         =>  'nullable|file',
            'description'   =>  'required'
        ]);

        if($comment = Comments::find($comment_id)){
            try{
                DB::beginTransaction();
                $comment->name = $request->name;
                $comment->description = $request->description;

                if($request->hasFile('before')){
                    $comment->before = $request->file('before')->extension();
                    $request->file('before')->storeAs($comment->id, 'before.'.$comment->before, 'comments');
                }

                if($request->hasFile('after')){
                    $comment->after = $request->file('after')->extension();
                    $request->file('after')->storeAs($comment->id, 'after.'.$comment->before, 'comments');
                }

                $comment->save();
                DB::commit();

                return back();
            }catch(\Exception $e){
                DB::rollback();

                abort(500);
            }
        }

        abort(404);
    }

    /**
     * @param   integer $id treatments_id
     * @param   integer $comment_id
     */
    public function editComment($id, $comment_id){
        if($treatment = Treatments::find($id)){
            $comment = $treatment->comments()->where('id', $comment_id)->first();

            return view('admin.treatments.comments.edit', compact('treatment', 'comment'));
        }

        abort(404);
    }

    public function destroyComment($id, $comment_id){
        if($comment = Comments::find($comment_id)){
            $comment->delete();

            return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
        }

        abort(404);
    }

    public function adviseList($id) {
        $treatment = Treatments::find($id);

        if(!$treatment)
            abort(404);

        $requests = $treatment->requests()->orderBy('id', 'desc')->paginate(15);

        return view('admin.treatments.request', compact('requests'));
    }

    public function requestList(Request $request) {
        $query = Requests::orderBy('id', 'desc')->with('treatment');

        if (!$request->has('archive'))
            $query->whereNull('checked_at');
        else
            $query->whereNotNull('checked_at');

        $requests = $query->paginate(15);
        return view('admin.treatments.requests', compact('requests'));
    }

    public function checkedRequest($id) {
        $request = Requests::where('id', $id)->first();

        $request->checked_at = \DB::raw('NOW()');
        $request->save();
    }
}

<?php

   namespace App\Http\Controllers\Admin;

   use App\Favorite;
   use App\Reply;
   use Illuminate\Http\Request;

   class FavoritesController extends Controller
   {
      /**
       * Store a newly created resource in storage.
       *
       * @param Reply $reply
       *
       * @return \Illuminate\Http\Response
       */
      public function store(Reply $reply)
      {
          $reply->favorite();

          return back();
      }

       /**
        * Remove the specified resource from storage.
        *
        * @param Reply $reply
        * @return void
        */
      public function destroy(Reply $reply)
      {
          $reply->unfavorite();
      }
   }

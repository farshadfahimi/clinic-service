<?php

namespace App\Http\Controllers\Admin;

use App\Models\Replies;
use App\Models\Threads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RepliesResource;

class RepliesController extends Controller {
    public function index($channelId, Threads $thread)
    {
        return RepliesResource::collection($thread->replies);
    }

    /**
     * @param        $channelId
     * @param Thread $thread
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\RedirectResponse
     */
    public function store($channelId, Threads $thread)
    {
        $this->validate(request(), ['body' => 'required']);

        $reply = $thread->addReply([
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

        if (request()->expectsJson())
        {
            return $reply->load('owner');
        }

        return back()->with('success', 'Your reply has been left.');
    }

    public function destroy(Replies $reply)
    {
        $reply->delete();
    }

    public function publish(Replies $reply){
        $reply->is_published = !$reply->is_published;
        $reply->approved_by = auth()->id();
        $reply->save();

        return new RepliesResource($reply);
    }

    public function update(Replies $reply)
    {
        $reply->update(['body' => request('body')]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\News;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::orderBy('id','desc')->paginate(25);

        return view('admin.news.index', compact('news'));
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required|max:250',
            'cover'         => 'required|file|image',
            'page_cover'    => 'required|file|image',
            'body'          => 'required|min:10'
        ]);

        try{
            DB::beginTransaction();

            $news = News::create([
                'user_id' => auth()->id(),
                'title' => $request->title,
                'body' => $request->body,
                'cover' =>  $request->file('cover')->extension(),
                'poster'    =>  $request->file('page_cover')->extension()
            ]);
            
            $request->file('cover')->storeAs('news_cover', $news->id.'.'.$request->file('cover')->extension(), 'public');
            $request->file('page_cover')->storeAs('news_poster', $news->id.'.'.$request->file('page_cover')->extension(), 'public');

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function edit($id)
    {
        $news = News::where('id', $id)->first();

        return view('admin.news.edit', compact('news'));
    }

    public function destroy($id)
    {
        News::where('id', $id)->delete();

        return back()->with('success', 'خبر با موفقیت حذف گردید');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Departments;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Resources\DepartmentsResource;

class DepartmentController extends Controller
{
    public function index(){

        if(request()->wantsJson()){
            $departments = Departments::orderBy('id', 'desc')->with('tags')->get();
            
            return DepartmentsResource::collection($departments);
        }

        return view('admin.departments.index');
    }

    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:100',
            'phone' =>  'required',
            'lang'  =>  'required|in:fa,en,ar',
            'address'   =>  'required|string|max:100',
        ]);

        app()->setLocale($request->lang);

        try{
            DB::beginTransaction();

            $department = Departments::create([
                'name'  =>  $request->name,
                'lang'  =>  strtoupper($request->lang),
                'phone' =>  $request->phone,
                'address'   =>  $request->address,
                'tags'      =>  collect($request->tags)->pluck('value')
            ]);
    
            Image::make($request->cover)->resize(500, 333, function($constraint){
                $constraint->aspectRatio();
            })->save(Storage::disk('public')->path('departments/'.$department->id.'.png'));

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }

        return new DepartmentsResource($department);
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'name' => 'required|string|max:100',
            'phone' =>  'required',
            'lang'  =>  'required|in:fa,en,ar',
            'address'   =>  'required|string|max:100',
        ]);

        try{
            DB::beginTransaction();

            if($department = Departments::find($id)){
                $department->name = $request->name;
                $department->lang = strtoupper($request->lang);
                $department->phone = en_num($request->phone);
                $department->address = en_num($request->address);

                $department->save();

                if(preg_match('/^data/', $request->cover))
                    Image::make($request->cover)->resize(500, 333, function($constraint){
                        $constraint->aspectRatio();
                    })->save(Storage::disk('public')->path('departments/'.$department->id.'.png'));
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }
    }

    public function destroy($id){
        $department = Departments::whereId($id)->first();

        if(!$department)
            abort(404);

        $department->delete();
    }
}

<?php

namespace App\Http\Controllers\Admin;


use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Models\Employees;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Resources\EmployeesResource;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    public function index(){
        return view('admin.users.employees');
    }

    public function list(){
        $employees = Employees::orderBy('lang', 'desc', 'id', 'desc')
        ->get();

        return EmployeesResource::collection($employees);
    }

    public function store(Request $request){
        $this->validate($request, [
            'name'  =>  'required',
            'avatar'=>  'required',
            'lang'  =>  'required|in:fa,en,ar,all',
        ]);

        try{
            DB::beginTransaction();

            $employee = Employees::create([
                'lang'  =>  $request->lang == 'all' ? null : strtoupper($request->lang),
                'name'  =>  $request->name,
                'expert'=>  $request->expert,
                'description'   =>  $request->description,
            ]);
    
            // Store Avatar
            Image::make($request->avatar)->resize(450, 450, function ($constraint) {
                $constraint->aspectRatio();
            })->save(Storage::disk('public')->path('employees/'.$employee->id.'.png'));

            // Store Thumb Avatar
            Image::make($request->avatar)->resize(90, 100, function($constraint){
                $constraint->aspectRatio();
            })->save(Storage::disk('public')->path('employees/'.$employee->id.'_thumb.png'));

            DB::commit();

            return new EmployeesResource($employee);
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name'  =>  'required',
            'avatar'=>  'required',
            'lang'  =>  'required|in:fa,en,ar,all',
        ]);

        $employee = Employees::find($id);

        if($employee){
            try{
                DB::beginTransaction();
    
                $employee->lang = $request->lang == 'all' ? null : strtoupper($request->lang);
                $employee->name = $request->name;
                $employee->expert = $request->expert;
                $employee->description = $request->description;
                $employee->save();
        
                // Store Avatar
                if(!filter_var($request->avatar, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)){
                    Image::make($request->avatar)->resize(450, 450, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(Storage::disk('public')->path('employees/'.$employee->id.'.png'));
        
                    // Store Thumb Avatar
                    Image::make($request->avatar)->resize(90, 100, function($constraint){
                        $constraint->aspectRatio();
                    })->save(Storage::disk('public')->path('employees/'.$employee->id.'_thumb.png'));
                }
    
                DB::commit();
    
                return new EmployeesResource($employee);
            }catch(\Exception $e){
                DB::rollback();
                abort(500, $e->getMessage());
            }
        }else{
            abort(404);
        }
    }

    public function update_values(Request $request, $id){
        $this->validate($request, [
            'values'    =>  'required|array',
            'values.*.title'    =>  'required',
            'values.*.value'    =>  'required',
        ]);

        if($employee = Employees::find($id)){
            $employee->values = json_encode($request->values, JSON_UNESCAPED_UNICODE);
            $employee->save();
        }else{
            abort(404);
        }
    }

    public function update_contact(Request $request, $id){
        $this->validate($request, [
            'values'    =>  'required',
        ]);

        if($employee = Employees::find($id)){
            $employee->contact = json_encode($request->values, JSON_UNESCAPED_UNICODE);
            $employee->save();
        }else{
            abort(404);
        }
    }

    public function destroy($id){
        Employees::where('id', $id)->delete();
    }
}

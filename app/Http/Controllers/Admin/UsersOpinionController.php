<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UsersOpinions;
use App\Http\Controllers\Controller;

class UsersOpinionController extends Controller
{
    public function index() {
        $opinions = UsersOpinions::orderBy('id', 'desc')->paginate(25);

        return view('admin.users-opinions.index', compact('opinions'));
    }

    public function create() {
        return view('admin.users-opinions.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'lang'  =>  'required|in:FA,EN,AR',
            'name'  =>  'required',
            'body'   =>  'required'
        ]);

        UsersOpinions::create([
            'lang'  =>  $request->lang,
            'name'  =>  $request->name,
            'body'   =>  $request->body
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function edit($id) {
        $opinion = UsersOpinions::where('id', $id)->first();

        return view('admin.users-opinions.edit', compact('opinion'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'lang'  =>  'required|in:FA,EN,AR',
            'name'  =>  'required',
            'body'   =>  'required'
        ]);

        UsersOpinions::where('id', $id)->update([
            'lang'  =>  $request->lang,
            'name'  =>  $request->name,
            'body'  =>  $request->body
        ]);

        return redirect()->route('admin.users-opinions.index')->with('success', 'طلاعات با موفقیت ذخیره گردید');
    }

    public function destroy($id) {
        UsersOpinions::where('id', $id)->delete();

        return back()->with('success', 'نظر کاربر حذف گردید');
    }
}

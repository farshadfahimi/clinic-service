<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Models\Departments;

class ServiceController extends Controller
{
    /**
     * @return View view
     */
    public function index(){
        return view('admin/services/index');
    }

    public function show($id){
        $service = Services::whereId($id)->first();

        if(!$service)
            abort(404);

        return view('admin.services.show', compact('service'));
    }

    /**
     * Store the Resource 
     * 
     * @param Illuminate\Http\Request $request
     * @return App\Models\Services
     */
    public function store(Request $request){
       $this->validate($request,[
           'lang'           =>  'required',
           'name'           =>  'required|string|max:100',
           'icon'           =>  'required',
           'description'    =>  'required'
       ]);
        try{
            DB::beginTransaction();
            $service = Services::create([
                'lang'          =>  strtoupper($request->lang),
                'name'          => $request->name,
                'description'   =>  $request->description
            ]);

            Image::make($request->icon)->resize(64, 64, function($constraint){
                $constraint->aspectRatio();
            })->save(Storage::disk('public')->path('icons').$service->id.".png");

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }
    }

    /**
     * Update Resources data
     * 
     * @param Illuminate\Http\Request $request
     * @param integer $id
     */
    public function update(Request $request, $id){
        $this->validate($request,[
            'lang'          =>  strtoupper($request->lang),
            'name'          => $request->name,
            'description'   =>  $request->description
        ]);
        
        try{
            Services::whereId($id)->update(['name'  =>  $request->name]);
        }catch(\Exception $e){
            abort(500, $e->getMessage());
        }
    }

    /**
     * Delete the resource
     * @param integer id
     */
    public function destroy($id){
        Services::whereId($id)->delete();
    }

    public function get_all_main_services(){
        $main_services = Services::orderBy('id', 'desc')->with('category')->get();
        $categories = ServicesCategories::orderBy('id', 'desc')->get();

        return response()->json([
            'services'      =>  $main_services,
            'categories'    =>  $categories
        ]);
    }

    /**
     * just return data for create service prices
     * @return json response
     */
    public function create_prices(){
        $carts_categories = CartsCategories::get();
        $departments = Departments::get();

        return response()->json(['carts_categories' => $carts_categories, 'departments' => $departments]);
    }

    /**
     * return all prices above the service
     */
    public function get_service_prices($id){
        $prices = ServicesPrices::whereServicesId($id)->with('departments', 'types')->get();

        return $prices;
    }

    /**
     * Store prices
     * @param integer $id services id
     * @param Illuminate\Http\Request   $request    return the prices data
     * 
     * @return App\Mdoels\ServicesPrices $price return the store price
     */
    public function store_service_price($id, Request $request){
        $this->validate($request,[
            'type'          =>  'required|exists:carts_categories,id',
            'department'    =>  'required|exists:departments,id',
            'price'         =>  'required|numeric',
            'offer'         =>  'required|numeric|max:100',
            'credit'        =>  'required|numeric|max:100'
        ]);

        $service  = Services::whereId($id)->exists();
        if(!$service)
            abort(422);

        $price = ServicesPrices::where([
            'services_id'   =>  $id,
            'departments_id'=>  $request->department,
            'types_id'  =>  $request->type
        ])->exists();

        if($price)
            abort(422);

        try{
            \DB::beginTransaction();
            $price = ServicesPrices::create([
                'services_id'       =>  $id,
                'departments_id'    =>  $request->department,
                'types_id'          =>  $request->type,
                'price'             =>  $request->price,
                'offer'             =>  $request->offer,
                'credit'            =>  $request->credit
            ]);
            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
            abort(500);
        }

        return ServicesPrices::whereId($price->id)->with('departments', 'types')->first();
    }

    /**
     * update service price
     * @param integer id service id
     * @para
     */
    public function update_service_price($id, $price_id, Request $request){
        $this->validate($request,[
            'price'         =>  'required|numeric',
            'offer'         =>  'required|numeric|max:100',
            'credit'        =>  'required|numeric|max:100'
        ]);

        try{
            \DB::beginTransaction();
            ServicesPrices::whereId($price_id)->update([
                'price' =>  $request->price,
                'offer' =>  $request->offer,
                'credit'=>  $request->credit    
            ]);
            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
            abort(500);
        }
    }

    /**
     * Update price is_publish field
     * @return response
     * 
     * @param integer $id price id
     */
    public function update_price_publishment($service_id, $price_id, Request $request){
        $this->validate($request, [
            'is_published' => 'required|boolean'
        ]);

        ServicesPrices::whereId($price_id)->update([
            'is_published'  =>  $request->is_published
        ]);
    }

    public function get_department_services(){
        if(auth()->user()->departments_id == null)
            abort(404);

        return ServicesCategories::with('prices')->get();
    }
}

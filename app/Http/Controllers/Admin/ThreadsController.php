<?php

namespace App\Http\Controllers\Admin;

use App\Models\Channels;
use App\Filters\ThreadFilters;
use App\Models\Threads;
use App\Models\Users;
use App\Models\Replies;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ThreadsResource;

class ThreadsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Channel       $channel
     *
     * @param ThreadFilters $filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, ThreadFilters $filters)
    {
        $channel = Channels::where('id', $id)->first();

        if (request()->wantsJson()){
            $threads = $this->getThreads($channel, $filters);
            return ThreadsResource::collection($threads);
        }
        
        return view('admin.channels.threads.index', compact('channel'));
    }

    public function create($id)
    {
        $channel = Channels::where('id', $id)->first();

        return view('admin.channels.threads.create', compact('channel'));
    }

    public function edit($id, Threads $thread)
    {
        $channel = Channels::where('id', $id)->first();
        $tags = $thread->tags()->pluck('name');
        return view('admin.channels.threads.edit', compact('channel', 'thread', 'tags'));
    }

    public function update(Request $request, $id, Threads $thread)
    {
        $this->validate($request, [
            'title'         => 'required|max:250',
            'cover'         => 'nullable|file|image',
            'page_cover'    => 'nullable|file|image',
            'body'          => 'required|min:10'
        ]);

        try{
            DB::beginTransaction();

            $thread->user_id = auth()->id();
            $thread->channel_id = $id;
            $thread->title = $request->title;
            $thread->body = $request->body;
            $thread->save();

            $thread->syncTags(explode('-', $request->tag));

            if($request->hasFile('cover')){
                $thread->cover = $request->file('cover')->extension();
                $request->file('cover')->storeAs('posts_cover', $thread->id.'.'.$request->file('cover')->extension(), 'public');
            }

            if($request->hasFile('page_cover')){
                $thread->poster = $request->file('page_cover')->extension();
                $request->file('page_cover')->storeAs('posts_poster', $thread->id.'.'.$request->file('page_cover')->extension(), 'public');
            }

            $thread->save();

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'title'         => 'required|max:250',
            'cover'         => 'required|file|image',
            'page_cover'    => 'required|file|image',
            'body'          => 'required|min:10'
        ]);

        try{
            DB::beginTransaction();

            $thread = Threads::create([
                'user_id' => auth()->id(),
                'channel_id' => $id,
                'title' => $request->title,
                'body' => $request->body,
                'cover' =>  $request->file('cover')->extension(),
                'poster'    =>  $request->file('page_cover')->extension()
            ]);

            $thread->attachTags(explode('-', $request->tag));
            
            $request->file('cover')->storeAs('posts_cover', $thread->id.'.'.$request->file('cover')->extension(), 'public');
            $request->file('page_cover')->storeAs('posts_poster', $thread->id.'.'.$request->file('page_cover')->extension(), 'public');

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500, $e->getMessage());
        }

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function seo($id, Threads $thread)
    {
        $channel = Channels::where('id', $id)->first();

        return view('admin.channels.threads.seo', compact('channel', 'thread'));
    }

    public function storeSeo(Request $request, $channel_id, $thread_id)
    {
        Threads::where('id', $thread_id)->update([
            'seo' => json_encode([
                'title' => $request->title,
                'description' => $request->description,
                'keywords'  =>  $request->keywords
            ])
        ]);

        return back()->with('success', 'اطلاعات با موفقیت ذخیره گردید');
    }

    public function replies($id, Threads $thread)
    {
        $channel = Channels::where('id', $id)->first();

        return view('admin.channels.threads.replies', compact('channel', 'thread'));
    }


    public function destroy($id, Threads $thread)
    {
        $thread->delete();

        if (request()->wantsJson())
            return response([], 200);
    }

    protected function getThreads(Channels $channel, ThreadFilters $filters)
    {
        $threads = Threads::latest()->filter($filters);

        if ($channel->exists)
        {
            $threads->where('channel_id', $channel->id);
        }

        return $threads->get();
    }
}

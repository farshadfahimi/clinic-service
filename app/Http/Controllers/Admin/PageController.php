<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Sliders;
use App\Http\Resources\SlidersResource;
use App\Models\Settings;

class PageController extends Controller
{
    public function home_main_slider(){

        if(request()->wantsJson()){
            $sliders = Sliders::orderBy('id', 'desc')->get();

            return SlidersResource::collection($sliders);
        }

        return view('admin.pages.home_slider');
    }

    public function store_main_slider(Request $request){
        $this->validate($request, [
            'title'         =>  'nullable|string|max:100',
            'description'   =>  'nullable|string|max:120',
            'lang'          =>  'required|in:FA,EN,AR,ALL',
            'image'         =>  'required'
        ]);

        try{
            DB::beginTransaction();

            $slide = Sliders::create([
                'title' =>  $request->title,
                'description'   =>  $request->description,
                'lang'      =>  $request->lang == 'ALL' ? null : $request->lang,
                'links'  =>  json_encode([], JSON_UNESCAPED_UNICODE),
                'image' =>  $request->file('image')->extension()    
            ]);
    
            $request->file('image')->storeAs('sliders', $slide->id.'.'.$request->file('image')->extension(), 'public');
            $request->file('mobile_image')->storeAs('sliders', $slide->id.'_mobile.'.$request->file('image')->extension(), 'public');

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return abort(500);
        }

        return back();
    }

    public function update_main_slider(Request $request){
        $this->validate($request, [
            'id'            =>  'required|exists:sliders,id',
            'title'         =>  'nullable|string|max:100',
            'description'   =>  'nullable|string|max:120',
            'lang'          =>  'required|in:FA,EN,AR,ALL',
            'image'         =>  'nullable'
        ]);

        try{
            DB::beginTransaction();

            $slide = Sliders::find($request->id);
            $slide->title = $request->title;
            $slide->description = $request->description;
            $slide->lang = $request->lang == 'ALL' ? null : strtoupper($request->lang);
            
            if($request->hasFile('image')){
                $slide->image = $request->file('image')->extension();
                $request->file('image')->storeAs('sliders', $slide->id.'.'.$request->file('image')->extension(), 'public');
            }

            if($request->hasFile('mobile_image')){
                $slide->image = $request->file('image')->extension();
                $request->file('mobile_image')->storeAs('sliders', $slide->id.'_mobile.'.$request->file('mobile_image')->extension(), 'public');
            }

            $slide->save();

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            abort(500);
        }

        return back();
    }

    public function destroy_main_slider($id){
        if($slide = Sliders::find($id)){
            $slide->delete();
        }else{
            return abort(404);
        }
    }

    public function store_main_slider_link(Request $request, $id){
        $this->validate($request, [
            'links'=> 'required|array',
            'links.*.title' => 'required|min:3',
            'links.*.link'  => 'required|min:3'
        ]);
        
        if($slide = Sliders::find($id)){
            $slide->links = json_encode($request->links, JSON_UNESCAPED_UNICODE);
            $slide->save();
        }else
            abort(404, "Not Found");
    }

    public function welcome_page(){
        return view('admin.pages.welcome');
    }

    public function menu(){
        return view('admin.pages.menu');
    }

    public function banner(){
        $setting = new Settings();
        return view('admin.pages.banner', compact('setting'));
    }

    public function storeBanner(Request $request){
        $this->validate($request, [
            'banner'    =>  'required',
            'type'      =>  'required|in:posts,conditions,treatments,team,services'    
        ]);

        $request->banner->storeAs('irsa', $request->type.'.jpg', 'public');
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Models\Conditions;
use App\Models\Settings;

class ConditionController extends Controller
{
    public function index(Request $request){
        $query = Conditions::where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
            ->orderBy('created_at', 'desc')
            ->published();

        $condition = null;

        if($request->condition) {
            $query->where('parent_id', $request->condition);
            $condition = Conditions::find($request->condition);
        }
        
        $conditions = $query->paginate(20);

        SEOMeta::setTitle(\Lang::get('message.pages.conditions'));
        $setting = new Settings;

        return view('conditions.index', compact('conditions', 'condition', 'setting'));
    }

    public function show($id){
        $condition = Conditions::where('id', $id)->published()->with('questions', 'childs', 'parent')->first();

        if($condition){
            if($condition->seo)
                SEOMeta::setTitle($condition->seo->title)->setDescription($condition->seo->description)->setKeywords($condition->seo->keywords);
            else
                SEOMeta::setTitle($condition->name);
            return view('conditions.show', compact('condition'));
        }

        abort(404);
    }
}

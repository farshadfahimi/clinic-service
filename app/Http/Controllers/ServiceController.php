<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Settings;

class ServiceController extends Controller
{
    public function index(){
        $services = Services::where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
            ->paginate(16);

        $setting = new Settings;

        return view('services.index', compact('services', 'setting'));
    }

    public function show($name){
        $service = Services::where('name', urldecode($name))
            ->where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
            ->first();
        
        if($service){
            $services = Services::where('lang', strtoupper(\LaravelLocalization::getCurrentLocale()))
                ->orWhere('id', $service->id)
                ->orderBy('id', 'desc')->take(8)->get();

            return view('services.show', compact('service', 'services'));
        }

        abort(404);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Threads;

class ReplyController extends Controller
{
    /**
     * @param       Thread $thread
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Threads $thread)
    {
        $this->validate($request, [
            'name'  =>  'required|max:100',
            'email' =>  'required|min:5|max:100',
            'body'  =>  'required|max:3000'
        ]);

        $reply = $thread->addReply([
            'name'  =>  $request->name,
            'email' =>  $request->email,
            'body' => $request->body
        ]);

        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use App\Models\Channels;
use App\Models\Threads;
use App\Models\Settings;
use App\Filters\ThreadFilters;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Channel       $channel
     *
     * @param ThreadFilters $filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channels $channel, ThreadFilters $filters)
    {
        $threads = $this->getThreads($channel, $filters);

        SEOMeta::setTitle(\Lang::get('message.pages.posts'));
        
        $setting = new Settings;

        return view('threads.index', compact('threads', 'setting'));
    }

    public function show($channel, Threads $thread)
    {
        $thread->with('replies');
        $tags = $thread->tags()->pluck('name')->toArray();

        try {
            if($thread->seo)
            SEOMeta::setTitle($thread->seo->title)->setDescription($thread->seo->description)->setKeywords($thread->seo->keywords);
            else
            SEOMeta::setTitle($thread->title)
                ->setDescription($thread->title);
        }catch(\Exception $e) {

        }
        
        return view('threads.show', compact('thread', 'tags'));
    }

    protected function getThreads(Channels $channel, ThreadFilters $filters)
    {
        $threads = Threads::latest()
            ->select('threads.*')
            ->join('channels', 'threads.channel_id', '=', 'channels.id')
            ->where('channels.lang', strtoupper(\LaravelLocalization::getCurrentLocale()))->filter($filters);

        if ($channel->exists)
        {
            $threads->where('channel_id', $channel->id);
        }

        return $threads->paginate(16);
    }
}

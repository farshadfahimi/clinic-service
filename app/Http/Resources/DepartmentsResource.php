<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  $this->id,
            'name'      =>  $this->name,
            'lang'      =>  $this->lang,
            'phone'     =>  $this->phone,
            'cover'     =>  $this->cover,
            'address'   =>  $this->address,
            'tags'      =>  TagsResource::collection($this->tags)
        ];  
    }
}

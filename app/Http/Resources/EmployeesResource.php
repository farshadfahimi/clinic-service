<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'lang'          =>  strtolower($this->lang),
            'avatar'        =>  $this->avatar,
            'name'          =>  $this->name,
            'expert'        =>  $this->expert,
            'description'   =>  $this->description,
            'values'        =>  $this->values,
            'contact'       =>  $this->contact,
            'updated_at'    =>  $this->updated_at
        ];
    }
}

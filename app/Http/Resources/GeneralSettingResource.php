<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GeneralSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'name'          =>  'general',
            'lang'          =>  strtolower($this->lang),
            'title'         =>  (string) $this->values->title,
            'keywords'      =>  (string) $this->values->keywords,
            'description'   =>  (string) $this->values->description,
            'email'         =>  (string) $this->values->email,
            'phone'         =>  (string) $this->values->phone,
            'companyPhone'  =>  isset($this->values->companyPhone) ? $this->values->companyPhone : '',
            'fax'           =>  (string) $this->values->fax,
            'address'       =>  (string) $this->values->address,
            'desc'          =>  isset($this->values->desc) ? $this->values->desc : '',
        ];
    }
}

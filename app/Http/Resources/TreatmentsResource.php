<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TreatmentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'parent_id' =>  $this->parent_id,
            'name'  =>  $this->name,
            'lang'  =>  $this->lang,
            'parent' => new TreatmentsResource($this->parent),
            'is_published'  =>  (boolean) $this->is_published,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepliesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'name'  =>  $this->name,
            'email' =>  $this->email,
            'avatar'=>  gravatar($this->email),
            'body'  =>  $this->body,
            'created_at'    =>  $this->created_at,
            'approved'  =>  $this->approvedBy,
            'is_published'  =>  (bool) $this->is_published
        ];
    }
}

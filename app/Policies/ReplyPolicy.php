<?php

   namespace App\Policies;

   use App\Models\Replies;
   use App\Models\Users;
   use Illuminate\Auth\Access\HandlesAuthorization;

   class ReplyPolicy
   {
      use HandlesAuthorization;

      public function update(Users $user,Replies $reply)
      {
         return $reply->user_id == $user->id;
      }
   }
